#define BOARD_NAME      "REMOTE_SOCKET_1"

/* Назначение выводов платы ESP-01 такое:
При прошивке в последний раз не менял плату. Оставил NodeMCU 1.0 (ESP-12 Module)

VCC, GND — питание платы (+3.3В);
URXD,UTXD — выводы RS232 толерантны к 3.3В
RST — Аппаратный сброс (reset)
GPIO0, GPIO2 — выводы GPIO
CH_PD — Chip enable, для работы должен быть подключен к +3.3В.

Для переключения в режим обновления прошивки нужно подать низкий уровень на GPIO0 и высокий на CH_PD.


 client.publish(topic, (uint8_t*)msg, 50, true);
*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

#include <PubSubClient.h>

#include <ArduinoJson.h>

#define PIN         1


/*_______________________________________________________________________________________________________________________________*/
const String DEVICES[] = {"rozetka1"};

const char* ssid = "ScorpGOS";
const char* password = "9031124945";

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];

boolean socket_status = false;

//home profiles-------------------------------------------
String currentProfileNo = "";
String jSonProfiles = "";
const int cDevices = sizeof(DEVICES) / sizeof(DEVICES[0]);
const char addLogTopic[]="profiles/log/add";
bool logEnable=false;
//--------------------------------------------------------
