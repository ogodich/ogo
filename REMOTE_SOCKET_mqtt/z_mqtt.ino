void mqtt_loop(){
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "reconnecting");
      // ... and resubscribe

      subscribeToDevicesStatus();
      client.subscribe("profiles/run/#");
      client.subscribe("profiles/change/log_enable");

      
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();

//*********************************************************************************************************************************
  String statusTopic = "devices/%s/status";
  String binarTopic = "devices/%s/status/binar";
  statusTopic.replace("%s",DEVICES[0]);
  binarTopic.replace("%s",DEVICES[0]);

  if (strcmp(topic,&binarTopic[0u])==0){
          socket_status = !socket_status;
          snprintf (msg, 75, "%d", socket_status);
          client.publish(&statusTopic[0u], msg, true);
  }
  else if (strcmp(topic,&statusTopic[0u])==0){
          socket_status = (call.toInt());
          digitalWrite(PIN, !socket_status);
  }



 //----------ПРОФИЛИ
  else if (String(topic).indexOf("profiles/run/") != -1) {
      RunProfiles(topic, String(call));
  }
  else if (strcmp(topic,"profiles/change/log_enable")==0){
      logEnable = call.toInt();
  }//----------------
   
//*********************************************************************************************************************************
//*********************************************************************************************************************************
}
