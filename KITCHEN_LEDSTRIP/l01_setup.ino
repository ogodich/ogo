

void setup(void){
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  //Первая лента
  strip.begin(); // This initializes the NeoPixel library.
  strip.setBrightness(255); // set brightness
  strip.show(); // Initialize all pixels to 'off'

  wifiPrint();

  otaInit();

  //Вторая лента
  strip2.begin(); // This initializes the NeoPixel library.
  strip2.setBrightness(255); // set brightness
  strip2.show(); // Initialize all pixels to 'off'

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  irrecv.enableIRIn();  // Start the receiver
  while (!Serial)  // Wait for the serial connection to be establised.
    delay(50);

  attachInterrupt(INTERRUPT_PIN, myInterrupt, RISING);

}
