void game(char* top, char* pay){
  //=============================================================================================
  if(String(top)=="GAME/start"){
    //Если игру включили, обнуляем счетчик и генерируем число
    if(String(pay).toInt()){
      gameCurrLeds=0;
      game_gen();
    }
    //при ВКЛ и ВЫКЛ обновляем ленту
    doStrip();
  }
  
  //=============================================================================================
  if(String(top)=="GAME/pressed"){
    int gamePressedButton=String(pay).toInt();
    if(gamePressedButton == gameCurrButton){                      //Если игрок угадал число,
      game_increase(gameColor[0], gameColor[1], gameColor[2]);    //награждаем (зажигаем ленту)
      game_gen();                                                 //и генерируем следующее число
    }
  } 
  
  //=============================================================================================
  if (String(top).indexOf("parts/") != -1) {
  int part = (String(top).substring(String(top).indexOf("parts/") + 6)).toInt();
  //При сообщении с номером выключателя, сохраняю статус этого выключателя
  gameParts[part-1] = String(pay).toInt();

  //Считаю сквозное количество активных кнопок с учетом статусов выключателей
  gameButtonsCount = 0;
  for(int i=0; i<3; i++){
    if(gameParts[i]){
      gameButtonsCount = gameButtonsCount + gameButtonsInSwitchs[i];
    }
  }
  
  int t=0;
  for(int i=0; i<11;i++){
    gameResultButtons[i] = 0;
  }
  
  for(int i=0; i<3;i++){                                    //бежим по выключателям
    if(gameParts[i]){                                       //заходим только во включенные выключатели
      for(int y=0; y<gameButtonsInSwitchs[i]; y++){        //бежим по кнопкам текущего выключателя
        gameResultButtons[t]=gameButtonsNumbers[i][y];      //добавляем в итоговый массив сквозной номер текущей кнопки текущего выключателя
        t++;
      }
    }
  }
  
    Serial.println("");
    Serial.print("top=");
    Serial.print(top);
    Serial.print(",\nparts:\t");    
  for(int i=0; i<3;i++){    
    Serial.print(i);
    Serial.print("=");
    Serial.print(gameParts[i]);
    Serial.print(",");
  }
  Serial.println("");
  Serial.print("gameResultButtons:\t");
  for(int i=0; i<11;i++){
    Serial.print(i);
    Serial.print("=");
    Serial.print(gameResultButtons[i]);
    Serial.print(",");
  }
    Serial.println("");
    Serial.print("gameButtonsCount:\t");
    Serial.print(gameButtonsCount);
    Serial.println(".");
    
  }
}

void game_gen(){
  //gameCurrButton = random(1,12);    //случайная кнопка (от 1 до 11)

  int randomButton = random(0,gameButtonsCount);              //Случайная кнопка от 0 до количества кнопок (исключительно)
  gameCurrButton = gameResultButtons[randomButton];           //Подставляю сквозной номер кнопки с учетом включенных выключателей
  
  int colRandMin = random(0,3);     //случайный канал цвета, который будем выключенным
  int colRandMax = random(0,3);     //случайный канал цвета, который будем ярким
  while(colRandMin==colRandMax){
    colRandMax = random(0,3);       //если совпали - ещё попытка
  }
  
  gameColor[colRandMin] = 0;        //случайный канал стал выключенным
  gameColor[colRandMax] = 255;      //случайный канал стал ярким
  for(int i=0; i<3; i++){
    if(i != colRandMin && i != colRandMax) gameColor[i] = random(0,256);  //последний по-честному рандом
  }
  char msg[100];
  snprintf(msg,100,"{\"button\":%d,\"color\":[%d,%d,%d],\"log\":\"colRandMin:%d,colRandMax:%d\"}",gameCurrButton, gameColor[0], gameColor[1], gameColor[2], colRandMin, colRandMax);

  //snprintf(msg,50,"{\"button\":%d,\"color\":[0,0,255]}",gameCurrButton); 
  client.publish("GAME/currButton", msg);
}

void game_increase(byte r, byte g, byte b){
  if(gameCurrLeds<PIX_NUM) {
        gameCurrLeds++;
        gameColorHistory[gameCurrLeds][0] = r;
        gameColorHistory[gameCurrLeds][1] = g;
        gameColorHistory[gameCurrLeds][2] = b;
        for(int i=PIX_NUM; i>=PIX_NUM-gameCurrLeds; i--) {
              strip.setPixelColor(i, strip.Color(
                                                  gameColorHistory[PIX_NUM-i][0],
                                                  gameColorHistory[PIX_NUM-i][1],
                                                  gameColorHistory[PIX_NUM-i][2],
                                                  0)
                                  );
      }
      strip.show();
  }
}
