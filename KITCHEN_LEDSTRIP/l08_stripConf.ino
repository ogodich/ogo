/*****************************************************************************************************************/
void upload_preset(char* pay){
  char myTop [40];
  snprintf (myTop, 40, "STRIP/db/%s/%d",  BOARD_NAME,  String(pay).toInt());

  snprintf(charOut,1,"");
  //пиксели
  for(int i=0;i<PIX_NUM;i++){
    //цвета
    for(int y=0;y<=3;y++){
      //отладка: //char iter[100]; //snprintf(iter,100, "arrRGBW[%d][%d]=%d", y,i,arrRGBW[y][i]); //Serial.println(iter);
      snprintf(charOut,4000, "%s%d,", charOut, arrRGBW[y][i]);
    }
  }
  //Serial.print("charOut=");
  //Serial.println(charOut);
  client.publish(myTop, charOut, true);
}
/*****************************************************************************************************************/
void set_conf_mode(char* top, char* pay){
  ConfMode = String(pay);
  //client.unsubscribe("STRIP/conf/currPoint");   //конкретная точка
  client.unsubscribe("STRIP/conf/parts/#");     //получаем статус частей
  client.unsubscribe("STRIP/conf/interval/#");  //группа очередей для интервального режима

  //if     (ConfMode=="point") client.subscribe("STRIP/conf/currPoint");   //конкретная точка
  if(ConfMode=="parts") {
        for(int part=1; part<=PartsNum; part++){
          char myTop [20];
          snprintf (myTop, 20, "STRIP/conf/parts/%d", part);
          client.publish(myTop, "0");
        }
        client.subscribe("STRIP/conf/parts/#");
  }
  else if(ConfMode=="interval") {
        //client.publish("STRIP/conf/interval/mode", "start");
        //ConfIntervalMode = "start";
        CurrIntStart = 1;
        CurrIntLength = 1;
        //sprintf(msg,"%d,%d",CurrIntStart,CurrIntLength);
        //client.publish("STRIP/conf/log", msg);
        client.subscribe("STRIP/conf/interval/#");
        paintInterval();
  }
}
/*****************************************************************************************************************/
void do_parts(char* top, char* pay){
  int part = (String(top).substring(String(top).indexOf("parts/") + 6)).toInt();
  PartsArray[part] = String(pay).toInt();
}

int selected_parts_num (){
  //Вычисляю количество выбранных частей
  int res = 0;
  for (int i=0; i<=PartsNum; i++) if(PartsArray[i+1]) res++;
  return res;
}
/*****************************************************************************************************************/
void saveCurrInterval(){
  int from = CurrIntStart - 1;
  int to = CurrIntStart + CurrIntLength - 2;

  int fromInv = PIX_NUM - to - 1;
  int toInv = PIX_NUM - from - 1;
  
  do_rgb    (fromInv, toInv, IntervalRGB);
  do_white  (fromInv, toInv, IntervalWhite);

  //sprintf(msg,"%d,%d;%d,%d",CurrIntStart,CurrIntLength,fromInv,toInv);
  //client.publish("STRIP/conf/log", msg);
}
/*****************************************************************************************************************/
/*void calculate_interval (char* top, char* pay){
  if     (ConfIntervalMode=="start")  {
    CurrIntStart += String(pay).toInt();
    CurrIntStart = CurrIntStart > PIX_NUM ? PIX_NUM : CurrIntStart;
    CurrIntStart = CurrIntStart < 1 ? 1 : CurrIntStart;
    //Если при изменении СТАРТа сумма старта и длины превысила длину ленты, увеличиваем на допустимую разницу
    if (CurrIntLength + CurrIntStart > PIX_NUM) CurrIntStart = PIX_NUM - CurrIntLength + 1;  
  }
  else if(ConfIntervalMode=="length") {
    CurrIntLength += String(pay).toInt();
    CurrIntLength = CurrIntLength > PIX_NUM ? PIX_NUM : CurrIntLength;
    CurrIntLength = CurrIntLength < 1 ? 1 : CurrIntLength;
    //Тут хитро: если при изменении ДЛИНЫ сумма старта и длины превысила длину ленты, сдвигаем старт влево
    if(CurrIntLength + CurrIntStart > PIX_NUM + 1) CurrIntStart = PIX_NUM + 1 - CurrIntLength;
  }
  
  sprintf(msg,"%d,%d",CurrIntStart,CurrIntLength);
  client.publish("STRIP/conf/log", msg);
  
  paintInterval();
}*/
void calculate_interval_new (char* top, char* pay){
  if (String(top).indexOf("/interval/start") != -1) {
    //два обработчика: для абсолютного и относительного сообщения
    if      (String(top).indexOf("/absolute") != -1) CurrIntStart = map(String(pay).toInt(),     0,100,    0,PIX_NUM);
    else if (String(top).indexOf("/relative") != -1) CurrIntStart = CurrIntStart + String(pay).toInt();
    CurrIntStart = CurrIntStart > PIX_NUM ? PIX_NUM : CurrIntStart;
    CurrIntStart = CurrIntStart < 1 ? 1 : CurrIntStart;
    //Если при изменении СТАРТа сумма старта и длины превысила длину ленты, увеличиваем на допустимую разницу
    if (CurrIntLength + CurrIntStart > PIX_NUM) CurrIntStart = PIX_NUM - CurrIntLength + 1;  
  }
  else if (String(top).indexOf("/interval/length") != -1) {
    if      (String(top).indexOf("/absolute") != -1) CurrIntLength = map(String(pay).toInt(),     0,100,    0,PIX_NUM);
    else if (String(top).indexOf("/relative") != -1) CurrIntLength = CurrIntLength + String(pay).toInt();
    CurrIntLength = CurrIntLength > PIX_NUM ? PIX_NUM : CurrIntLength;
    CurrIntLength = CurrIntLength < 1 ? 1 : CurrIntLength;
    //Тут хитро: если при изменении ДЛИНЫ сумма старта и длины превысила длину ленты, сдвигаем старт влево
    if(CurrIntLength + CurrIntStart > PIX_NUM + 1) CurrIntStart = PIX_NUM + 1 - CurrIntLength;
  }
  
  //sprintf(msg,"%d,%d",CurrIntStart,CurrIntLength);
  //client.publish("STRIP/conf/log", msg);
  
  paintInterval();
}

/*****************************************************************************************************************/
void paintInterval(){
  doStrip();
  for (int pix = CurrIntStart - 1; pix <= CurrIntStart + CurrIntLength - 2; pix++) {
    if (STRIPTYPE=="RGBW") strip.setPixelColor(PIX_NUM - pix -1, strip.Color((IntervalRGB / 256) / 256,
                                                                              (IntervalRGB / 256) % 256,
                                                                               IntervalRGB % 256,
                                                                               IntervalWhite));
    else if (STRIPTYPE=="RGB") strip.setPixelColor(PIX_NUM - pix -1, strip.Color((IntervalRGB / 256) / 256,
                                                                                  (IntervalRGB / 256) % 256,
                                                                                   IntervalRGB % 256));
  }
  strip.show();
}
/*****************************************************************************************************************/
void set_color(char* top, uint32_t color, boolean saveColor){
  //RGB
  if (String(top)=="STRIP/conf/setColor/rgb") {
    if (saveColor) CurrRGB=color;
    if(ConfMode=="full"){
      do_rgb  (0,PIX_NUM-1,color);
    }
  
    else if(ConfMode=="parts"){
      for(int part=0; part<=PartsNum; part++){      
        if(PartsArray[part+1] == true)  do_rgb(PartsBegins[part],PartsEnds[part],color);        
      }
    }
  
    //else if(ConfMode=="point"){
    //  do_rgb (CurrConfPoint,CurrConfPoint,color);
    //}
  
    else if(ConfMode=="middles"){
      for(auto point : Middles) do_rgb(point,point,color);
    }
  
    else if(ConfMode=="interval"){
      paintInterval();
    }
  }
  
  //WHITE
  else if (String(top)=="STRIP/conf/setColor/white") {
    if (saveColor) CurrWhite = color;
    if(ConfMode=="full"){
      do_white  (0,PIX_NUM-1,color);
    }
  
    else if(ConfMode=="parts"){
      for(int part=0; part<=PartsNum; part++){      
        if(PartsArray[part+1] == true)  do_white(PartsBegins[part],PartsEnds[part],color);
      }
    }
  
    //else if(ConfMode=="point"){
    //  do_white (CurrConfPoint,CurrConfPoint,color);
    //}
  
    else if(ConfMode=="middles"){
      for(auto point : Middles) do_white(point,point,color);
    }
  
    else if(ConfMode=="interval"){
      paintInterval();
    }    
  }

  if(ConfMode!="interval") doStrip(); //Зажигаем ленту
  if (saveColor) {
    CurrBrightness=100;
    IntervalRGB = CurrRGB;
    IntervalWhite = CurrWhite;
  }
}
/*****************************************************************************************************************/
void set_brightness(char* top, char* pay){
  if (String(top).indexOf("/setBrightness/step") != -1) {
    BrightnessStep = String(pay).toInt();
  }
  else if (String(top).indexOf("/setBrightness/delta") != -1) {
    CurrBrightness += String(pay).toInt() * BrightnessStep;
    CurrBrightness = CurrBrightness > 100 ? 100 : CurrBrightness;
    CurrBrightness = CurrBrightness < 0 ? 0 : CurrBrightness;
    
    int r = ((CurrRGB / 256) / 256) * CurrBrightness / 100;
    int g = ((CurrRGB / 256) % 256) * CurrBrightness / 100;
    int b = (CurrRGB % 256) * CurrBrightness / 100;
    uint32_t newRGB = r*256*256 + g*256 + b;
    int newWhite = CurrWhite * CurrBrightness / 100;
    IntervalRGB = newRGB;
    IntervalWhite = newWhite;

    set_color("STRIP/conf/setColor/rgb", newRGB, false);
    set_color("STRIP/conf/setColor/white", newWhite, false);

    snprintf(msg, 10, "brgt=%d", CurrBrightness);
    client.publish("STRIP/conf/log", msg);
    //char msg[200];
    //snprintf(msg, 200, "CurrBrightness=%d, r=%d, g=%d, b=%d, newRGB=%d, newWhite=%d, CurrRGB=%d, CurrWhite=%d", CurrBrightness, r, g, b, newRGB, newWhite, CurrRGB, CurrWhite);
    //client.publish("log", msg);
  }
  else if (String(top).indexOf("/setBrightness/absolute") != -1) {
    CurrBrightness = String(pay).toInt();
    CurrBrightness = CurrBrightness > 100 ? 100 : (CurrBrightness < 0 ? 0 : CurrBrightness);
    //snprintf(msg, 5, "%d", CurrBrightness);
    //client.publish("STRIP/conf/setBrightness/absolute_info", msg);

    
    int r = ((CurrRGB / 256) / 256) * CurrBrightness / 100;
    int g = ((CurrRGB / 256) % 256) * CurrBrightness / 100;
    int b = (CurrRGB % 256) * CurrBrightness / 100;
    uint32_t newRGB = r*256*256 + g*256 + b;
    int newWhite = CurrWhite * CurrBrightness / 100;
    IntervalRGB = newRGB;
    IntervalWhite = newWhite;

    set_color("STRIP/conf/setColor/rgb", newRGB, false);
    set_color("STRIP/conf/setColor/white", newWhite, false);
    
  }  
}
/*****************************************************************************************************************/
