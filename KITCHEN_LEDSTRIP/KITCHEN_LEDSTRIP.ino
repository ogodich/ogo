#define BOARD_NAME    "KITCHEN_STRIP"
#define BOARD_PASS    "strip"
//*************************************************************************************************************

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <Adafruit_NeoPixel.h>

#include <ArduinoSort.h>

#include <PubSubClient.h>

#include <stdlib.h>

#include <ArduinoJson.h>

#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRutils.h>


//*************************************************************************************************************

#define PIN            D1
#define PIN2           D7
#define RECV_PIN       D2
#define INTERRUPT_PIN  D5
#define PIX_NUM        216
#define STRIPTYPE      "RGBW"

//*************************************************************************************************************
const String DEVICES[] = {"kitStrip1","kitStrip2"};
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

const int PartsNum = 6;
const int PartsBegins    [PartsNum] = {180, 157, 97,  71, 23, 0};
const int PartsEnds      [PartsNum] = {215, 179, 156, 96, 70, 22};
const int Middles        [9]  = {11, 35, 58, 83, 106, 124, 145, 168, 197};

Adafruit_NeoPixel strip  = Adafruit_NeoPixel(PIX_NUM, PIN,  NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(70,      PIN2, NEO_GRBW + NEO_KHZ800);

//это обязательная хрень после перехода на IDE 1.8.10. Нужна, если в скетче используются прерывания. Без этой строчки падает плата после загрузки!
//Нужно указывать имя функции, выполняемой при прерывании
void ICACHE_RAM_ATTR myInterrupt ();

//boolean switchedStrip = false;
boolean switchedStrip2 = false;

int mLeds[PIX_NUM];
int countLeds[10];

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;

boolean startBoot = false;
double startTime = 0;

char msg[50];
int value = 0;

long lastBrightMsg = 0;
int currentBrightness = 0;
int prevBrightness = 0;

int motivationBrightness = 0;
int secondStripBrigth = 110;

String debug;
char deb[150];

boolean PartsArray[7];
int     stripConfR [6][6];
int     stripConfG [6][6];
int     stripConfB [6][6];
int     stripConfW [6][6];
int     stripConfBr[6][6];
int oldCurrPreset = 1;

boolean runTimer = false;
long timerRefresh = 0;
long timerStop = 0;
int timerCurLed = 0;
int timerPrevLed = -1;
int timerPassTime = 0;
int timerInterval = 0;

IRrecv irrecv(RECV_PIN);
decode_results results;


String irrPrevVal = "";
String irrCurrVal = "";
int irrTime = 0;
long irrLastTime = 0;
boolean irrRecieved = false;
uint64_t iCurrVal;
uint64_t iPrevVal;

int irrMode = 0;
long irrModeTimer = 0;

long irrModeAnimationTimer = 0;
boolean irrAnimationTrig = false;

boolean runLedAnimation = false;

byte neopix_gamma[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };


char charCall[4000];
char charOut[4000];

int arrRGBW[4][PIX_NUM];

long irrCountMessages = 0;

int currPreset = 1;
int currSavePreset = 1;

String ConfMode = "full";
int CurrConfPoint = 0;

String ConfIntervalMode = "";
int CurrIntStart = 1;
int CurrIntLength = 1;
uint32_t CurrRGB = 7405568;
int CurrWhite = 0;
int CurrBrightness = 100;
int BrightnessStep = 1;
uint32_t IntervalRGB = 7405568;
int IntervalWhite = 0;

int TimerRemainTime = 0;

int gameCurrLeds = 0;
int gameCurrButton = 0;
byte gameColor[3];
byte gameColorHistory[216][3];
boolean gameParts[3]={false,false,false};
int gameButtonsInSwitchs[3]={5,3,3};
int gameButtonsCount = 0;
int gameButtonsNumbers[3][5]={{1,2,3,4,5},{6,7,8,-1,-1},{9,10,11,-1,-1}};
int gameResultButtons[11];

uint32_t secondStripRGB = 0;
int secondStripWhite = 0;

//home profiles-------------------------------------------
String currentProfileNo = "";
String jSonProfiles = "";
const int cDevices = sizeof(DEVICES) / sizeof(DEVICES[0]);
const char addLogTopic[]="profiles/log/add";
bool logEnable=false;
//--------------------------------------------------------
