void mqtt_loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}

//*********************************************************************************************************************************
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // ... and resubscribe

      client.subscribe("strip/kitchen/timer");
      client.subscribe("strip/kitchen/specificLedOn");
      
      client.subscribe("STRIP/run/#");

      client.subscribe("STRIP/selectStripToConfig");
                                                                  
      client.subscribe("GAME/#");

      subscribeToDevicesStatus();
      client.subscribe("profiles/run/#");
      client.subscribe("profiles/change/log_enable");


    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
      
      //временная метка
      for (int i=72; i<99; i++) {
        strip.setPixelColor(i, strip.Color(255, 0, 0, 0));
        strip.show();
      }

    }
  }
}


boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  String top = String(topic);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.println("] ");
  //Serial.print("length=");
  //Serial.println(length);

  String call = "";
  
  //char charCall[length];
  //memset(&charCall, 0, sizeof(charCall));
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call += (char)payload[i];
    charCall[i] = (char)payload[i];
  }
  charCall[length]=0;
  Serial.println();


//****************************************************************************************************************************************
//************ПЕРВАЯ*ЛЕНТА****************************************************************************************************************
  if (strcmp(topic, "devices/kitStrip1/status") == 0) {
        //switch_to_preset(topic, call.toInt());
        
        int preset = call.toInt();
        if (preset == 0) client.publish("wled/kitchen", "OFF");
        else {
          snprintf (msg, 75, "PL=%d", call.toInt());
          client.publish("wled/kitchen/api", msg);
        }
  }
  if (strcmp(topic, "devices/kitStrip1/status/binar") == 0) {
    //бинарный обработчик
        currPreset = currPreset > 0 ? 0 : 1;
        snprintf (msg, 75, "%d", currPreset);
        client.publish("devices/kitStrip1/status", msg, true);
  }
//************ВТОРАЯ*ЛЕНТА****************************************************************************************************************
  else if (strcmp(topic, "devices/kitStrip2/status") == 0) {
        secondStripBrigth = call.toInt();
        set_strip2_color(190 * secondStripBrigth / 100, 45 * secondStripBrigth / 100, 45 * secondStripBrigth / 100, 120 * secondStripBrigth / 100);
        strip2.show();
  }
  else if (strcmp(topic, "devices/kitStrip2/status/binar") == 0) {
    //бинарный обработчик
        switchedStrip2 = !switchedStrip2;
        client.publish("devices/kitStrip2/status", switchedStrip2 ? "100" : "0", true);
  }
  else if (top.indexOf("devices/kitStrip2/status/color") != -1 ) {
        simpleColorSet(topic, charCall);
  }

//****************************************************************************************************************************************
  else if (top.indexOf("STRIP/db/") != -1) {
    load_preset(topic, charCall);
  }

//****************************************************************************************************************************************
  //ПОДПИСЫВАЮСЬ НА ОЧЕРЕДИ НАСТРОЙКИ ЛЕНТЫ                 только если пришло сообщение с именем моей платы
  else if (strcmp(topic, "STRIP/selectStripToConfig") == 0) {
    if(call==BOARD_NAME){
      //client.publish("log/teststrip", "subscribed");
      client.subscribe("STRIP/conf/uploadPreset");          //Сохранить настройку текущего профиля
      client.subscribe("STRIP/conf/mode");                  //Выбрать режим настройки
      client.subscribe("STRIP/conf/setColor/#");            //Цветовые режимы
      client.subscribe("STRIP/conf/setBrightness/#");       //Настройка яркости
    }
    else {                                                    //если имя платы не моё, отписываюсь (настраивают другую плату)
      //client.publish("log/teststrip", "UNsubscribed");
      client.unsubscribe("STRIP/conf/uploadPreset");          //Сохранить настройку текущего профиля
      client.unsubscribe("STRIP/conf/mode");                  //Выбрать режим настройки
      client.unsubscribe("STRIP/conf/setColor/#");            //Цветовые режимы
      client.unsubscribe("STRIP/conf/setBrightness/#");       //Настройка яркости
      client.unsubscribe("STRIP/conf/parts/#");               //Селектор частей (подписка прописана в логике stripConf)
      client.unsubscribe("STRIP/conf/interval/#");            //Группа очередей для настройки интерваловь(подписка прописана в логике stripConf)
    }
  }

//****************************************************************************************************************************************
  else if (strcmp(topic, "STRIP/conf/uploadPreset") == 0) {
    upload_preset(charCall);
  }
  else if (top.indexOf("STRIP/conf/setColor/") != -1) {
    set_color(topic, call.toInt(), true);
  }
  else if (top.indexOf("STRIP/conf/setBrightness/") != -1) {
    set_brightness(topic, charCall);
  }
  else if (strcmp(topic, "STRIP/conf/mode") == 0) {
    set_conf_mode(topic, charCall);
  }
  else if (String(top).indexOf("/interval/length") != -1) {
    calculate_interval_new(topic, charCall);
  }
  else if (String(top).indexOf("/interval/start") != -1) {
    calculate_interval_new(topic, charCall);
  }
  else if (strcmp(topic, "STRIP/conf/interval/save") == 0) {
    saveCurrInterval();
  }
  //обработчик выбора частей для настройки
  else if (top.indexOf("STRIP/conf/parts/") != -1 ) {
    do_parts(topic, charCall);
  }
  

//****************************************************************************************************************************************
//****************************************************************************************************************************************
  else if (strcmp(topic, "strip/hex") == 0) {
    String rr = "0x";
    rr += (char)payload[1];
    rr += (char)payload[2];
    char r[50];
    rr.toCharArray(r, 50);

    String gg = "0x";
    gg += (char)payload[3];
    gg += (char)payload[4];
    char g[50];
    gg.toCharArray(g, 50);

    String bb = "0x";
    bb += (char)payload[5];
    bb += (char)payload[6];
    char b[50];
    bb.toCharArray(b, 50);

    PowerStrip(strtoul(r, NULL, 16),
                 strtoul(g, NULL, 16),
                 strtoul(b, NULL, 16),
                 0x0);
  }
//*****************
  else if (strcmp(topic, "strip/decimal") == 0) {
    uint32_t sourceInt = call.toInt();
    int r = (sourceInt / 256) / 256;
    int g = (sourceInt / 256) % 256;
    int b = sourceInt % 256;

    PowerStrip(r, g, b, 0);
  }





//***************** ТАЙМЕР
  else if (strcmp(topic, "strip/kitchen/timer") == 0) {    
    my_timer(call.toInt());
  }
//***************** Адресное зажигание светодиода на ленте
  else if (strcmp(topic, "strip/kitchen/specificLedOn") == 0) {   
    DynamicJsonBuffer specificLedOnBuf;
    JsonObject& specificLedOnObj = specificLedOnBuf.parseObject(charCall);
    int specLed  = specificLedOnObj["led"];
    int specColR = specificLedOnObj["red"];
    int specColG = specificLedOnObj["green"];
    int specColB = specificLedOnObj["blue"];
    int specColW = specificLedOnObj["white"];
    strip.setPixelColor(specLed, strip.Color(specColR, specColG, specColB, specColW));
    strip.show();
  }
  else if (top.indexOf("GAME/") != -1) {
    game(topic, charCall);
  }
  
  //----------ПРОФИЛИ
  else if (String(topic).indexOf("profiles/run/") != -1) {
      RunProfiles(topic, String(call));
  }
  else if (strcmp(topic,"profiles/change/log_enable")==0){
      logEnable = call.toInt();
  }//----------------

 
}   //*********************************************************************************************************************************
