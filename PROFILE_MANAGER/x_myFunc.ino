void dht_send() {
  if (millis() - lastMsg > 60000) {
    lastMsg= millis();
    
    char dht_message[100];

    double H = round(dht.readHumidity() * 10)/10;
    double T = round(dht.readTemperature() * 10)/10;
    int h = round(H);
    int t = round(T);

    snprintf (dht_message, 100, "{\"TEMP\":%f, \"HUM\":%f, \"temp\":%d, \"hum\":%d, \"now\":%d}", T,H,t,h, millis());
    Serial.println(dht_message);

    //отправляю
    //client.publish("sensors/temp_hum/kitchen", dht_message, true);    
    snprintf (msg, 5, "%d",t); client.publish("sensors/kitchen/temperature", msg, true);
    snprintf (msg, 5, "%d",h); client.publish("sensors/kitchen/humidity", msg, true);

  }
}


/*******************************************************************************************************************************/

void otaInit() {
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
  ArduinoOTA.setPassword((const char *)BOARD_PASS);

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
}
