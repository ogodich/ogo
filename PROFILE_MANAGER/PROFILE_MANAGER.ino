/* Назначение выводов платы ESP-01 такое:
VCC, GND — питание платы (+3.3В);
URXD,UTXD — выводы RS232 толерантны к 3.3В
RST — Аппаратный сброс (reset)
GPIO0, GPIO2 — выводы GPIO
CH_PD — Chip enable, для работы должен быть подключен к +3.3В.

Для переключения в режим обновления прошивки нужно подать низкий уровень на GPIO0 и высокий на CH_PD.


 client.publish(topic, (uint8_t*)msg, 50, true);
*/


#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>

#include <Adafruit_Sensor.h>

#include <DHT.h>
#include <DHT_U.h>

#include <ArduinoJson.h>





#define BOARD_NAME "PROFILE_MANAGER"
#define BOARD_PASS "profiles"

#define BUILT_IN_LED D0

#define DHTPIN     D6
#define DHTTYPE    DHT22     // DHT 22 (AM2302)
DHT dht(DHTPIN, DHTTYPE);

uint32_t delayMS;


/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";


const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
