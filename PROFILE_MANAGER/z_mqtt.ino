void mqtt_loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}

void mySubscribtion(){
  client.subscribe("light/kitchen/corner");
  
  client.subscribe("profiles/change/#");  
  client.subscribe("profiles/run/#");
  client.subscribe("profiles/log");
  client.subscribe("profiles/log/add");
  client.subscribe("devices/+/status");  
}

void callback(char* topic, byte* payload, unsigned int length) {
  String top = String(topic);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call += (char)payload[i];
  }
  Serial.println();

  //*********************************************************************************************************************************
    if (top.indexOf("devices/") != -1 && top.indexOf("/status") != -1) {
          RecordCurrentProfile(topic, String(call));
    }
    
    else if (top.indexOf("profiles/change") != -1 || top.indexOf("profiles/run") != -1) {
        ProfilesSettings(topic, String(call));
    }

    else if (top.indexOf("profiles/log") != -1) {
        ProfilesLog(topic, String(call));
    }

  //*********
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "reconnecting");
      // ... and resubscribe
      mySubscribtion();
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
