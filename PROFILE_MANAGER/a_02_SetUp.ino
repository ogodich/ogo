void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Trying again");
    delay(500);
    if(millis() > 5000) break;
  }

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  otaInit();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  dht.begin();
  pinMode(LED_BUILTIN, OUTPUT);
}
