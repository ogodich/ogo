String newProfileName = "";
String jSonProfiles = "";
String strArchLog = "";
String lg = "";
String logTopic = "profiles/log";
char jProfilesTopic[] = "profiles/run/jSon";
const char currentProfileTopic[] = "profiles/run/currentProfileNo";
const int cLogSize = 1000;
bool logEnable=false;
byte currentProfileNo = 0;
bool enableRecordProfile = false;


  //Настройки
  void ProfilesSettings(char* top, String pay){

    
    //ВКЛ-ВЫКЛ записи профиля
    if (strcmp(top,"profiles/change/record")==0){
              enableRecordProfile = pay.toInt();

              lg=BOARD_NAME; lg+=":\n   ";
              lg+="Profile recording was succesfully ";
              lg+=enableRecordProfile ? "enabled." : "disabled.";
              if(enableRecordProfile){
                lg+=" Profile #";
                lg+=currentProfileNo;
                lg+=" is recording now";
              }
              addLog(lg);
    }


    
    //Выбираем профиль из списка
    else if (strcmp(top,"profiles/run/currentProfileNo")==0){
              currentProfileNo = pay.toInt();
              
              Serial.print("currentProfileNo=");
              Serial.println(currentProfileNo);
    }
  


    //Применяем текущий профиль
    else if (strcmp(top,"profiles/run/apply")==0){
              
              lg=BOARD_NAME; lg+=":\n   ";
              lg+="Profile #";
              lg+=currentProfileNo;
              lg+=" has been applied";
              addLog(lg);
    }

    
    
    //Создаем новый профиль
    else if (strcmp(top,"profiles/change/create")==0){
              newProfileName = pay;

              ModifyProfiles("create");
              
              lg=BOARD_NAME; lg+=":\n   ";
              lg+="New profile '";
              lg+=newProfileName;
              lg+="' was succesfully created by number #";
              lg+=currentProfileNo;
              addLog(lg);
    }
  


    //Переименовываем текущий профиль
    else if (strcmp(top,"profiles/change/rename")==0){
              newProfileName = pay;

              ModifyProfiles("rename");
              
              lg=BOARD_NAME; lg+=":\n   ";
              lg+="Profile #";
              lg+=currentProfileNo;
              lg+=" has been renamed to '";
              lg+=newProfileName;
              lg+="'";
              addLog(lg);
    }



    //Удаляем текущий профиль
    else if (strcmp(top,"profiles/change/delete")==0){

              ModifyProfiles("delete");
            
              lg=BOARD_NAME; lg+=":\n   ";
              lg+="Profile #";
              lg+=currentProfileNo;
              lg+=" has been succesfully deleted!";
              addLog(lg);
    }

    else if (strcmp(top,"profiles/change/clean")==0){
              ModifyProfiles("clean");
            
              lg=BOARD_NAME; lg+=":\n   ";
              lg+="Profile #";
              lg+=currentProfileNo;
              lg+=" has been succesfully cleaned!";
              addLog(lg);
    }




    //Сохраняем у себя JSon
    else if (strcmp(top,"profiles/run/jSon")==0){
              jSonProfiles = pay;
              
              Serial.println("jSonProfiles was recieved:");
              //Serial.println(jSonProfiles);
    }



    //ВКЛ-ВЫКЛ логирования
    else if (strcmp(top,"profiles/change/log_enable")==0){
          logEnable=pay.toInt();
          Serial.print("log was ");
          Serial.println(logEnable ? "enabled." : "disabled.");
    }
    
    
    } //Конец настроек    /////////////////////////////////////////
    
    void ProfilesLog(char* top, String pay){
        //Сохраняем у себя log
        if (strcmp(top,"profiles/log")==0){
            strArchLog = pay;
            
            Serial.print("strArchLog was recieved.");
            Serial.println(strArchLog);

            client.unsubscribe("profiles/log");
            Serial.println("I have unsubscribed from logTopic.");
        }
        else if (strcmp(top,"profiles/log/add")==0){
            addLog(pay);
        }
    }


  
  







  void myBlink(int cycles){
        bool tempStatus = false;
        for(int i=0;i<=cycles;i++){
          tempStatus =!tempStatus;
          digitalWrite(BUILT_IN_LED, tempStatus);
          delay(30);
        }
  }

  void addLog (String newMessage){
      if(logEnable){
            strArchLog+="\n";
            strArchLog+=newMessage;
            int startSubstr=strArchLog.length() - cLogSize;
            if(startSubstr<0)startSubstr=0;
            strArchLog=strArchLog.substring(startSubstr);
      
            
            client.publish(&logTopic[0u], &strArchLog[0u], true);
            Serial.print(millis());
            Serial.print(" - i have send new log message: ");
            Serial.println(newMessage);
      }
  }

  void RecordCurrentProfile(char* top, String pay){
    //Serial.print("enableRecordProfile=");
    //Serial.println(enableRecordProfile);
    if(enableRecordProfile){
            //имя добавляемого устройства
            String locDeviceName = String(top);
            locDeviceName.replace("devices/", "");
            locDeviceName.replace("/status", "");
        
            ModifyProfiles("addDevice",locDeviceName,pay);
    }
  }

  void ModifyProfiles(String parModeType){
    ModifyProfiles(parModeType, "", "");
  }
  void ModifyProfiles(String parModeType, String parDeviceName, String parDeviceStatus){

      //Создаем jSon объект
      DynamicJsonBuffer jsonBuffer;
      JsonObject& jProfiles = jsonBuffer.parseObject(jSonProfiles);
      //jProfiles.prettyPrintTo(Serial);

      
      if(parModeType=="create"){
              //Создаем новый профиль
              int locNewProfileNo = jProfiles.size()+1;
              String toRaw = "{\"label\":\"";
                      toRaw += newProfileName;
                      toRaw += "\",\"devices\":{}}";
              jProfiles[String(locNewProfileNo)] = RawJson(toRaw);

              //Присваиваем текущему профилю новый профиль
              currentProfileNo = locNewProfileNo;
              client.publish(currentProfileTopic, &String(currentProfileNo)[0u], true);
      }
      else if(parModeType=="rename"){
              //Переименовываем текущий профиль
              jProfiles[String(currentProfileNo)]["label"] = newProfileName;
      }
      else if(parModeType=="clean"){
              //Очищаем настройки текущего профиля
              String locCurProfileName = jProfiles[String(currentProfileNo)]["label"];
              String toRaw = "{\"label\":\"";
                      toRaw += locCurProfileName;
                      toRaw += "\",\"devices\":{}}";
              jProfiles[String(currentProfileNo)] = RawJson(toRaw);
              //jProfiles[String(currentProfileNo)]["devices"] = "{}";
      }
      else if(parModeType=="delete"){
              int curSize = jProfiles.size();
              //Удаляем текущий профиль
              jProfiles.remove(String(currentProfileNo));

              //Пересортировываем
              int next = currentProfileNo+1;
              for(int i=next; i<=curSize; i++){
                  String key = String(i-1);
                  jProfiles.set(key, "");

                  String value = "";
                  jProfiles[String(i)].printTo(value);
                  jProfiles[key]=RawJson(value);

                  jProfiles.remove(String(i));
              }
      }
      else if(parModeType=="addDevice"){
            jProfiles[String(currentProfileNo)]["devices"][parDeviceName] = parDeviceStatus;

            lg=BOARD_NAME; lg+=":\n   ";
            lg+="Added a device '";
            lg+=parDeviceName;
            lg+="' whith status '";
            lg+=parDeviceStatus;
            lg+="' to profile #";
            lg+=currentProfileNo;            
            addLog(lg);
      }
      
      //Сохраняем измененный jSon в очередь
      size_t size = jProfiles.measureLength() + 1;
      //char chrToPrint[size+200];
      //jProfiles.prettyPrintTo(chrToPrint, size+200);
      //client.publish(jProfilesTopic, chrToPrint, true);    

      String toPrint;
      jProfiles.prettyPrintTo(toPrint);
      client.publish(jProfilesTopic, &toPrint[0u], true);   

      jProfiles.prettyPrintTo(Serial);
  }
