void setup() {
  Serial.begin(115200);
  Serial.println("Booting");

  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed!");
    delay(500);
    if(millis() > 5000) break;
  }

  if (WiFi.waitForConnectResult() == WL_CONNECTED) {  
    Serial.println("Ready");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  
    client.setServer(mqtt_server, 1883);
    client.setCallback(callback);
  
    otaInit();
  }

  pinMode(lights[0], OUTPUT);
  digitalWrite(lights[0], HIGH);

  pinMode(lightSensorPin, INPUT);
  
  //умные светодиоды
  smartLed.begin(); // This initializes the NeoPixel library.
  smartLed.setBrightness(255); // set brightness
  smartLed.show(); // Initialize all pixels to 'off'

  //Прерывания по кнопкам
  buttonHandler.buttonsNumber=numButtons; //default is 10
  for(int i=0; i<buttonHandler.buttonsNumber; i++) {
	  buttonHandler.buttonPins[i]=buttons[i];
	  attachInterrupt(buttonHandler.buttonPins[i], interruptButtons, RISING);
  }
  
  
}
