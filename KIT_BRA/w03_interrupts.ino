//процедура обработки прерывайний
void interruptButtons() {
  //Отображение информации о нажатиях
  buttonHandler.showPresses();

  if(!gameStarted){
  //КОМБИНАЦИИ
      //включить радио----------------------------------------------------------------------------------------------
      if(buttonHandler.longPresses[1] ==1 && buttonHandler.shortPresses[0]==1) async.addTask("radio", "2", false);   
    
      //Понижаем громкость, если зажата II и нажата больше одного I
      else if (buttonHandler.longPresses[1]==1 && buttonHandler.shortPresses[0]>1)  {
        snprintf (msg, 5, "%d", (buttonHandler.shortPresses[0]-1)*(-1));
        async.addTask("volume_change", msg, false);
      }
      //Повышаем громкость, если зажата II и нажата больше одного III
      else if (buttonHandler.longPresses[1]==1 && buttonHandler.shortPresses[2]>1)  {
        snprintf (msg, 5, "%d", (buttonHandler.shortPresses[2]-1));
        async.addTask("volume_change", msg, false);
      }

  //ПЕРВАЯ кнопка-------------------------------------------------------------------------------------------------  
      else if (buttonHandler.shortPresses[0]==1) async.addTask("devices/coohoo/status/binar", BOARD_NAME, false);
      else if (buttonHandler.longPresses[0]==1) {
        lightStat[1] = !lightStat[1];
        digitalWrite(lights[1], !lightStat[1]);
        refreshIndicators();
        snprintf (msg, 75, "%d", lightStat[1]);
        async.addTask("devices/kitBra/status", msg, true);
      }
    
  //ВТОРАЯ кнопка-------------------------------------------------------------------------------------------------
      else if (buttonHandler.shortPresses[1]==1)async.addTask("devices/kitchen/status/binar", BOARD_NAME, false);
      else if (buttonHandler.longPresses[1]==1) async.addTask("devices/kitStrip1/status", "2", true);
    
  //ТРЕТЬЯ кнопка-------------------------------------------------------------------------------------------------
      else if (buttonHandler.shortPresses[2]==1)  async.addTask("devices/kitStrip1/status/binar", BOARD_NAME, false);
      else if (buttonHandler.longPresses[2]==1)   async.addTask("devices/kitStrip2/status/binar", BOARD_NAME, false);
  }
  
  //Игра  
      else if(gameStarted){
      if      (buttonHandler.shortPresses[0]==1 || buttonHandler.longPresses[0]==1) client.publish("GAME/pressed", "9");
      else if (buttonHandler.shortPresses[1]==1 || buttonHandler.longPresses[1]==1) client.publish("GAME/pressed", "10");
      else if (buttonHandler.shortPresses[2]==1 || buttonHandler.longPresses[2]==1) client.publish("GAME/pressed", "11");
  }




}
