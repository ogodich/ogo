void get_brightness(){
  if(autoBrightnessEnable){
          long now = millis();  
          if (now - SwitchBrightTimer > 3000) {
                  CurLigthSensorVal = analogRead(lightSensorPin);
                  /*Serial.print("A0=");
                  Serial.print(analogRead(lightSensorPin));
                  Serial.print(", millis=");
                  Serial.println(millis());*/
                  
                  if (fabs(CurLigthSensorVal-PrevLigthSensorVal) > 40){
                    //В зависимости от схемы подключения фоторезистера возможна как прямая зависимость от яркости, так и обратная
                    //CurrenSwitchtBrightness = map(CurLigthSensorVal,      0, 800,      3, 100);
                    CurrenSwitchtBrightness = map(CurLigthSensorVal,      1024, 50,      3, 100);
                    CurrenSwitchtBrightness = CurrenSwitchtBrightness < 0 ? 0 : (CurrenSwitchtBrightness > 100 ? 100 : CurrenSwitchtBrightness);
              
                    //Управляем яркостью выключателя
                    snprintf (msg, 5, "%d", CurrenSwitchtBrightness);
                    client.publish("settings/switch/corner/bright", msg, true);
                          
                    //Передаем также значение сенсора для мониторинга
                    snprintf (msg, 5, "%d", CurLigthSensorVal);
                    client.publish("settings/switch/corner/bright/log", msg, true);  
              
                    /*Serial.print("CurLigthSensorVal=");
                    Serial.print(CurLigthSensorVal);
                    Serial.print(", PrevLigthSensorVal=");
                    Serial.print(PrevLigthSensorVal);
                    Serial.print(", razn=");
                    Serial.print(fabs(CurLigthSensorVal-PrevLigthSensorVal));
                    Serial.print(",");
                    Serial.println(millis());*/
              
                    PrevLigthSensorVal = CurLigthSensorVal;
                  }
              
                  SwitchBrightTimer = now;
          }
  }
}


void refreshIndicators(){

    //вытяжка, угловой
    if      (lightStat[0] && lightStat[1])    mySetColor(0, 250,200,0);
    else if (lightStat[0] && !lightStat[1])   mySetColor(0, 0,200,0);
    else if (!lightStat[0] && lightStat[1])   mySetColor(0, 0,0,200);
    else if (!lightStat[0] && !lightStat[1])  mySetColor(0, 250,0,0);

    //кухня верхний
    if      (lightStat[2])   mySetColor(1, 0,200,0);
    else if (!lightStat[2])  mySetColor(1, 250,0,0);
    
    //Ленты
    if      (lightStat[3] && lightStat[4])    mySetColor(2, 250,200,0);
    else if (lightStat[3] && !lightStat[4])   mySetColor(2, 0,200,0);
    else if (!lightStat[3] && lightStat[4])   mySetColor(2, 0,0,200);
    else if (!lightStat[3] && !lightStat[4])  mySetColor(2, 250,0,0);

/*
  
  if      (lightStat[0] && offline)   smartLed.setPixelColor(0, smartLed.Color(0,200,50));
  else if (lightStat[0])              mySetColor(0, 0,200,0);
  else if (!lightStat[0] && offline)  smartLed.setPixelColor(0, smartLed.Color(0, 250,0,50));
  else if (!lightStat[0])             mySetColor(0, 250,0,0);
  
  //2
  if      (offline)        smartLed.setPixelColor(1, smartLed.Color(30,0,5));  
  else if (lightStat[2])   mySetColor(1, 0,200,0);
  else if (!lightStat[2])  mySetColor(1, 250,0,0);

  //3
  if      (lightStat[1] && offline)   smartLed.setPixelColor(2, smartLed.Color(0,200,50));
  else if (lightStat[1])              mySetColor(2, 0,200,0);
  else if (!lightStat[1] && offline)  smartLed.setPixelColor(2, smartLed.Color(250,0,50));
  else if (!lightStat[1])             mySetColor(2, 250,0,0);
   */
  
  
  smartLed.show(); // This sends the updated pixel color to the hardware.
}

void mySetColor(int pix, int r, int g, int b){
  smartLed.setPixelColor(pix, smartLed.Color(r * CurrenSwitchtBrightness / 100, g * CurrenSwitchtBrightness / 100, b * CurrenSwitchtBrightness / 100));
}

void animation_1 (){
  for (int i=0; i<250; i++){
    smartLed.setPixelColor(0, smartLed.Color(250-i, i < 200 ? i : 200, 0));
    delay(3);    
    smartLed.show();
    i=i+10;
  }
}

void animation_2 (){
  for (int i=0; i<250; i++){
    smartLed.setPixelColor(0, smartLed.Color(i, 200 - (i < 200 ? i : 200), 0));
    delay(3);    
    smartLed.show();
    i=i+10;
  }
}

void switchLights(){
  digitalWrite(lights[0], !lightStat[1]);
  printStatus();
}

void printStatus(){
  Serial.println(millis());
  for(int i=0; i < sizeof lightStat; i++){
    char boof[100];
    snprintf(boof, 100, "lightStat[%d]=%d;", i, lightStat[i]);
    Serial.println(boof);
  }
}

void otaInit(){
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
   ArduinoOTA.setPassword((const char *)BOARD_PASS);

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();  
}
