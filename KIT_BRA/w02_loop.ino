void loop() {
  //my_multy_switcher();	//Вместо этого теперь обработчик прерываний - interruptButtons()
  mqtt_loop();
  if(!gameStarted)get_brightness();
  if(!offline)ArduinoOTA.handle();
  
  //Отправка асинхронных сообщений
  if(async.doTask()) {
    char msg[100];
    sprintf(msg,"%d\tLOOP\tI have published %s message '%s' to the topic '%s'",
      millis(),
      async.retain ? "retain" : "not retain",
      async.value,
      async.topic);
    if(client.publish(async.topic, async.value, async.retain)) Serial.println(msg);  
  }

}
