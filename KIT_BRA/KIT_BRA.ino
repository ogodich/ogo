#define BOARD_NAME "KITCHEN_BRA"
#define BOARD_PASS "kitbra"
//-=-=-=-=-=-=-=-=-=-=-=-

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#include <ArduinoJson.h>

#include <myLib.h>

/*_______________________________________________________________________________________________________________________________*/
const String DEVICES[] = {"kitBra"};
const char* ssid = "ScorpGOS";
const char* password = "9031124945";
const char* mqtt_server = "192.168.1.1";

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
int lights[1] 	= {D2};
const int numButtons = 3; int buttons[numButtons] 	= {D6, D5, D7};
int smartLedPin = D8;
const int lightSensorPin = A0;
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

boolean lightStat[5] = {false, false, false, false, false};

WiFiClient 		espClient;
PubSubClient	client(espClient);
AsyncTask 		async;
ButtonHandler 	buttonHandler;

void ICACHE_RAM_ATTR interruptButtons ();		//это обязательная хрень после перехода на IDE 1.8.10. Что-то связанное с памятью про прерывания

char msg[50];

Adafruit_NeoPixel smartLed = Adafruit_NeoPixel(numButtons, smartLedPin, NEO_GRB + NEO_KHZ800);

boolean offline = false;
long mqttLastReconnect = 0;

int CurrenSwitchtBrightness = 100;

boolean gameStarted = false;
int gameCurrButton = 3;

int arrGameNumber[15] = {-1,    -1,-1,-1,-1,-1,    -1,-1,-1,    0,1,2,      -1,-1,-1};

long SwitchBrightTimer = 0.0;
int PrevLigthSensorVal = 0;
int CurLigthSensorVal = 0;
bool autoBrightnessEnable = false;

//home profiles-------------------------------------------
String currentProfileNo = "";
String jSonProfiles = "";
const int cDevices = sizeof(DEVICES) / sizeof(DEVICES[0]);
const char addLogTopic[]="profiles/log/add";
bool logEnable=false;
//--------------------------------------------------------
