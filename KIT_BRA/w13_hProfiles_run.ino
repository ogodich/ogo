  //Подписка на очереди устройств этой платы
  void subscribeToDevicesStatus(){
        for(int i=0; i<cDevices; i++){
              String deviceTopic = "devices/%s/status/#";
              deviceTopic.replace("%s",DEVICES[i]);
              client.subscribe(&deviceTopic[0u]);
        }
  }

  void RunProfiles(char* top, String pay){
        //сохраняю номер текущего профиля
        if (strcmp(top,"profiles/run/currentProfileNo")==0){
            currentProfileNo = pay;
            Serial.print("currentProfileNo = ");
            Serial.println(currentProfileNo);
        }
        //сохраняю (но не парсю) настройки всех профилей
        else if (strcmp(top,"profiles/run/jSon")==0){
            jSonProfiles = pay;
            Serial.println("jSonProfiles saved");
        }
        //применяю текущий профиль для моих устройств:
            //Распарсиваю все профили
            //Пробегаю по всем устройствам этой платы
                //если для устройства есть настройка, применяю её
                    //если включено логирование, отправляю сообщение
        else if (strcmp(top,"profiles/run/apply")==0){
              DynamicJsonBuffer jsonBuffer;
              JsonObject& jProfiles = jsonBuffer.parseObject(jSonProfiles);
              //jProfiles.prettyPrintTo(Serial);

              //пробегаемся по всем устройствам этой платы
              for(int i=0; i<cDevices; i++){
                    const char* locDevStatus = jProfiles[currentProfileNo]["devices"][DEVICES[i]];
                    if(locDevStatus != nullptr){
                          //Изменяем статус устройства, если он есть в текущем профиле
                          String locDeviceTopic="devices/%s/status";
                          locDeviceTopic.replace("%s", DEVICES[i]);
                          client.publish(&locDeviceTopic[0u], locDevStatus, true);

                          //логируем
                          if(logEnable){
                                String lg=BOARD_NAME; lg+=":\n   ";
                                       lg+="device '";
                                       lg+= DEVICES[i];
                                       lg+="' applied status to '";
                                       lg+=locDevStatus;
                                       lg+="'";
                                client.publish(addLogTopic, &lg[0u]);
                          }
                    }
              }
        }
        
  }
