void mqtt_loop(){
  if (WiFi.waitForConnectResult() == WL_CONNECTED) {
    if (!client.connected()) {
      reconnect();
    }
    else client.loop();
  }  
}

void mySubscribtion(){  
  client.subscribe("devices/coohoo/status");
  client.subscribe("devices/kitchen/status");
  client.subscribe("devices/kitStrip1/status");
  client.subscribe("devices/kitStrip2/status");
  
  client.subscribe("settings/switch/corner/bright/#");
  client.subscribe("GAME/#");

  subscribeToDevicesStatus();
  client.subscribe("profiles/run/#");
  client.subscribe("profiles/change/log_enable");

}

void reconnect() {
  if (!offline || (offline && millis() - mqttLastReconnect > 5000)){
    Serial.print("WiFi.waitForConnectResult()=");
    Serial.println(WiFi.waitForConnectResult());
    Serial.print("Attempting MQTT connection...");
    
    // Если удалось подключиться
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      //Если пришли из офлайна, отправляем изменения
      if(offline) sendFromOffline();
      
      offline = false;
      // ... and resubscribe
      mySubscribtion();
    
    // Если не удалось подключиться
    } else {
      Serial.print("failed, rc=");
      Serial.println(client.state());
      offline = true;
      mqttLastReconnect = millis();

      refreshIndicators(); //только для устройств со светодиодами!!!
    }
       
  }
}

void sendFromOffline(){
    Serial.println("nothing");
}



void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();
//*********************************************************************************************************************************
    if (strcmp(topic,"devices/kitBra/status/binar")==0){
          lightStat[1] = !lightStat[1];
          snprintf (msg, 75, "%d", lightStat[1]);
          client.publish("devices/kitBra/status", msg, true);
    }
    else if (strcmp(topic,"devices/kitBra/status")==0){
          lightStat[1] = call.toInt();
          switchLights();
          refreshIndicators();
    }

    //----------ПРОФИЛИ
    else if (String(topic).indexOf("profiles/run/") != -1) {
        RunProfiles(topic, String(call));
    }
    else if (strcmp(topic,"profiles/change/log_enable")==0){
        logEnable = call.toInt();
    }//----------------

  else if (strcmp(topic,"devices/coohoo/status")==0){
      lightStat[0] = call.toInt();
      refreshIndicators();
  }
  
  if (strcmp(topic,"devices/kitchen/status")==0){
      lightStat[2] = call.toInt();
      refreshIndicators();
  }

  else if (strcmp(topic,"devices/kitStrip1/status")==0){
      lightStat[3] = call.toInt();
      refreshIndicators();
  }
  else if (strcmp(topic,"devices/kitStrip2/status")==0){
      lightStat[4] = call.toInt();
      refreshIndicators();
  }


  else if (strcmp(topic,"settings/switch/corner/bright/auto")==0){
    autoBrightnessEnable = call.toInt();
  }
  else if (strcmp(topic,"settings/switch/corner/bright")==0){
    CurrenSwitchtBrightness = call.toInt();
    refreshIndicators();
  }



  else if (String(topic).indexOf("GAME/") != -1) {
    game(topic, call);
  }


//*********************************************************************************************************************************
}
