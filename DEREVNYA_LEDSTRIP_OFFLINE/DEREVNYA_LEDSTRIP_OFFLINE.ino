#define BOARD_NAME "DEREVNYA_STRIP"
#define BOARD_PASS "strip"
//*************************************************************************************************************

#include <ESP8266WiFi.h>
#include <WiFiClient.h>

#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <Adafruit_NeoPixel.h>

//*************************************************************************************************************

#define PIN            D7
#define PIN2           D6
#define PIX_NUM        96
#define PIX_NUM_2      143
#define STRIPTYPE      "RGBW"

//*************************************************************************************************************
const char* ssid = "berloga";
const char* password = "plutonnastya";

Adafruit_NeoPixel strip = Adafruit_NeoPixel (PIX_NUM,   PIN,  NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(PIX_NUM_2, PIN2, NEO_GRBW + NEO_KHZ800);

int buttons[2] = {D2, D1};
const int cProfiles = 7;
byte arrBase[cProfiles][4]={
                      {0,0,0,0},
                      {255,0,0,228},
                      {50,50,50,50},
                      {250,0,0,50},
                      {255,0,255,50},
                      {0,255,0,50},
                      {255,255,255,255}
                    };

int currProfile = 0;
int currProfile2 = 0;
