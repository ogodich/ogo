void simpleSetProfile(int parProfile){
  Serial.println("\nrun simpleSetProfile");
  currProfile = parProfile;
  strip.setPixelColor(0, 0,0,0,0);
  for (int i=1; i<PIX_NUM; i++){
    strip.setPixelColor
      (
        i,
        arrBase[currProfile][0],
        arrBase[currProfile][1],
        arrBase[currProfile][2],
        arrBase[currProfile][3]
      );
  strip.show();
  }
  strip.show();
  Serial.print("currProfile=");
  Serial.println(currProfile);
}

void simpleNextProfile(){
  Serial.println("\nrun simpleNextProfile");
  currProfile ++;
  currProfile = currProfile > cProfiles ? 1 : currProfile;
  simpleSetProfile(currProfile);
  Serial.print("currProfile=");
  Serial.println(currProfile);
}

void simpleSetProfile2(int parProfile){
  Serial.println("\nrun simpleSetProfile");
  currProfile2 = parProfile;
  strip2.setPixelColor(0, 0,0,0,0);
  for (int i=0; i<PIX_NUM_2; i++){
    strip2.setPixelColor
      (
        i,
        arrBase[currProfile2][0],
        arrBase[currProfile2][1],
        arrBase[currProfile2][2],
        arrBase[currProfile2][3]
      );
  strip2.show();
  
  }
  strip2.show();
  Serial.print("currProfile2=");
  Serial.println(currProfile2);
}

void simpleNextProfile2(){
  Serial.println("\nrun simpleNextProfile2");
  currProfile2 ++;
  currProfile2 = currProfile2 > cProfiles ? 1 : currProfile2;
  simpleSetProfile2(currProfile2);
  Serial.print("currProfile2=");
  Serial.println(currProfile2);
}



void wifiPrint(){
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if(millis() > 8000) break;
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}



void otaInit(){
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
   ArduinoOTA.setPassword((const char *)BOARD_PASS);

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();  
}
