void setup(void){
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  wifiPrint();

  otaInit();

  //Первая лента
  strip.begin(); // This initializes the NeoPixel library.
  strip.setBrightness(255); // set brightness
  strip.show(); // Initialize all pixels to 'off'

  //Вторая лента
  strip2.begin(); // This initializes the NeoPixel library.
  strip2.setBrightness(255); // set brightness
  strip2.show(); // Initialize all pixels to 'off'

}
