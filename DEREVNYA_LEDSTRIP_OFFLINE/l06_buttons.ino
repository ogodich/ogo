void newClick(){
  const int cDelay=20;
  const int cFastClickTimes=3;
  const int cLongClickTimes=60;
  
  boolean fastClick[2]={false,false};
  int it[2]={0,0};
  
  while (digitalRead(buttons[0])){
    it[0]++;

    //если одиночный или повторяющийся раз в 'cLongClickTimes' ДЛИННЫЙ клик:
    if (it[0] >= cLongClickTimes && (it[0] % cLongClickTimes) == 0){
      fastClick[0]=false;   //отменяем короткий клик
      Serial.println("longClick[0]");
      simpleNextProfile();
    }

    else if (it[0] >= cFastClickTimes && it[0] < cLongClickTimes) {
      fastClick[0]=true;
    }

    delay(cDelay);
  }

  while (digitalRead(buttons[1])){
    it[1]++;

    //если одиночный или повторяющийся раз в 'cLongClickTimes' ДЛИННЫЙ клик:
    if (it[1] >= cLongClickTimes && (it[1] % cLongClickTimes) == 0){
      fastClick[1]=false;   //отменяем короткий клик
      Serial.println("longClick[1]");
      simpleNextProfile2();
    }

    else if (it[1] >= cFastClickTimes && it[1] < cLongClickTimes) {
      fastClick[1]=true;
    }

    delay(cDelay);
  }

  if(fastClick[0]){
    Serial.println("fastClick[0]");
    currProfile = currProfile > 0 ? 0 : 1;
    simpleSetProfile(currProfile);
  }
  if(fastClick[1]){
    Serial.println("fastClick[1]");
    currProfile2 = currProfile2 > 0 ? 0 : 1;
    simpleSetProfile2(currProfile2);
  }
  
} //end newClick()
