#define BOARD_NAME "HALL_STRIP"
#define BOARD_PASS "strip"
//*************************************************************************************************************

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <Adafruit_NeoPixel.h>

#include <ArduinoSort.h>

#include <PubSubClient.h>

#include <stdlib.h>

#include <ArduinoJson.h>

//*************************************************************************************************************

#define PIN            D6
#define PIN2           D5
#define DOOR_PIN       D7
#define PIX_NUM        51
#define STRIPTYPE      "RGB"

//*************************************************************************************************************
const String DEVICES[] = {"hallStrip1"};

const char* ssid = "ScorpGOS";
const char* password = "9031124945";

Adafruit_NeoPixel strip = Adafruit_NeoPixel(51, PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(18, PIN2, NEO_GRB + NEO_KHZ800);

boolean switchedStrip = false;
//boolean switchedStrip2 = false;

int mLeds[217];
int countLeds[10];

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;

boolean startBoot = false;
double startTime = 0;

char msg[50];
int value = 0;

long lastBrightMsg = 0;
int currentBrightness = 0;
int prevBrightness = 0;

int motivationBrightness = 0;
int secondStripBrigth = 110;

String debug;
char deb[150];

boolean stripConfParts[7];
int     stripConfR [6][6];
int     stripConfG [6][6];
int     stripConfB [6][6];
int     stripConfW [6][6];
int     stripConfBr[6][6];
//int currPreset = 1;
int prevPreset = 1;

boolean runTimer = false;
long timerRefresh = 0;
long timerStop = 0;
int timerCurLed = 0;
int timerPrevLed = -1;
int timerPassTime = 0;
int timerInterval = 0;

int strip2ProfileNo = 0;

int testR = 0;
int testG = 0;
int testB = 0;


char charCall[4000];      //парсинг mqtt

//констаны для ленты
const int PartsNum = 4;
boolean   PartsArray[PartsNum+1];
const int PartsBegins    [PartsNum] = {38,25,13,0};//{0,  13, 25, 38};
const int PartsEnds      [PartsNum] = {50,37,24,12};//{12, 24, 37, 50};
const int Middles        [4]  = {6, 19, 31, 45};

//Настройки ленты
int currPreset = 1; //пресет по умолчанию
char charOut[4000];       //хранение настроек ленты
int arrRGBW[4][PIX_NUM];  //хранение настроек ленты
String ConfMode = "full";
int CurrIntStart = 1;
int CurrIntLength = 1;
uint32_t CurrRGB = 7405568;
int CurrWhite = 0;
int CurrBrightness = 100;
int BrightnessStep = 1;
uint32_t IntervalRGB = 7405568;
int IntervalWhite = 0;

//home profiles-------------------------------------------
String currentProfileNo = "";
String jSonProfiles = "";
const int cDevices = sizeof(DEVICES) / sizeof(DEVICES[0]);
const char addLogTopic[]="profiles/log/add";
bool logEnable=false;
//--------------------------------------------------------
