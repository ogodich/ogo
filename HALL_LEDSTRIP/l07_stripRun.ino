/*****************************************************************************************************************/
void switch_to_preset(char* top, int preset){
  currPreset = preset;
  
  //Делаем исключение для 11го профиля
  if (currPreset == 11){
    doStrip();
  }
  else{
    strip_off();
    char myTop [100];
    sprintf(myTop, "STRIP/db/%s/%d",  BOARD_NAME,preset);      //генерируем имя очереди в соответствии с запрошенным пресетом
    client.subscribe(myTop);                                   //подписываемся на эту очередь, чтобы вычитать из неё retained
    client.unsubscribe(myTop);
  }
}
/*****************************************************************************************************************/
void load_preset(char* top, char* pay){
  for(int t=0; t<=3; t++) for(int z=0; z<PIX_NUM; z++) arrRGBW[t][z]=0;
  int i=0;
  int y=0;
  String buf;

  //цикл до конца сообщения
  while(pay[i]!=0){
    //накапливаем, пока не встретили разделитель
    if(pay[i]!=',') buf = buf + pay[i];    
    //если встретили разделитель
    else{
      int numX = (y+4) % 4;        //вычисляем первую разрядность массива (R/G/B/W)
      int numY = (y/4);            //вычисляем вторую разрядность массива (№ п/п)
      arrRGBW[numX][numY]=buf.toInt();
      //char iter[100];
      //snprintf(iter,100, "arrRGBW[%d][%d]=%d", numX,numY,arrRGBW[numX][numY]);
      //Serial.println(iter);

      y++;    //"перводим взгляд" на следующий элемент результирующего массива
      buf=""; //и обнуляем накопление
    }
    //следующий символ сообщения
    i++;
  }

  //Зажигаем ленту
  for (int i=0; i<PIX_NUM; i++) strip.setPixelColor(i, strip.Color(arrRGBW[0][i], arrRGBW[1][i], arrRGBW[2][i], arrRGBW[3][i]));
  strip.show();

  
  //печатаем результат
  /*Serial.print("r= ");
  for(int i=0; i<PIX_NUM; i++){
    Serial.print(i);
    Serial.print(":");
    Serial.print(arrRGBW[0][i]);
    Serial.print(" ");
  }
  Serial.println("");

  Serial.print("g= ");
  for(int i=0; i<PIX_NUM; i++){
    Serial.print(i);
    Serial.print(":");
    Serial.print(arrRGBW[1][i]);
    Serial.print(" ");
  }
  Serial.println("");

  Serial.print("b= ");
  for(int i=0; i<PIX_NUM; i++){
    Serial.print(i);
    Serial.print(":");
    Serial.print(arrRGBW[2][i]);
    Serial.print(" ");
  }
  Serial.println("");

  Serial.print("w= ");
  for(int i=0; i<PIX_NUM; i++){
    Serial.print(i);
    Serial.print(":");
    Serial.print(arrRGBW[3][i]);
    Serial.print(" ");
  }
  Serial.println("");*/
 }
 /*****************************************************************************************************************/
void doStrip(){
  if (STRIPTYPE=="RGBW") for (int i=0; i<PIX_NUM; i++) strip.setPixelColor(i, strip.Color(arrRGBW[0][i], arrRGBW[1][i], arrRGBW[2][i], arrRGBW[3][i]));
  if (STRIPTYPE=="RGB")  for (int i=0; i<PIX_NUM; i++) strip.setPixelColor(i, strip.Color(arrRGBW[0][i], arrRGBW[1][i], arrRGBW[2][i]));
  strip.show();
}
void do_rgb(int from, int to, uint32_t sourceInt){
  for(int i=from;i<=to;i++){
    arrRGBW[0][i]=(sourceInt / 256) / 256;
    arrRGBW[2][i]=(sourceInt / 256) % 256;    //B
    arrRGBW[1][i]=sourceInt % 256;            //G
  }  
}
void do_white(int from, int to, int sourceInt){
  for(int i=from;i<=to;i++) arrRGBW[3][i]=sourceInt;
}
void strip_off(){
  for (int i=0; i<PIX_NUM; i++) strip.setPixelColor(i, 0,0,0,0);
  strip.show();
  for(int i=0; i<=18; i++)  strip2.setPixelColor(i, strip2.Color(0, 0, 0));
  strip2.show();
  
}
