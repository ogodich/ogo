

void setup(void){
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  wifiPrint();
//  myWebQueries();
  

  otaInit();

  //pinMode(leds[0], OUTPUT);
  //pinMode(leds[1], OUTPUT);
  //pinMode(leds[2], OUTPUT);

  //Первая лента
  strip.begin(); // This initializes the NeoPixel library.
  strip.setBrightness(255); // set brightness
  strip.show(); // Initialize all pixels to 'off'

  //Вторая лента
  strip2.begin(); // This initializes the NeoPixel library.
  strip2.setBrightness(255); // set brightness
  strip2.show(); // Initialize all pixels to 'off'

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  client.subscribe("radio");
  client.subscribe("motivation_green");
  client.subscribe("motivation_red");
  client.subscribe("motivation_blue");

  pinMode(LED_BUILTIN, OUTPUT);

  pinMode(D7, INPUT);
}
