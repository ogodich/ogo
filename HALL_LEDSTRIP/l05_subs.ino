boolean doorIs = false;
boolean doorWas = false;
void check_the_door(){
  //Проверяем состояние двери
  doorIs=digitalRead(DOOR_PIN);
  
  //Небольшая проверка на дребезг
  if (doorIs!=doorWas){
    delay(500);
    doorIs=digitalRead(DOOR_PIN);
  }
  
  //Если показания после дребезга актуальны:
  if (doorIs!=doorWas){
    snprintf (msg, 5, "%d", doorIs);
    client.publish("doors/hall", msg);
    Serial.print("doorIs=");
    Serial.println(doorIs);

    //Если дверь открыта
    if(doorIs == false) {
      //Зажигаем участок на первой ленте
      for(int i=13; i<=37; i++) strip.setPixelColor(i, strip.Color(255, 255, 255));
      strip.show();

      //Зажигаем вторую ленту профилем, следующим по очереди // R B G
      
      strip2ProfileNo ++;
      if (strip2ProfileNo>5) strip2ProfileNo = 1;
      
      switch(strip2ProfileNo){
        case 1:
          for(int i=0; i<=2; i++)    strip2.setPixelColor(i, strip2.Color(255, 0, 0));
          for(int i=3; i<=4; i++)    strip2.setPixelColor(i, strip2.Color(0, 0, 255));
          for(int i=5; i<=7; i++)    strip2.setPixelColor(i, strip2.Color(255, 0, 0));
          for(int i=8; i<=9; i++)   strip2.setPixelColor(i, strip2.Color(0, 0, 255));
          for(int i=10; i<=12; i++)  strip2.setPixelColor(i, strip2.Color(255, 0, 0));
          for(int i=13; i<=14; i++)  strip2.setPixelColor(i, strip2.Color(0, 0, 255));
          for(int i=15; i<=17; i++)  strip2.setPixelColor(i, strip2.Color(227, 0, 0));
          break;

        case 2:
          for(int i=0; i<=17; i++)  strip2.setPixelColor(i, strip2.Color(255, 255, 255));
          break;
        case 3:
          for(int i=0; i<=17; i++)  strip2.setPixelColor(i, strip2.Color(255, 105, 105));
          break;
        case 4:
          for(int i=0; i<=17; i++)  strip2.setPixelColor(i, strip2.Color(255, 29, 83));
          break;
        case 5:
          for(int i=0; i<=2; i++)    strip2.setPixelColor(i, strip2.Color(255, 0, 0));
          for(int i=3; i<=4; i++)    strip2.setPixelColor(i, strip2.Color(255, 0, 97));
          for(int i=5; i<=7; i++)    strip2.setPixelColor(i, strip2.Color(255, 0, 235));
          for(int i=8; i<=9; i++)   strip2.setPixelColor(i, strip2.Color(0, 0, 255));
          for(int i=10; i<=12; i++)  strip2.setPixelColor(i, strip2.Color(0, 250, 255));
          for(int i=13; i<=14; i++)  strip2.setPixelColor(i, strip2.Color(0, 255, 0));
          for(int i=15; i<=17; i++)  strip2.setPixelColor(i, strip2.Color(227, 255, 0));
          break;

      }
      strip2.show();
    }


    
    //Если дверь закрыта
    else {
      snprintf (msg, 5, "%d", currPreset);
      client.publish("STRIP/HALL_STRIP/run/switchToPreset", msg);

      for(int i=0; i<=18; i++){
        strip2.setPixelColor(i, strip2.Color(0, 0, 0));        
      }
      strip2.show();

    }
    //Сохраняю текущее положение двери
    doorWas=doorIs;
  }
}


void my_distance_count(){
  Serial.print("Distance=");
  Serial.println(digitalRead(D7));
  delay(1000);
  digitalWrite(LED_BUILTIN, digitalRead(D7));
}


void my_timer(int interval){
                                //когда получаем сообщение на установку включения/выключения таймера, делаем следующее
    timerCurLed = 0;            //сбрасываем текущую позицию таймера на ленте
    timerPrevLed = -1;          //сбрасываем техническую переменную предыдущей позиции таймера на ленте
    timerPassTime = 0;          //сбрасываем счетчик прошедшего времени

    if (interval == 0) runTimer = false;      //если пришел ноль, выключаем таймер
    else runTimer = true;                     //если пришло сообщение больше нуля, таймер нужно включить

    timerStop = millis() + interval * 60000;  //рассчитываем время окончания работы таймера

    timerInterval = interval * 60;            //рассчитываем длительность таймера в секундах
}


void my_booting() {
  if (startTime > millis()) {
    ArduinoOTA.handle();
  }
  else {
    startBoot = false;
  }
}
//****************************************************************************************************************
void getBright(){
  long now = millis();  
  if (now - lastBrightMsg > 1000) {
    currentBrightness = analogRead(A0);
    Serial.print("MILLIS. ");
    Serial.print("A0=");
    Serial.print(analogRead(A0));
    Serial.print(", millis=");
    Serial.println(millis());
    /*Serial.print("now=");
    Serial.print(now);
    Serial.print(",lastBrightMsg=");
    Serial.println(lastBrightMsg); */   

    
    if (currentBrightness - prevBrightness > 10 || currentBrightness - prevBrightness < 10){
      motivationBrightness = map(currentBrightness, 10, 800, 5, 100);
      if (motivationBrightness <1) motivationBrightness = 1;
      else if (motivationBrightness >100) motivationBrightness = 100;
      showMotivation();
    }

    prevBrightness = currentBrightness;
    lastBrightMsg = now;
  }
}


void wifiPrint(){
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}



void otaInit(){
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
   ArduinoOTA.setPassword((const char *)BOARD_PASS);

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();  
}


//*****************************************************************************************************
void new_motivation(int col, int num) {
  for(int i=0; i<=217; i++){
    if(mLeds[i]==col) mLeds[i]=1;
  }

  int y=0;
  for(int i=0; i<=217; i++){
    if(y<num && (mLeds[i]==1 || mLeds[i]==0)){
      mLeds[i]=col;
      y++;
    }
  }

  //Сортируем: синие - зелёные - красные - пустые
  sortArrayReverse(mLeds, 216);

  //Непосредственно зажигаем светодиоды
  showMotivation();

  //Превращаем единицы в нули, чтобы освободить нежужные ячейки массива
  for(int i=0; i<217; i++){
    if(mLeds[i]==1) mLeds[i]=0;
  }

}
//*****************************************************************************************************
void showMotivation(){
  for(int i=0; i<217; i++){
    if (mLeds[i]==4)                SetPixel(216-i, 0,  0,  110,  0);
    else if(mLeds[i]==3){
      if(i%2!=0 || mLeds[i+1]==0)   SetPixel(216-i, 0,  60, 0,    0);
      else                          SetPixel(216-i, 20, 60, 0,    20);
    }
    else if(mLeds[i]==2)            SetPixel(216-i, 110,0,  0,    0);
    else if(mLeds[i]==1)            SetPixel(216-i, 0,  0,  0,    0);
  }
  strip.show();
}
//*****************************************************************************************************
void SetPixel (int pixel, int r, int g, int b, int w){
  strip.setPixelColor(pixel, strip.Color(r*motivationBrightness/100,
                                         g*motivationBrightness/100,
                                         b*motivationBrightness/100,
                                         w*motivationBrightness/100));
}
//*****************************************************************************************************
void PowerStrip(int r, int g, int b, int w) {
  for(int i=0;i<217;i++){
    strip.setPixelColor(i, strip.Color(r,g,b,w));    
  }
  strip.show(); // This sends the updated pixel color to the hardware.
}
//*****************************************************************************************************
/*void PowerStrip2(int r, int g, int b, int w) {
  for(int i=0;i<70;i++){
    strip2.setPixelColor(i, strip2.Color(r,g,b,w));    
  }
  strip2.show(); // This sends the updated pixel color to the hardware.
}*/
