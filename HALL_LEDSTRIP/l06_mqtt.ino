void mqtt_loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

 /* long now = millis();
  if (now - lastMsg > 3000) {
    lastMsg = now;
    snprintf (msg, 75, "%ld, %d", currentBrightness, motivationBrightness);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("getBright/kitchen", msg);
  }*/
}

//*********************************************************************************************************************************
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic_2", "reconnect");
      // ... and resubscribe

      client.subscribe("strip/hall/builtin_led");
      client.subscribe("strip/hall/test");

      client.subscribe("strip/hall/conf/#");
      client.subscribe("strip/presets/hall");

      client.subscribe("hall_strip_2/RGB");

      client.subscribe("STRIP/+/run/#");              //Новый ран
      client.subscribe("STRIP/selectStripToConfig");    //Новый конф

      subscribeToDevicesStatus();
      client.subscribe("profiles/run/#");
      client.subscribe("profiles/change/log_enable");

      client.subscribe("strip/hall/rgb32");

            
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  String top = String(topic);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  String call = "";
  char charCall[length];
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call += (char)payload[i];
    charCall[i] = (char)payload[i];
  }
  charCall[length]=0;
  Serial.println();



//****************************************************************************************************************************************
//****************************************************************************************************************************************
//****************************************************************************************************************************************
  char topicSwitchPreset[50]; snprintf (topicSwitchPreset, 50, "STRIP/%s/run/switchToPreset", BOARD_NAME);
  if (strcmp(topic, topicSwitchPreset) == 0) {
    switch_to_preset(topic, call.toInt());
  }

  //включаем профили ленты из "devices/"
  else if (strcmp(topic,"devices/hallStrip1/status/binar")==0){
          //Отправляем сообщения для включения/отключения первого пресета
          switchedStrip = !switchedStrip;
          client.publish("devices/hallStrip1/status", &String(switchedStrip)[0u], true);
  }
  else if (strcmp(topic,"devices/hallStrip1/status")==0){
          client.publish("STRIP/HALL_STRIP/run/switchToPreset", charCall, true);
  }

 //----------ПРОФИЛИ
  else if (String(topic).indexOf("profiles/run/") != -1) {
      RunProfiles(topic, String(call));
  }
  else if (strcmp(topic,"profiles/change/log_enable")==0){
      logEnable = call.toInt();
  }//----------------



  else if (top.indexOf("STRIP/db/") != -1) {
    load_preset(topic, charCall);
  }

//****************************************************************************************************************************************
  //ПОДПИСЫВАЮСЬ НА ОЧЕРЕДИ НАСТРОЙКИ ЛЕНТЫ                 только если пришло сообщение с именем моей платы
  else if (strcmp(topic, "STRIP/selectStripToConfig") == 0) {
    if(call==BOARD_NAME){
      client.subscribe("STRIP/conf/uploadPreset");          //Сохранить настройку текущего профиля
      client.subscribe("STRIP/conf/mode");                  //Выбрать режим настройки
      client.subscribe("STRIP/conf/setColor/#");            //Цветовые режимы
      client.subscribe("STRIP/conf/setBrightness/#");       //Настройка яркости
    }
    else {                                                    //если имя платы не моё, отписываюсь (настраиваю другую плату)
      client.unsubscribe("STRIP/conf/uploadPreset");          //Сохранить настройку текущего профиля
      client.unsubscribe("STRIP/conf/mode");                  //Выбрать режим настройки
      client.unsubscribe("STRIP/conf/setColor/#");            //Цветовые режимы
      client.unsubscribe("STRIP/conf/setBrightness/#");       //Настройка яркости
      client.unsubscribe("STRIP/conf/parts/#");               //Селектор частей (подписка прописана в логике stripConf)
      client.unsubscribe("STRIP/conf/interval/#");            //Группа очередей для настройки интерваловь(подписка прописана в логике stripConf)
    }
  }

//****************************************************************************************************************************************
  else if (strcmp(topic, "STRIP/conf/uploadPreset") == 0) {
    upload_preset(charCall);
  }
  else if (top.indexOf("STRIP/conf/setColor/") != -1) {
    set_color(topic, call.toInt(), true);
  }
  else if (top.indexOf("STRIP/conf/setBrightness/") != -1) {
    set_brightness(topic, charCall);
  }
  else if (strcmp(topic, "STRIP/conf/mode") == 0) {
    set_conf_mode(topic, charCall);
  }
  else if (String(top).indexOf("/interval/length") != -1) {
    calculate_interval_new(topic, charCall);
  }
  else if (String(top).indexOf("/interval/start") != -1) {
    calculate_interval_new(topic, charCall);
  }
  else if (strcmp(topic, "STRIP/conf/interval/save") == 0) {
    saveCurrInterval();
  }
  //обработчик выбора частей для настройки
  else if (top.indexOf("STRIP/conf/parts/") != -1 ) {
    do_parts(topic, charCall);
  }
  
//****************************************************************************************************************************************


//*****************  
  //Тестовый цвет второй ленты
  else if (strcmp(topic, "hall_strip_2/RGB") == 0) {  
    //Раскладываю число на три цвета
    uint32_t strip2RGB = call.toInt();
    int TestR = (strip2RGB / 256) / 256;
    int TestG = (strip2RGB / 256) % 256;
    int TestB = strip2RGB % 256;

    //Зажигаю ленту
    for(int i=0; i<=18; i++)  strip2.setPixelColor(i, strip2.Color(TestR, TestB, TestG));
    strip2.show();

    //В специальную очередь отправляю строку с каждым цветом
    snprintf (msg, 75, "R=%d, G=%d, B=%d", TestR, TestG, TestB);
    client.publish("hall_strip_2/R_G_B", msg);
  } 

  //на входе rgb32 на всю ленту. Включаем 11-ый псевдопрофиль
  else if (strcmp(topic, "strip/hall/rgb32") == 0) {  

    //В массивы-хранилище отправляю цвет
    uint32_t ui32 = strtoul(call.c_str(), NULL, 10);
    do_rgb(0, PIX_NUM-1, ui32);
    //Зажигаю ленту
    doStrip();
    //Включаю псевдо-профиль
    client.publish("devices/hallStrip1/status", "11", true);
  }

}
