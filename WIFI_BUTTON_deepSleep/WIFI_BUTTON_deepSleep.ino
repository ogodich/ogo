#define BOARD_NAME          "WIFI_BUTTON_1"
#define WIFI_BUTTON_NUMBER  "1"

/* Назначение выводов платы ESP-01 такое:
При прошивке в последний раз не менял плату. Оставил NodeMCU 1.0 (ESP-12 Module)

VCC, GND — питание платы (+3.3В);
URXD,UTXD — выводы RS232 толерантны к 3.3В
RST — Аппаратный сброс (reset)
GPIO0, GPIO2 — выводы GPIO
CH_PD — Chip enable, для работы должен быть подключен к +3.3В.

Для переключения в режим обновления прошивки нужно подать низкий уровень на GPIO0 и высокий на CH_PD.

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "ScorpGOS";
const char* password = "9031124945";
const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);

char msg[100];
char workTopicName[100];

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  snprintf (workTopicName, 100, "wifi-button/%s", WIFI_BUTTON_NUMBER);  
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.print("Connecting to ");
  Serial.println(ssid);
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");

      // Once connected, publish an announcement...
      snprintf (msg, 100, "wifi-button #%s has been successfuly started, ip-address=#%s", WIFI_BUTTON_NUMBER, WiFi.localIP().toString().c_str());
      client.publish("wifi-button", msg);

      // ... and resubscribe
      //client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

boolean isSleeping = false;
boolean sended = false;
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  if(!sended){
    client.publish(workTopicName, "11");
    Serial.print("Published at ");
    Serial.println(workTopicName);
    sended = true;
    delay(3000);
  }

  if(!isSleeping && sended){
    isSleeping = true;
    Serial.println("Go sleep!");
    ESP.deepSleep(0);
  }

}
