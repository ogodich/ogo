void mqtt_loop(){
  if (!client.connected()) reconnect();
  client.loop();
}

void mySubscribtion(){
  //client.subscribe("light/kitchen/coohoo");
  client.subscribe("bounce/coohoo");

  subscribeToDevicesStatus();
  client.subscribe("profiles/run/#");
  client.subscribe("profiles/change/log_enable");
}

void reconnect() {
  if (!offline || (offline && millis() - mqttLastReconnect > 5000)){
    Serial.print("Attempting MQTT connection...");
    
    // Если удалось подключиться
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      //Если пришли из офлайна, отправляем изменения
      if(offline) sendFromOffline();
      
      offline = false;
      // ... and resubscribe
      mySubscribtion();

    
    // Если не удалось подключиться
    } else {
      Serial.print("failed, rc=");
      Serial.println(client.state());
      offline = true;
      mqttLastReconnect = millis();
    }
  }
}

void sendFromOffline(){
  snprintf (msg, 75, "%d", lightStat[0]);
  client.publish("devices/coohoo/status", msg, true);  
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();


//*********************************************************************************************************************************
  if (strcmp(topic,"devices/coohoo/status/binar")==0){
          lightStat[0] = !lightStat[0];
          snprintf (msg, 75, "%d", lightStat[0]);
          client.publish("devices/coohoo/status", msg, true);
  }
  else if (strcmp(topic,"devices/coohoo/status")==0){
    if(call=="-1") {
        lightStat[0] = !lightStat[0];
        snprintf (msg, 75, "%d", lightStat[0]);
        client.publish("devices/coohoo/status", msg, true);
    }
    else {
      lightStat[0] = (call.toInt());
      digitalWrite(lights[0], !lightStat[0]);
    }
  }
  else if (strcmp(topic,"bounce/coohoo")==0){
    if(call=="1") {
      snprintf (msg, 75, "1=%d, 2=%d, 3=%d", bounceCount[0], bounceCount[1], bounceCount[2]);
      client.publish("bounce/coohoo", msg);
    }
    else {
      bounceCount[0] = 0;
      bounceCount[1] = 0;
      bounceCount[2] = 0;
    }
  }

  //----------ПРОФИЛИ
  else if (String(topic).indexOf("profiles/run/") != -1) {
      RunProfiles(topic, String(call));
  }
  else if (strcmp(topic,"profiles/change/log_enable")==0){
      logEnable = call.toInt();
  }//----------------
  

  //*********
}
