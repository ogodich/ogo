/* Node MCU D1 mini
Pin  Function  ESP-8266 Pin
TX  TXD TXD
RX  RXD RXD
A0  Analog input, max 3.3V input  A0
D0  IO  GPIO16
D1  IO, SCL GPIO5
D2  IO, SDA GPIO4
D3  IO, 10k Pull-up GPIO0
D4  IO, 10k Pull-up, BUILTIN_LED  GPIO2
D5  IO, SCK GPIO14
D6  IO, MISO  GPIO12
D7  IO, MOSI  GPIO13
D8  IO, 10k Pull-down, SS GPIO15 (Нельзя делать выходом, не стартует)
G Ground  GND
5V  5V  -
3V3 3.3V  3.3V
RST Reset RST


 client.publish(topic, (uint8_t*)msg, 50, true);
*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>

#include <PubSubClient.h>

#include <ArduinoJson.h>

#define BOARD_NAME "COOHOO"
#define BOARD_PASS "coohoo"

/*_______________________________________________________________________________________________________________________________*/
const String DEVICES[] = {"coohoo"};

const char* ssid = "ScorpGOS";
//const char* ssid = "ScorpGOS";
const char* password = "9031124945";

int lights[1] = {14};
int buttons[4] = {4, 12, 13, 5};

boolean lightStat[1] = {true};

boolean buttonIs = false;
boolean buttonWas = false;

boolean statusChanged = false;

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
char msg[50];

long bounceCount[3];

boolean offline = false;
long mqttLastReconnect = 0;

//home profiles-------------------------------------------
String currentProfileNo = "";
String jSonProfiles = "";
const int cDevices = sizeof(DEVICES) / sizeof(DEVICES[0]);
const char addLogTopic[]="profiles/log/add";
bool logEnable=false;
//--------------------------------------------------------
