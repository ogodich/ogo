//---------------------------------------------------------------
void my_timer(int interval){
                                  //когда получаем сообщение на установку включения/выключения таймера, делаем следующее
    timerCurPower = 0;            //сбрасываем текущую яркость таймера на ленте
    timerPrevPower = -1;          //сбрасываем техническую переменную предыдущей яркости таймера на ленте
    timerPassTime = 0;            //сбрасываем счетчик прошедшего времени

    if (interval == 0) runTimer = false;      //если пришел ноль, выключаем таймер
    else runTimer = true;                     //если пришло сообщение больше нуля, таймер нужно включить

    timerStop = millis() + interval * 60000;  //рассчитываем время окончания работы таймера
    timerInterval = interval * 60;            //рассчитываем длительность таймера в секундах

    dblTimerStartPower = currStripPower > 0.0 ? currStripPower : 100.0;         //начинаем таймер не с 100%, как было, а с текущей яркости

    snprintf (msg, 20, "%d",  interval);
    client.publish("strip/picture/timer/remain", msg, true);
}

void run_my_timer() {
  long now = millis();
  if (now - timerRefresh > 1000) {
    timerRefresh = now;

    timerPassTime ++;                                                   //инкремент пройденного времени

    //Оповещаю пользователя о том, сколько осталось минут до окончания таймера
    TimerRemainTime = timerInterval - timerPassTime;
    snprintf (msg, 20, "%d",  TimerRemainTime / 60);
    if(TimerRemainTime % 60 == 0) client.publish("strip/picture/timer/remain", msg, true);
    
    double dbltimerCurPower = dblTimerStartPower / timerInterval * timerPassTime;   //рассчитываем текущую яркость, с дробной частью
    timerCurPower = dblTimerStartPower - round(dbltimerCurPower);                      //округляем, инвертируем и переносим в целочисленную переменную
    timerCurPower = timerCurPower < 0 ? 0 : timerCurPower;

    if(timerPrevPower!=timerCurPower){
      snprintf (msg, 20, "%d",  timerCurPower);
      client.publish("devices/bedroomPicture/status", msg, true);
    }

    //если время закончилось
    if (now > timerStop) {
      runTimer = false;
      timerCurPower = 0;             //сбрасываем текущую позицию таймера на ленте
      timerPrevPower = -1;           //сбрасываем техническую переменную предыдущей яркости таймера
      timerPassTime = 0;             //сбрасываем счетчик прошедшего времени
    }

  timerPrevPower = timerCurPower;
  /*---------------------------------------*/  
  }
}
