void read_sensor(){
  //Если сработало, объявляем переменные и входим в цикл, пока нажат сенсор
  if(digitalRead(SENSOR_1)) {
    long sensorStart = 0;
    long sensorWasHold = 0;
    sensorStart = millis();
    
    while(digitalRead(SENSOR_1)) {    
      delay(50);
    }

    //Вычисляем время, сколько сенсор был "нажат"
    sensorWasHold = millis()-sensorStart;
    Serial.print("sensorTime=");
    Serial.println(sensorWasHold);

    //Для разных диапазонов выполняем разные команды
    if      (sensorWasHold > 50    &&  sensorWasHold < 500) client.publish("devices/bedroomPicture/status/binar", BOARD_NAME);
    else if (sensorWasHold > 500   && sensorWasHold < 1000) client.publish("devices/bedroomPicture/status", "50", true);
    else if (sensorWasHold > 1000  && sensorWasHold < 5000) client.publish("devices/bedroomPicture/status", "5", true);
  }
}

///////////////////////////////////////////////



void wifiPrint(){
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void otaInit(){
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
   ArduinoOTA.setPassword((const char *)OTA_PSWRD);

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();  
}
