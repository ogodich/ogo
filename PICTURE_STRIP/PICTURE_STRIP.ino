/* Node MCU D1 mini
  Pin  Function  ESP-8266 Pin
  TX  TXD TXD
  RX  RXD RXD
  A0  Analog input, max 3.3V input  A0
  D0  IO  GPIO16
  D1  IO, SCL GPIO5
  D2  IO, SDA GPIO4
  D3  IO, 10k Pull-up GPIO0                   - прижимаем к земле для загрузки
  D4  IO, 10k Pull-up, BUILTIN_LED  GPIO2
  D5  IO, SCK GPIO14
  D6  IO, MISO  GPIO12
  D7  IO, MOSI  GPIO13
  D8  IO, 10k Pull-down, SS GPIO15 (Нельзя делать выходом, не стартует)
  G Ground  GND
  5V  5V  -
  3V3 3.3V  3.3V
  RST Reset RST


  some_string.toInt()



  ---------
  map();
  value: значение для переноса
  fromLow: нижняя граница текущего диапазона
  fromHigh: верхняя граница текущего диапазона
  toLow: нижняя граница нового диапазона, в который переносится значение
  toHigh: верхняя граница нового диапазона
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
//#include <ESP8266WebServer.h>

#include <WiFiUdp.h>
#include <ArduinoOTA.h>

//#include <ArduinoSort.h>

#include <PubSubClient.h>

#include <stdlib.h>

#include <ArduinoJson.h>

//*************************************************************************************************************

#define STRIP_1         D6
#define STRIP_2         D7
#define SENSOR_1        D5
#define BOARD_NAME      "PICTURE_STRIP"
#define OTA_PSWRD       "picture"

//*************************************************************************************************************
const String DEVICES[] = {"bedroomPicture"};

const char* ssid = "ScorpGOS";
const char* password = "9031124945";    
const char* mqtt_server = "192.168.1.1";

//const char* ssid = "berloga";
//const char* password = "plutonnastya";    
//const char* mqtt_server = "192.168.8.102";

WiFiClient espClient;
PubSubClient client(espClient);

char msg[50];

int currStripPower = 0;

boolean runTimer = false;
long timerRefresh = 0;
long timerStop = 0;
int timerCurPower = 0;
int timerPrevPower = -1;
int timerPassTime = 0;
int timerInterval = 0;

double dblTimerStartPower = 100;

int TimerRemainTime = 0;

//home profiles-------------------------------------------
String currentProfileNo = "";
String jSonProfiles = "";
const int cDevices = sizeof(DEVICES) / sizeof(DEVICES[0]);
const char addLogTopic[]="profiles/log/add";
bool logEnable=false;
//--------------------------------------------------------
