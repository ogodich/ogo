void mqtt_loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

//*********************************************************************************************************************************
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      client.subscribe("strip/picture/#");

      subscribeToDevicesStatus();
      client.subscribe("profiles/run/#");
      client.subscribe("profiles/change/log_enable");

    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  String top = String(topic);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  String call = "";
  char charCall[length];
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call += (char)payload[i];
    charCall[i] = (char)payload[i];
  }
  charCall[length]=0;
  Serial.println();


//*****************
  //----------------------  device  ---------------------------------
  //обработчик булевого включения/выключения
  if (strcmp(topic,"devices/bedroomPicture/status/binar")==0){
        currStripPower = currStripPower > 0 ? 0 : 100;
        snprintf (msg, 75, "%d", currStripPower);
        client.publish("devices/bedroomPicture/status", msg, true);
  }
  //обработчик процентного включения
  else if (strcmp(topic,"devices/bedroomPicture/status")==0){
        int param = call.toInt();
    
        //рассчет процента для каждой из двух лент. и их включение
        if(param >= 0 && param <= 100){
          int power_1 = param >= 50 ? 50 : param;
          int power_2 = param - 50;
          analogWrite(STRIP_1, map(power_1, 0, 50, 0, 1024));
          analogWrite(STRIP_2, map(power_2, 0, 50, 0, 1024));
        }
    
        //При кардинальном изменении яркости сохраняю в статусной очереди статус ленты 1 или 0
        if     (currStripPower == 0 && param > 0)  client.publish("light/picture/led_strip_status", "100", true);
        else if(currStripPower > 0 && param == 0)  client.publish("light/picture/led_strip_status", "0", true);
    
        //при выключении ленты пользователем, отключаем таймер (костыль. возникал кейс, когда таймер останавливался, но приходило ещё одно сообщение на включение ленты)
        if(param==0 && runTimer) {
          client.publish("strip/picture/timer", "0");
          client.publish("devices/bedroomPicture/status", "0", true);
        }
    
        currStripPower = param;
  }

//***************** ТАЙМЕР
  else if (strcmp(topic, "strip/picture/timer") == 0) {    
    my_timer(call.toInt());
  }
//*****************



 //----------ПРОФИЛИ
  else if (String(topic).indexOf("profiles/run/") != -1) {
      RunProfiles(topic, String(call));
  }
  else if (strcmp(topic,"profiles/change/log_enable")==0){
      logEnable = call.toInt();
  }//----------------
 

 
}


/* 50 = 255
 * 25 = 127 
 *  
 *  
 *  50 = 50 + 0
 *  55 = 50 + 5
 *  30 = 30 + 0
 *  
 *  100 - x
 *  
 */
