void setup(void){
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  wifiPrint();  

  otaInit();

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  pinMode(STRIP_1, OUTPUT);
  pinMode(STRIP_2, OUTPUT);
  pinMode(SENSOR_1, INPUT);

  digitalWrite(STRIP_1, LOW);
  digitalWrite(STRIP_2, LOW);
}
