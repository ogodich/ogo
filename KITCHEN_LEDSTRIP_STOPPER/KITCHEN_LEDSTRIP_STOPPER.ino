

/* Node MCU D1 mini
  Pin  Function  ESP-8266 Pin
  TX  TXD TXD
  RX  RXD RXD
  A0  Analog input, max 3.3V input  A0
  D0  IO  GPIO16
  D1  IO, SCL GPIO5
  D2  IO, SDA GPIO4
  D3  IO, 10k Pull-up GPIO0
  D4  IO, 10k Pull-up, BUILTIN_LED  GPIO2
  D5  IO, SCK GPIO14
  D6  IO, MISO  GPIO12
  D7  IO, MOSI  GPIO13
  D8  IO, 10k Pull-down, SS GPIO15 (Нельзя делать выходом, не стартует)
  G Ground  GND
  5V  5V  -
  3V3 3.3V  3.3V
  RST Reset RST


  some_string.toInt()
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
//#include <ESP8266WebServer.h>

#include <WiFiUdp.h>
#include <ArduinoOTA.h>

//#include <Adafruit_NeoPixel.h>
/*#ifdef __AVR__
  #include <avr/power.h>
  #endif
*/

//#include <ArduinoSort.h>

#include <PubSubClient.h>

//#include <stdlib.h>

//#include <ArduinoJson.h>

//#include <IRremoteESP8266.h>
//#include <IRrecv.h>
//#include <IRutils.h>

//#include <sstream>

//*************************************************************************************************************

#define PIN            D7
#define BOARD_NAME "LED_STRIP_STOPPER"

//*************************************************************************************************************
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

//ESP8266WebServer server(80);

//int leds[] = {2, 4, 5};

//Adafruit_NeoPixel strip = Adafruit_NeoPixel(217, PIN, NEO_GRBW + NEO_KHZ800);
//Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(70, PIN2, NEO_GRBW + NEO_KHZ800);
//boolean switchedStrip = false;
//boolean switchedStrip2 = false;

//int mLeds[217];
//int countLeds[10];

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
//long lastMsg = 0;

//boolean startBoot = false;
//double startTime = 0;

char msg[50];
//int value = 0;

//long lastBrightMsg = 0;
//int currentBrightness = 0;
//int prevBrightness = 0;

//int motivationBrightness = 0;
//int secondStripBrigth = 110;

//String debug;
//char deb[150];

/*boolean stripConfParts[7];
int     stripConfR [6][6];
int     stripConfG [6][6];
int     stripConfB [6][6];
int     stripConfW [6][6];
int     stripConfBr[6][6];
int currPreset = 1;


boolean runTimer = false;
long timerRefresh = 0;
long timerStop = 0;
int timerCurLed = 0;
int timerPrevLed = -1;
int timerPassTime = 0;
int timerInterval = 0;

IRrecv irrecv(RECV_PIN);
decode_results results;


String irrPrevVal = "";
String irrCurrVal = "";
int irrCount = 0;
long irrLastTime = 0;
boolean irrRecieved = false;
uint64_t iCurrVal;
uint64_t iPrevVal;

int irrMode = 0;
long irrModeTimer = 0;

long irrModeAnimationTimer = 0;
boolean irrAnimationTrig = false;
*/
