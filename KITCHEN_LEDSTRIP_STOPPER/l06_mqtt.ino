void mqtt_loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

//*********************************************************************************************************************************
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic_2", "reconnect");
      // ... and resubscribe

      client.subscribe("strip/presets/kitchen");
      client.subscribe("strip/kitchen/setPreset");
      client.subscribe("light/kitchen/led_strip");
      client.subscribe("light/kitchen/led_strip2_bright");
      client.subscribe("light/kitchen/led_strip2");

    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  String top = String(topic);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  String call = "";
  char charCall[length];
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call += (char)payload[i];
    charCall[i] = (char)payload[i];
  }
  charCall[length]=0;
  Serial.println();


//*****************
  if (strcmp(topic, "boot/ledstrip") == 0) {
  }

  else {
    Serial.print(millis());
    Serial.print(" - ");
    Serial.print(topic);
    Serial.print(", call=");
    Serial.println(call);

    digitalWrite(PIN, HIGH);
    delay(100);
    digitalWrite(PIN, LOW);
  }


 
}   //*********************************************************************************************************************************
