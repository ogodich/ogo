void mqtt_loop(){
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();

//*********************************************************************************************************************************
  if (strcmp(topic,"battary/LRX22C")==0){
    if      (call.toInt()<=40) digitalWrite(lights[0], true);
    else if (call.toInt()>=80) digitalWrite(lights[0], false);
    lastMsg = millis();
  }
  //*********
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "reconnecting");
      // ... and resubscribe
      client.subscribe("battary/LRX22C");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
