/* Назначение выводов платы ESP-01 такое:
VCC, GND — питание платы (+3.3В);
URXD,UTXD — выводы RS232 толерантны к 3.3В
RST — Аппаратный сброс (reset)
GPIO0, GPIO2 — выводы GPIO
CH_PD — Chip enable, для работы должен быть подключен к +3.3В.

Для переключения в режим обновления прошивки нужно подать низкий уровень на GPIO0 и высокий на CH_PD.


 client.publish(topic, (uint8_t*)msg, 50, true);
*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>

#define BOARD_NAME "PHONE_CHARGE_test"
#define BOARD_PASS "charge"

/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

int lights[1] = {1};

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
