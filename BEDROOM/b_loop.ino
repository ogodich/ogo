void loop() {
  my_multy_switcher();
  mqtt_loop();

  if(!offline) {
    ArduinoOTA.handle();
  }
}
