/*
static const uint8_t D0   = 16; * - кнопка
static const uint8_t D1   = 5;  * - кнопка
static const uint8_t D2   = 4;  * - кнопка
static const uint8_t D3   = 0;  - - РЕЛЕ?             - не делать входом*
static const uint8_t D4   = 2;  * - не делать входом
3V3
GND
static const uint8_t D5   = 14; * - кнопка
static const uint8_t D6   = 12; * - реле
static const uint8_t D7   = 13;   - светодиоды
static const uint8_t D8   = 15;   - кнопка
static const uint8_t D9   = 3;  * - реле, моргает при прошивке
static const uint8_t D10  = 1;  * - долго висело реле, но случилась проблема
GND
3V3

A0
GND ???
VU  ???
SD3 (GPIO 10)
SD2 (GPIO 9)
SD1
CMD
SD0
CLK
GND
3V3
EN
RST
GND
VIN
SDD3 = 10  * - реле
SDD2 = 9
*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif


#define BOARD_NAME "BEDROOM"
#define BOARD_PASS "bedroom"

#define SMART_LED_1            D7


/*_______________________________________________________________________________________________________________________________*/
//const char* ssid = "ScorpGOS";
const char* ssid = "ScorpGOS_test";
const char* password = "9031124945";

boolean startBoot = false;
double startTime = 0;

int lights[2] = {D6};

int buttons[3] = {D0, D1, D2};
boolean lightStat[3] = {false, false, false};

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
char msg[50];

Adafruit_NeoPixel smartLed1 = Adafruit_NeoPixel(3, SMART_LED_1, NEO_GRB + NEO_KHZ800);

boolean testMode = false;

boolean offline = false;
long mqttLastReconnect = 0;
