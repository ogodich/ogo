void setup() {
  Serial.begin(115200);
  Serial.println("Booting");

  pinMode(D9, OUTPUT);
  digitalWrite(D9, HIGH);
  
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  /*while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("WiFi.waitForConnectResult()");
    Serial.println("Connection Failed! Rebooting...");
    digitalWrite(D9, LOW);
    delay(300);
    digitalWrite(D9, HIGH);
    delay(4000);
    //ESP.restart();
  }*/

for(int i=0;i<6;i++){
  digitalWrite(D9, LOW);
  delay(100);
  digitalWrite(D9, HIGH);
  delay(200);
}
  

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  otaInit();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  pinMode(buttons[0], INPUT_PULLUP);
  pinMode(buttons[1], INPUT_PULLUP);
  pinMode(buttons[2], INPUT_PULLUP);

  pinMode(lights[0], OUTPUT);
  digitalWrite(lights[0], HIGH);

  
  //умные светодиоды
  smartLed1.begin(); // This initializes the NeoPixel library.
  smartLed1.setBrightness(255); // set brightness
  smartLed1.show(); // Initialize all pixels to 'off'


}
