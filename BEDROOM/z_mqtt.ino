void mqtt_loop(){
  if (!client.connected()) reconnect();
  client.loop();
}

void mySubscribtion(){
  client.subscribe("bedroom/#");
}

void reconnect() {
  if (!offline || (offline && millis() - mqttLastReconnect > 5000)){
    Serial.print("Attempting MQTT connection...");
    
    // Если удалось подключиться
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      //Если пришли из офлайна, отправляем изменения
      if(offline) sendFromOffline();
      
      offline = false;
      // ... and resubscribe
      mySubscribtion();

    
    // Если не удалось подключиться
    } else {
      Serial.print("failed, rc=");
      Serial.println(client.state());
      offline = true;
      mqttLastReconnect = millis();

      refreshIndicators(); //только для устройств со светодиодами!!!
    }
       
  }
}

void sendFromOffline(){
  snprintf (msg, 75, "%d", lightStat[0]);
  client.publish("bedroom/test/0", msg, true);  
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();
//*********************************************************************************************************************************
  if (strcmp(topic,"corridor/test_mode")==0){
    testMode = call.toInt();
  }

//==================================================================================================================
//TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  
//==================================================================================================================

else if (strcmp(topic,"bedroom/test/0")==0){
    if(call=="-1") {
      lightStat[0] = !lightStat[0];
      snprintf (msg, 75, "%d", lightStat[0]);
      client.publish("bedroom/test/0", msg, true);
    }
    else lightStat[0] = call.toInt();

    switchLights();
    refreshIndicators();
}
else if (strcmp(topic,"bedroom/test/1")==0){
    if(call=="-1") {
      lightStat[1] = !lightStat[1];
      snprintf (msg, 75, "%d", lightStat[1]);
      client.publish("bedroom/test/1", msg, true);
    }
    else lightStat[1] = call.toInt();
    
    refreshIndicators();
}
else if (strcmp(topic,"bedroom/test/2")==0){
    if(call=="-1") {
      lightStat[2] = !lightStat[2];
      snprintf (msg, 75, "%d", lightStat[2]);
      client.publish("bedroom/test/2", msg, true);
    }
    else lightStat[2] = call.toInt();
    
    refreshIndicators();
}
  

//==================================================================================================================
//REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  
//==================================================================================================================
  
  else if (strcmp(topic,"light/toilet/1")==0){
    if(call=="-1" || call=="2") {
      lightStat[0] = !lightStat[0];
      snprintf (msg, 75, "%d", lightStat[0]);
      client.publish("light/toilet/1", msg, true);
    }
    else {
      lightStat[0] = call.toInt();    
      if (lightStat[0]) animation_1();
      else              animation_2();
    }
    refreshIndicators();
    digitalWrite(lights[0], !lightStat[0]);
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/kitchen/main")==0){
    if(call=="-1" || call=="2") {
      lightStat[1] = !lightStat[1];
      snprintf (msg, 75, "%d", lightStat[1]);
      client.publish("light/kitchen/main", msg, true);
    }
    else lightStat[1] = call.toInt();
    refreshIndicators();
    switchLights();    
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/kitchen/corner")==0){
    if(call=="1" || call=="0") {
      lightStat[2] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/kitchen/coohoo")==0){
    if(call=="1" || call=="0") {
      lightStat[3] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------

  if (strcmp(topic,"strip/kitchen/setPreset")==0){
    if(call.toInt()==0) lightStat[4] = false;
    if(call.toInt()>0)  lightStat[4] = true;
    refreshIndicators();
  }//-----------------------------------------------

  if (strcmp(topic,"light/kitchen/led_strip2")==0){
    if(call=="1" || call=="0") {
      lightStat[5] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------

  if (strcmp(topic,"light/bath/1")==0){
    if(call=="-1") {
      lightStat[6] = !lightStat[6];
      snprintf (msg, 75, "%d", lightStat[6]);
      client.publish("light/bath/1", msg, true);
    }
    else {
      lightStat[6] = call.toInt();
      if (call.toInt() == 1){
        lightStat[7] = false;
        client.publish("light/bath/2", "0", true);        
      }
    }
    refreshIndicators();
    switchLights();    
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/bath/2")==0){
    if(call=="-1") {
      lightStat[7] = !lightStat[7];
      snprintf (msg, 75, "%d", lightStat[7]);
      client.publish("light/bath/2", msg, true);
    }
    else {
      lightStat[7] = call.toInt();
      if (call.toInt() == 1){
        lightStat[6] = false;
        client.publish("light/bath/1", "0", true);
      }
    }
    refreshIndicators();
    switchLights();    
  }//-----------------------------------------------

  if (strcmp(topic,"homeLightPresets")==0){
  
    //Уютный ужин
    if(call=="1") {
      //Большой свет на кухне - ВЫКЛ
      client.publish("light/kitchen/main", "0", true);
      //Вытяжка на кухне - ВЫКЛ
      client.publish("light/kitchen/coohoo", "0", true);
      //Угловой свет на кухне - ВКЛ
      client.publish("light/kitchen/corner", "1", true);
      //Светодиодная лента - включить первый пресет
      client.publish("strip/kitchen/setPreset", "1", true);
      //Верхняя лента - ВКЛ
      client.publish("light/kitchen/led_strip2", "1", true);
    }

    //Спокойной ночи
    if(call=="2") {
      //Большой свет на кухне
      client.publish("light/kitchen/main", "0", true);
      //Вытяжка на кухне
      client.publish("light/kitchen/coohoo", "0", true);
      //Угловой свет на кухне
      client.publish("light/kitchen/corner", "0", true);
      //Светодиодная лента на кухне
      client.publish("strip/kitchen/setPreset", "0", true);
      client.publish("light/kitchen/led_strip2", "0", true);

      //Ванная
      client.publish("light/bath/1", "0", true);
      client.publish("light/bath/2", "0", true);
      //Туалет
      client.publish("light/toilet/1", "0", true);
      //Лента в прихожей
      client.publish("strip/hall/setPreset", "0", true);
      //Гирлянды
      client.publish("remote_socket/1", "0", true);
      client.publish("remote_socket/2", "0", true);
    }
    
  }//-----------------------------------------------













//*********************************************************************************************************************************
}
