void my_booting() {
  if (startTime > millis()) {
    ArduinoOTA.handle();
  }
  else {
    startBoot = false;
  }
}

void refreshIndicators(){
  //1
  if      (lightStat[0] && offline)   smartLed1.setPixelColor(0, smartLed1.Color(0,200,50));
  else if (lightStat[0])              smartLed1.setPixelColor(0, smartLed1.Color(0,200,0));
  else if (!lightStat[0] && offline)  smartLed1.setPixelColor(0, smartLed1.Color(250,0,50));
  else if (!lightStat[0])             smartLed1.setPixelColor(0, smartLed1.Color(250,0,0));
  
  //2
  if      (offline)        smartLed1.setPixelColor(1, smartLed1.Color(30,0,5));  
  else if (lightStat[1])   smartLed1.setPixelColor(1, smartLed1.Color(0,200,0));
  else if (!lightStat[1])  smartLed1.setPixelColor(1, smartLed1.Color(250,0,0));

  //3
  if      (offline)        smartLed1.setPixelColor(2, smartLed1.Color(30,0,5));  
  else if (lightStat[2])   smartLed1.setPixelColor(2, smartLed1.Color(0,200,0));
  else if (!lightStat[2])  smartLed1.setPixelColor(2, smartLed1.Color(250,0,0));
   
  /*
    //Угловой, вытяжка
  if      (lightStat[2] && lightStat[3])    smartLed1.setPixelColor(3, smartLed1.Color(40,30,0));
  else if (lightStat[2] && !lightStat[3])   smartLed1.setPixelColor(3, smartLed1.Color(0,30,0));
  else if (!lightStat[2] && lightStat[3])   smartLed1.setPixelColor(3, smartLed1.Color(0,0,40));
  else if (!lightStat[2] && !lightStat[3])  smartLed1.setPixelColor(3, smartLed1.Color(40,0,0));
  //Ленты
  if      (lightStat[4] && lightStat[5])    smartLed1.setPixelColor(2, smartLed1.Color(40,30,0));
  else if (lightStat[4] && !lightStat[5])   smartLed1.setPixelColor(2, smartLed1.Color(0,30,0));
  else if (!lightStat[4] && lightStat[5])   smartLed1.setPixelColor(2, smartLed1.Color(0,0,40));
  else if (!lightStat[4] && !lightStat[5])  smartLed1.setPixelColor(2, smartLed1.Color(40,0,0));
  //Ванна
  if      (lightStat[6] && !lightStat[7])   smartLed1.setPixelColor(1, smartLed1.Color(0,200,0));
  else if (!lightStat[6] && lightStat[7])   smartLed1.setPixelColor(1, smartLed1.Color(0,0,200));
  else if (!lightStat[6] && !lightStat[7])  smartLed1.setPixelColor(1, smartLed1.Color(250,0,0));
  */
  smartLed1.show(); // This sends the updated pixel color to the hardware.
}

void animation_1 (){
  for (int i=0; i<250; i++){
    smartLed1.setPixelColor(0, smartLed1.Color(250-i, i < 200 ? i : 200, 0));
    delay(3);    
    smartLed1.show();
    i=i+10;
  }
}

void animation_2 (){
  for (int i=0; i<250; i++){
    smartLed1.setPixelColor(0, smartLed1.Color(i, 200 - (i < 200 ? i : 200), 0));
    delay(3);    
    smartLed1.show();
    i=i+10;
  }
}

void switchLights(){
  digitalWrite(lights[0], !lightStat[0]);
  //digitalWrite(lights[1], !lightStat[1]);
}

void otaInit(){
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
   ArduinoOTA.setPassword((const char *)BOARD_PASS);

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();  
}
