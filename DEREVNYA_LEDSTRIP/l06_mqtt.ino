void mqtt_loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

 /* long now = millis();
  if (now - lastMsg > 3000) {
    lastMsg = now;
    snprintf (msg, 75, "%ld, %d", currentBrightness, motivationBrightness);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("getBright/kitchen", msg);
  }*/
}

//*********************************************************************************************************************************
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic_2", "reconnect");
      // ... and resubscribe

      /*client.subscribe("boot/ledstrip");
      client.subscribe("motivation/+");
      client.subscribe("strip/conf/#");


      client.subscribe("light/kitchen/led_strip");
      client.subscribe("light/kitchen/led_strip2_bright");
      client.subscribe("light/kitchen/led_strip2");*/
      

      client.subscribe("strip/hall/builtin_led");
      client.subscribe("strip/hall/test");

      client.subscribe("strip/hall/conf/#");
      client.subscribe("strip/presets/hall");
      
      //старый выбор пресетов. уходим от него
      //client.subscribe("STRIP/HALL_STRIP/run/switchToPreset");
      client.subscribe("light/hall_strip");

      client.subscribe("hall_strip_2/RGB");

      client.subscribe("STRIP/+/run/#");              //Новый ран
      client.subscribe("STRIP/selectStripToConfig");    //Новый конф

            
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  String top = String(topic);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  String call = "";
  char charCall[length];
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call += (char)payload[i];
    charCall[i] = (char)payload[i];
  }
  charCall[length]=0;
  Serial.println();



//****************************************************************************************************************************************
//****************************************************************************************************************************************
//****************************************************************************************************************************************
  
  
  char topicSwitchPreset[50]; snprintf (topicSwitchPreset, 50, "STRIP/%s/run/switchToPreset", BOARD_NAME);
  if (strcmp(topic, topicSwitchPreset) == 0) {
    switch_to_preset(topic, call.toInt());
    //При выборе пресета сохраняю в статусной очереди статус ленты 1 или 0
    //if (call.toInt() > 0) client.publish("light/kitchen/led_strip_status", "1", true);
    //else                  client.publish("light/kitchen/led_strip_status", "0", true);
  }
  
  else if (strcmp(topic, "STRIP/run/switch") == 0) {
    currPreset = currPreset > 0 ? 0 : 1;
    snprintf (msg, 75, "%d", currPreset);
    client.publish("STRIP/run/switchToPreset", msg, true);
    client.publish("light/kitchen/led_strip_status", msg, true);
  }
  else if (top.indexOf("STRIP/db/") != -1) {
    load_preset(topic, charCall);
  }

//****************************************************************************************************************************************
  //ПОДПИСЫВАЮСЬ НА ОЧЕРЕДИ НАСТРОЙКИ ЛЕНТЫ                 только если пришло сообщение с именем моей платы
  else if (strcmp(topic, "STRIP/selectStripToConfig") == 0) {
    if(call==BOARD_NAME){
      client.subscribe("STRIP/conf/uploadPreset");          //Сохранить настройку текущего профиля
      client.subscribe("STRIP/conf/mode");                  //Выбрать режим настройки
      client.subscribe("STRIP/conf/setColor/#");            //Цветовые режимы
      client.subscribe("STRIP/conf/setBrightness/#");       //Настройка яркости
    }
    else {                                                    //если имя платы не моё, отписываюсь (настраиваю другую плату)
      client.unsubscribe("STRIP/conf/uploadPreset");          //Сохранить настройку текущего профиля
      client.unsubscribe("STRIP/conf/mode");                  //Выбрать режим настройки
      client.unsubscribe("STRIP/conf/setColor/#");            //Цветовые режимы
      client.unsubscribe("STRIP/conf/setBrightness/#");       //Настройка яркости
      client.unsubscribe("STRIP/conf/parts/#");               //Селектор частей (подписка прописана в логике stripConf)
      client.unsubscribe("STRIP/conf/interval/#");            //Группа очередей для настройки интерваловь(подписка прописана в логике stripConf)
    }
  }

//****************************************************************************************************************************************
  else if (strcmp(topic, "STRIP/conf/uploadPreset") == 0) {
    upload_preset(charCall);
  }
  else if (top.indexOf("STRIP/conf/setColor/") != -1) {
    set_color(topic, call.toInt(), true);
  }
  else if (top.indexOf("STRIP/conf/setBrightness/") != -1) {
    set_brightness(topic, charCall);
  }
  else if (strcmp(topic, "STRIP/conf/mode") == 0) {
    set_conf_mode(topic, charCall);
  }
  else if (String(top).indexOf("/interval/length") != -1) {
    calculate_interval_new(topic, charCall);
  }
  else if (String(top).indexOf("/interval/start") != -1) {
    calculate_interval_new(topic, charCall);
  }
  else if (strcmp(topic, "STRIP/conf/interval/save") == 0) {
    saveCurrInterval();
  }
  //обработчик выбора частей для настройки
  else if (top.indexOf("STRIP/conf/parts/") != -1 ) {
    do_parts(topic, charCall);
  }
  
//****************************************************************************************************************************************
  //Обработчик включения первого профиля с выключателей
  else if (strcmp(topic, "light/hall_strip") == 0) {
    if (call == "2") {
      switchedStrip = !switchedStrip;
      //Отправляем сообщения для включения/отключения первого пресета
      if (switchedStrip) client.publish("STRIP/HALL_STRIP/run/switchToPreset", "1", true);
      else               client.publish("STRIP/HALL_STRIP/run/switchToPreset", "0", true);
    }
    else client.publish("STRIP/HALL_STRIP/run/switchToPreset", charCall, true);
  }

              //Тестовый кусок. потом удалить
                            else if (strcmp(topic, "strip/hall/test") == 0) {
                              if (call.toInt() == 1) {
                                for(int i=0; i < 3; i++) strip.setPixelColor(i, strip.Color(0,0,255)); // Moderately bright green color.
                              }
                              else {
                                for(int i=0; i < 3; i++) strip.setPixelColor(i, strip.Color(0,0,0)); // Moderately bright green color.
                              }    
                              strip.show();
                              digitalWrite(LED_BUILTIN, call.toInt());   // turn the LED on (HIGH is the voltage level)
                            }
                            /*моргаем встроенным светодиодом*/
                            else if (strcmp(topic, "strip/hall/builtin_led") == 0) {
                              digitalWrite(LED_BUILTIN, call.toInt());   // turn the LED on (HIGH is the voltage level)
                            }





//*****************  
  //Тестовый цвет второй ленты
  else if (strcmp(topic, "hall_strip_2/RGB") == 0) {    
    //Раскладываю число на три цвета
    uint32_t strip2RGB = call.toInt();
    int TestR = (strip2RGB / 256) / 256;
    int TestG = (strip2RGB / 256) % 256;
    int TestB = strip2RGB % 256;

    //Зажигаю ленту
    for(int i=0; i<=18; i++)  strip2.setPixelColor(i, strip2.Color(TestR, TestB, TestG));
    strip2.show();

    //В специальную очередь отправляю строку с каждым цветом
    snprintf (msg, 75, "R=%d, G=%d, B=%d", TestR, TestG, TestB);
    client.publish("hall_strip_2/R_G_B", msg);
  }
//*****************
  //Выбрать пресет
  else if (strcmp(topic, "STRIP/HALL_STRIP/run/switchToPreset") == 0) {    
    currPreset = call.toInt();
    prevPreset=currPreset;
    
    if (currPreset > 0) {
      //Включаем ленту
      presetStrip();
      //Глобальная отметка включения:
      switchedStrip = true;
    }
    else {
      PowerStrip(0, 0, 0, 0);
      //Глобальная отметка включения:
      switchedStrip = false;
    }
  }
//*****************
  //Старая система. Уходим от неё
  //При включении/перезагрузке получаем настроечный jSon и заполняем настроечные массивы
  else if (currPreset >0 && strcmp(topic, "strip/presets/hall") == 0) {
    //Serial.println(charCall);

    //Создаем jSon объект
    DynamicJsonBuffer jsonBuffer;
    JsonObject& jObj = jsonBuffer.parseObject(charCall);
    //jObj.prettyPrintTo(Serial);

    //Сохраняю настройки
    for(int y=1; y<=5; y++){
      for(int i=0; i<6; i++){
        stripConfR [y][i]=jObj[String(y)]["r"][i];
        stripConfG [y][i]=jObj[String(y)]["g"][i];
        stripConfB [y][i]=jObj[String(y)]["b"][i];
        stripConfW [y][i]=jObj[String(y)]["w"][i];
        stripConfBr[y][i]=jObj[String(y)]["br"][i];
      }
    }

    /*Serial.println();
    for(int i=0; i<6; i++){
      Serial.print("w");
      Serial.print(i);
      Serial.print("=");
      Serial.println(stripConfW[1][i]);
    }*/
    
    //После получения и обработки jSon отписываюсь от этой очереди, чтобы не читать сообщения, которые сам же и отправляю
    //Таким образом получаю их только при запуске контроллера или реконнекте к MQTT
    client.unsubscribe("strip/presets/hall");

    //Освобождаем память буфера
    jsonBuffer.clear();
  }
//*****************
  //обработчик выбора частей для настройки
  else if (top.indexOf("strip/hall/conf/parts") != -1 ) {
    int part = (top.substring(top.indexOf("parts/") + 6)).toInt();
    stripConfParts[part] = call.toInt();
  }
//*****************
  //обработчик RGB
  else if (currPreset >0 && strcmp(topic, "strip/hall/conf/rgb") == 0) {
    uint32_t sourceInt = call.toInt();
    int r = (sourceInt / 256) / 256;
    int g = (sourceInt / 256) % 256;
    int b = sourceInt % 256;
    for (int i=1; i<=6; i++) {
      if (stripConfParts[i]) {
        stripConfR[currPreset][i-1] = r;
        stripConfG[currPreset][i-1] = g;
        stripConfB[currPreset][i-1] = b;
      }
    }
    //Отправляем JSon обратно в очередь
    savePresets();
    //Включаем ленту
    presetStrip();
  }
//*****************
  //обработчик белого цвета
  else if (currPreset >0 && strcmp(topic, "strip/hall/conf/white") == 0) {
    for (int i=1; i<=6; i++) {
      if (stripConfParts[i]) stripConfW[currPreset][i-1] = call.toInt();
    }
    //Отправляем JSon обратно в очередь
    savePresets();
    //Включаем ленту
    presetStrip();
  }
//*****************
  //обработчик яркости
  else if (currPreset >0 && strcmp(topic, "strip/hall/conf/bright") == 0) {
    for (int i=1; i<=6; i++) {
      if (stripConfParts[i]) stripConfBr[currPreset][i-1] = call.toInt();
    }
    //Отправляем JSon обратно в очередь
    savePresets();
    //Включаем ленту
    presetStrip();
  }  
//*****************
  //Ластик
  else if (currPreset >0 && strcmp(topic, "strip/hall/conf/clearCurrPreset") == 0) {
    for (int i=1; i<=6; i++) {
      if (stripConfParts[i]) {
        stripConfR[currPreset][i-1] = 0;
        stripConfG[currPreset][i-1] = 0;
        stripConfB[currPreset][i-1] = 0;
        stripConfW[currPreset][i-1] = 0;
        stripConfBr[currPreset][i-1] = 100;
      }
    }

    client.unsubscribe("strip/hall/conf/#");
    client.publish("strip/hall/conf/parts/1", "1", true);
    client.publish("strip/hall/conf/parts/2", "1", true);
    client.publish("strip/hall/conf/parts/3", "1", true);
    client.publish("strip/hall/conf/parts/4", "1", true);
    client.publish("strip/hall/conf/parts/5", "1", true);
    client.publish("strip/hall/conf/parts/6", "1", true);     
    client.publish("strip/hall/conf/rgb", "0", false);
    client.publish("strip/hall/conf/white", "0", false);
    client.publish("strip/hall/conf/bright", "100", false);
    client.subscribe("strip/hall/conf/#");

    //Отправляем JSon обратно в очередь
    savePresets();
    //Включаем ленту
    presetStrip();
  }
//*****************


 
}   //*********************************************************************************************************************************
void savePresets(){
  //отладка (потом убрать)
  /*Serial.println();
  for (int i=1; i<=6; i++) {
    Serial.print("part-");
    Serial.print(i);
    Serial.print("=");
    Serial.println(stripConfParts[i]);
  }//конец отладки*/
      
  char chrOutPresets[1000];
  memset(&chrOutPresets, 0, sizeof(chrOutPresets));
 
  StaticJsonBuffer<500> jsonOutBuffer;
  
  for (int y=1; y<=5; y++){      
    JsonObject& jOut = jsonOutBuffer.createObject();

    //Создаем jМассивы
    JsonArray& jRed     = jOut.createNestedArray("r");
    JsonArray& jGreen   = jOut.createNestedArray("g");
    JsonArray& jBlue    = jOut.createNestedArray("b");
    JsonArray& jWhite   = jOut.createNestedArray("w");
    JsonArray& jBright  = jOut.createNestedArray("br");

    //Наполняем jМассивы
    for(int i=0; i<6; i++){
      jRed.add    (stripConfR[y][i]);
      jGreen.add  (stripConfG[y][i]);
      jBlue.add   (stripConfB[y][i]);
      jWhite.add  (stripConfW[y][i]);
      jBright.add (stripConfBr[y][i]);
    }

    //Serial.println(y);
    //jOut.printTo(Serial);
    //Serial.println();
    char chrOnePreset[200];
    jOut.printTo(chrOnePreset);

    //Очистка jБуфера
    jsonOutBuffer.clear();
    
    sprintf(chrOutPresets, "%s%s%d:%s%s",
                                          chrOutPresets,
                                          (y==1?"{":""),
                                          y,
                                          chrOnePreset,
                                          (y!=5?",":"}"
                                          ));
    //Очистка текстового буфера внутри цикла
    memset(&chrOnePreset, 0, sizeof(chrOnePreset));
  } //конец внешнего цикла

  Serial.println();
  Serial.println(chrOutPresets);

  client.publish("strip/presets/hall", chrOutPresets, true);
  //Очистка текстового буфера снаружи цикла
  memset(&chrOutPresets, 0, sizeof(chrOutPresets));
}

//Непосредственное зажигание ленты по частям
void presetStrip() {
/*
1 - 23
2 - 49
3 - 26
4 - 60
5 - 23
6 - 36

*/
  
  for (int currPart=0; currPart<6; currPart++) {
    int start = 0;
    int stp = 0;
    switch (currPart) {
      case 5:
        start = 40;
        stp = 51;
        break;
      case 4:
        start = 32;
        stp = 40;
        break;
      case 3:
        start = 24;
        stp = 32;
        break;
      case 2:
        start = 16;
        stp = 24;
        break;
      case 1:
        start = 8;
        stp = 16;
        break;
      case 0:
        start = 0;
        stp = 8;
        break;        
    }
    for (int i=start; i<stp; i++) {
      int r = stripConfR[currPreset][currPart];
      //для следующих двух позиций изменил соответствие для ленты в корридоре//в деревне откатил
      int g = stripConfG[currPreset][currPart];
      int b = stripConfB[currPreset][currPart];
      int w = stripConfW[currPreset][currPart];
      strip.setPixelColor(i, strip.Color(r, g, b));
    }
  
  } //конец основного цикла
  strip.show();
}

//*******
