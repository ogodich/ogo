#define BOARD_NAME "HALL"
#define BOARD_PASS "hall"
//-=-=-=-=-=-=-=-=-=-=-=-

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#include <ArduinoJson.h>

#include <myLib.h>

/*_______________________________________________________________________________________________________________________________*/
const String DEVICES[] = {"lobby","hall"};

const char* ssid = "ScorpGOS";
const char* password = "9031124945";
const char* mqtt_server = "192.168.1.1";

//const char* ssid = "berloga";
//const char* password = "plutonnastya";
//const char* mqtt_server = "192.168.8.101";

const int lights[2] = {D5, D6};
const int numButtons = 3; const int buttons[numButtons] = {D2, D7, D1};
const int smartLedPin = D3;
const int smartLeds[3]={2, 0, 1};         //Обычно {0,1,2}, но зависит от последовательности светодиодов на плате

boolean lightStat[3] = {false, false, false};

WiFiClient espClient;
PubSubClient client(espClient);
AsyncTask     async;
ButtonHandler   buttonHandler;

void ICACHE_RAM_ATTR interruptButtons ();   //это обязательная хрень после перехода на IDE 1.8.10. Что-то связанное с памятью про прерывания


char msg[50];

Adafruit_NeoPixel smartLed = Adafruit_NeoPixel(3, smartLedPin, NEO_GRB + NEO_KHZ800);

boolean offline = false;
long mqttLastReconnect = 0;

bool autoBrightnessMode = false;
int CurrenSwitchtBrightness = 100;

boolean gameStarted = false;
int gameCurrButton = 3;

int arrGameNumber[15] = {-1,    -1,-1,-1,-1,-1,    0,1,2,-1,-1,   -1,-1,-1,-1};

long SwitchBrightTimer = 0.0;
int PrevLigthSensorVal = 0;
int CurLigthSensorVal = 0;

//home profiles-------------------------------------------
String currentProfileNo = "";
String jSonProfiles = "";
const int cDevices = sizeof(DEVICES) / sizeof(DEVICES[0]);
const char addLogTopic[]="profiles/log/add";
bool logEnable=false;
//--------------------------------------------------------
