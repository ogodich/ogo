//процедура обработки прерывайний
void interruptButtons() {
  //Отображение информации о нажатиях
  buttonHandler.showPresses();

  if(!gameStarted){
  //КОМБИНАЦИИ
      //включить радио----------------------------------------------------------------------------------------------
      if(buttonHandler.longPresses[1]==1 && buttonHandler.shortPresses[0]==1) async.addTask("radio", "2", false);   
    
      //Понижаем громкость, если зажата II и нажата больше одного I
      else if (buttonHandler.longPresses[1]==1 && buttonHandler.shortPresses[0]>1)  {
        snprintf (msg, 5, "%d", (buttonHandler.shortPresses[0]-1)*(-1));
        async.addTask("volume_change", msg, false);
      }
      //Повышаем громкость, если зажата II и нажата больше одного III
      else if (buttonHandler.longPresses[1]==1 && buttonHandler.shortPresses[2]>1)  {
        snprintf (msg, 5, "%d", (buttonHandler.shortPresses[2]-1));
        async.addTask("volume_change", msg, false);
      }

      //Устанавливаем профиль ленты в шкафу, удерживая III и нажимая n раз II
      else if (buttonHandler.longPresses[2]==1 && buttonHandler.shortPresses[1]>0)  {
        snprintf (msg, 5, "%d", buttonHandler.shortPresses[1]);
        async.addTask("devices/hallStrip1/status", msg, true);
      }
      //Устанавливаем профиль потолочной ленты в прихожей, удерживая I и нажимая n раз II
      else if (buttonHandler.longPresses[0]==1 && buttonHandler.shortPresses[1]>0)  {
        snprintf (msg, 5, "PL=%d", buttonHandler.shortPresses[1]);
        async.addTask("wled/hall/api", msg, false);
      }
      
      //Всеобщий пресет - 2
      else if (buttonHandler.longPresses[0]==1 && buttonHandler.longPresses[2]==1) async.addTask("homeLightPresets", "2", false);
      
      //Умная розетка 2
      else if (buttonHandler.longPresses[0]==1 && buttonHandler.longPresses[1]==1) client.publish("remote_socket/2", "-1", false);
      //Умная розетка 1
      else if (buttonHandler.longPresses[1]==1 && buttonHandler.longPresses[2]==1) client.publish("devices/rozetka1/status/binar", BOARD_NAME);
      
  //ПЕРВАЯ кнопка-------------------------------------------------------------------------------------------------  
  else if (buttonHandler.shortPresses[0]==1) {
    if(offline){
      lightStat[0] = !lightStat[0];
      switchLights();
      refreshIndicators();
    }
    else async.addTask("devices/hall/status/binar", BOARD_NAME, false);
  }
  else if (buttonHandler.longPresses[0]==1) async.addTask("devices/bedroomPicture/status/binar", BOARD_NAME, false);

  //ВТОРАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (buttonHandler.shortPresses[1]==1) async.addTask("devices/hallStrip1/status/binar", BOARD_NAME, false);
  else if (buttonHandler.longPresses[1]==1) async.addTask("wled/hall", "T", false);
    
  //ТРЕТЬЯ кнопка-------------------------------------------------------------------------------------------------
  //ЛЕНТА. Прихожая. Профиль 1 / ВЫКЛ
  else if (buttonHandler.shortPresses[2]==1) {
    if(offline){
      lightStat[1] = !lightStat[1];
      switchLights();
      refreshIndicators();
    }
    else async.addTask("devices/lobby/status/binar", BOARD_NAME, false);
  }
  else if (buttonHandler.longPresses[2]==1) async.addTask("devices/kitStrip2/status/binar", BOARD_NAME, false);
}
  
  //Игра  
      else if(gameStarted){
      if      (buttonHandler.shortPresses[0]==1 || buttonHandler.longPresses[0]==1) async.addTask("GAME/pressed", "6", false);
      else if (buttonHandler.shortPresses[1]==1 || buttonHandler.longPresses[1]==1) async.addTask("GAME/pressed", "7", false);
      else if (buttonHandler.shortPresses[2]==1 || buttonHandler.longPresses[2]==1) async.addTask("GAME/pressed", "8", false);
  }




}
