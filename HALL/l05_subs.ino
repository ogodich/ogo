void get_brightness(){
  long now = millis();  
  if (now - SwitchBrightTimer > 3000) {
    CurLigthSensorVal = analogRead(A0);
    /*Serial.print("A0=");
    Serial.print(analogRead(A0));
    Serial.print(", millis=");
    Serial.println(millis());*/
    
    if (fabs(CurLigthSensorVal-PrevLigthSensorVal) > 20){
      //В зависимости от схемы подключения фоторезистера возможна как прямая зависимость от яркости, так и обратная
      //CurrenSwitchtBrightness = map(CurLigthSensorVal,      0, 800,      3, 100);
      CurrenSwitchtBrightness = map(CurLigthSensorVal,      1024, 60,      3, 100);
      CurrenSwitchtBrightness = CurrenSwitchtBrightness < 0 ? 0 : (CurrenSwitchtBrightness > 100 ? 100 : CurrenSwitchtBrightness);

      //Управляем яркостью выключателя в прихожей
      snprintf (msg, 5, "%d", CurrenSwitchtBrightness);
      client.publish("settings/switch/hall/bright", msg, true);
      
      //Также по этому же сенсору управляем яркостью выключателя в коридоре (пока там нет своего сенсора), но немного поярче
      int corridorSwitchtBrightness = CurrenSwitchtBrightness < 100-6 ? CurrenSwitchtBrightness + 6 : CurrenSwitchtBrightness;
      snprintf (msg, 5, "%d", corridorSwitchtBrightness);
      client.publish("settings/switch/coridor/bright", msg, true);  
      
      //Передаем также значение сенсора для мониторинга
      snprintf (msg, 5, "%d", CurLigthSensorVal);
      client.publish("settings/switch/hall/bright/log", msg, true);  

      /*Serial.print("CurLigthSensorVal=");
      Serial.print(CurLigthSensorVal);
      Serial.print(", PrevLigthSensorVal=");
      Serial.print(PrevLigthSensorVal);
      Serial.print(", razn=");
      Serial.print(fabs(CurLigthSensorVal-PrevLigthSensorVal));
      Serial.print(",");
      Serial.println(millis());*/

      PrevLigthSensorVal = CurLigthSensorVal;
    }

    SwitchBrightTimer = now;
  }
}


void refreshIndicators(){
  //1
  if      (lightStat[0] && offline)   smartLed.setPixelColor(smartLeds[0], smartLed.Color(0,200,50));
  else if (lightStat[0])              mySetColor(smartLeds[0], 0,200,0);
  else if (!lightStat[0] && offline)  smartLed.setPixelColor(smartLeds[0], smartLed.Color(250,0,50));
  else if (!lightStat[0])             mySetColor(smartLeds[0], 250,0,0);
  
  //2
  if      (offline)        smartLed.setPixelColor(smartLeds[1], smartLed.Color(30,0,5));  
  else if (lightStat[2])   mySetColor(smartLeds[1], 0,200,0);
  else if (!lightStat[2])  mySetColor(smartLeds[1], 250,0,0);

  //3
  if      (lightStat[1] && offline)   smartLed.setPixelColor(smartLeds[2], smartLed.Color(0,200,50));
  else if (lightStat[1])              mySetColor(smartLeds[2], 0,200,0);
  else if (!lightStat[1] && offline)  smartLed.setPixelColor(smartLeds[2], smartLed.Color(250,0,50));
  else if (!lightStat[1])             mySetColor(smartLeds[2], 250,0,0);
   
  smartLed.show(); // This sends the updated pixel color to the hardware.
}

void mySetColor(int pix, int r, int g, int b){
  smartLed.setPixelColor(pix, smartLed.Color(r * CurrenSwitchtBrightness / 100, g * CurrenSwitchtBrightness / 100, b * CurrenSwitchtBrightness / 100));
}

void animation_1 (){
  for (int i=0; i<250; i++){
    smartLed.setPixelColor(0, smartLed.Color(250-i, i < 200 ? i : 200, 0));
    delay(3);    
    smartLed.show();
    i=i+10;
  }
}

void animation_2 (){
  for (int i=0; i<250; i++){
    smartLed.setPixelColor(0, smartLed.Color(i, 200 - (i < 200 ? i : 200), 0));
    delay(3);    
    smartLed.show();
    i=i+10;
  }
}

void switchLights(){
  digitalWrite(lights[0], !lightStat[0]);
  digitalWrite(lights[1], !lightStat[1]);

  for(int i=0; i < sizeof lightStat; i++){
    char boof[100];
    snprintf(boof, 100, "lightStat[%d]=%d;", i, lightStat[i]);
    Serial.println(boof);
  }
}

void otaInit(){
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
   ArduinoOTA.setPassword((const char *)BOARD_PASS);

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();  
}
