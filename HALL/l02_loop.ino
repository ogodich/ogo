void loop() {
  mqtt_loop();
  if(!gameStarted && autoBrightnessMode) get_brightness();

  if(!offline) {
    ArduinoOTA.handle();
  }

  //Отправка асинхронных сообщений
  if(async.doTask()) {
    char msg[100];
    sprintf(msg,"%d\tLOOP\tI have published %s message '%s' to the topic '%s'",
      millis(),
      async.retain ? "retain" : "not retain",
      async.value,
      async.topic);
    if(client.publish(async.topic, async.value, async.retain)) Serial.println(msg);  
  }

}
