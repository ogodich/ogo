void mqtt_loop(){
  if (WiFi.waitForConnectResult() == WL_CONNECTED) {
    if (!client.connected()) {
      reconnect();
    }
    else client.loop();
  }  
}

void mySubscribtion(){
  client.subscribe("light/hall/#");
  client.subscribe("devices/hallStrip1/status");
  client.subscribe("settings/switch/hall/bright/#");
  client.subscribe("GAME/#");

  subscribeToDevicesStatus();
  client.subscribe("profiles/run/#");
  client.subscribe("profiles/change/log_enable");
}

void reconnect() {
  if (!offline || (offline && millis() - mqttLastReconnect > 5000)){
    Serial.print("WiFi.waitForConnectResult()=");
    Serial.println(WiFi.waitForConnectResult());
    Serial.print("Attempting MQTT connection...");
    
    // Если удалось подключиться
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      //Если пришли из офлайна, отправляем изменения
      if(offline) sendFromOffline();
      
      offline = false;
      // ... and resubscribe
      mySubscribtion();
    
    // Если не удалось подключиться
    } else {
      Serial.print("failed, rc=");
      Serial.println(client.state());
      offline = true;
      mqttLastReconnect = millis();

      refreshIndicators(); //только для устройств со светодиодами!!!
    }
       
  }
}

void sendFromOffline(){
  snprintf (msg, 75, "%d", lightStat[0]);
  client.publish("devices/hall/status", msg, true);  
  snprintf (msg, 75, "%d", lightStat[1]);
  client.publish("devices/lobby/status", msg, true);  
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();
//*********************************************************************************************************************************
  //----------------------  hall  ---------------------------------
  if (strcmp(topic,"devices/hall/status/binar")==0){
          lightStat[0] = !lightStat[0];
          client.publish("devices/hall/status", &String(lightStat[0])[0u], true);
  }
  else if (strcmp(topic,"devices/hall/status")==0){
          lightStat[0] = call.toInt();  
          switchLights();
          refreshIndicators();
  }

  //---------------------------  lobby  ---------------------------
  else if (strcmp(topic,"devices/lobby/status/binar")==0){
          lightStat[1] = !lightStat[1];
          client.publish("devices/lobby/status", &String(lightStat[1])[0u], true);
  }
  else if (strcmp(topic,"devices/lobby/status")==0){
          lightStat[1] = call.toInt();  
          switchLights();
          refreshIndicators();
  }
 
  else if (strcmp(topic,"devices/hallStrip1/status")==0){
    if(call.toInt()==0) lightStat[2] = false;
    if(call.toInt()>0)  lightStat[2] = true;
    refreshIndicators();
  }

  else if (strcmp(topic,"settings/switch/hall/bright/auto")==0){
    autoBrightnessMode = call.toInt();
  }

  else if (strcmp(topic,"settings/switch/hall/bright")==0){
    CurrenSwitchtBrightness = call.toInt();
    refreshIndicators();
  }

  else if (String(topic).indexOf("GAME/") != -1) {
    game(topic, call);
  }

  //----------ПРОФИЛИ
  else if (String(topic).indexOf("profiles/run/") != -1) {
      RunProfiles(topic, String(call));
  }
  else if (strcmp(topic,"profiles/change/log_enable")==0){
      logEnable = call.toInt();
  }//----------------


//*********************************************************************************************************************************
}
