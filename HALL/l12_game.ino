void game(char* top, String pay){
/*  
char boof[100];
snprintf(boof,100,"%s, i=%d",boof, i);
client.publish("GAME/log", boof);
*/
  //===========================================================================================
  if(String(top)=="GAME/start"){
    gameStarted = pay.toInt();

    //Началась игра. Гасим все светодиоды
    if(gameStarted) {
      game_leds_off();
    }
    //Кончилась игра
    else refreshIndicators();
  }
  //===========================================================================================
  if(String(top)=="GAME/currButton"){
    DynamicJsonBuffer gameJSonBuff;
    JsonObject& gameJSon = gameJSonBuff.parseObject(pay);
    int gameCurrButton = gameJSon["button"];
    /*if (!gameJSon.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    gameJSon.prettyPrintTo(Serial);*/
    
    //int gameCurrButton=pay.toInt();
    //Если полученное число наше, включаем нужный светодиод
    if(arrGameNumber[gameCurrButton] > -1) game_set_led(arrGameNumber[gameCurrButton], gameJSon["color"][0], gameJSon["color"][1], gameJSon["color"][2]);
    //Если полученное число не наше, гасим все светодиоды
    else game_leds_off();
  }
  //===========================================================================================
  //===========================================================================================
  //===========================================================================================
}

void game_set_led(int pix, int r, int g, int b){
  for(auto i : arrGameNumber) if(i>-1) smartLed.setPixelColor(smartLeds[i], smartLed.Color(0,0,0));
  smartLed.setPixelColor(smartLeds[pix], smartLed.Color(r,g,b));
  smartLed.show();
}
void game_leds_off(){
  for(auto i : arrGameNumber) if(i>-1)   smartLed.setPixelColor(smartLeds[i], smartLed.Color(0,0,0));
  smartLed.show();
}
