void setup() {
  Serial.begin(115200);
  Serial.println("Booting");

  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed!");
    delay(500);
    if(millis() > 5000) break;
  }
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  otaInit();

  //pinMode(buttons[0], INPUT_PULLUP);
  //pinMode(buttons[1], INPUT_PULLUP);
  //pinMode(buttons[2], INPUT_PULLUP);

  pinMode(lights[0], OUTPUT);
  pinMode(lights[1], OUTPUT);
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);

  pinMode(A0, INPUT);
  
  //умные светодиоды
  smartLed.begin(); // This initializes the NeoPixel library.
  smartLed.setBrightness(255); // set brightness
  smartLed.show(); // Initialize all pixels to 'off'

  //Прерывания по кнопкам
  buttonHandler.buttonsNumber=numButtons; //default is 10
  for(int i=0; i<buttonHandler.buttonsNumber; i++) {
    buttonHandler.buttonPins[i]=buttons[i];
    attachInterrupt(buttonHandler.buttonPins[i], interruptButtons, RISING);
  }
  
}
