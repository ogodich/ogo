#define LIGHT_FIRST      5
#define LIGHT_SECOND    3
#define BUTTON_PIN_1   2
#define BUTTON_PIN_2   4

#include <myFirstClass.h>
myFirstClass firstButton(BUTTON_PIN_1, true);     // создаём экземпляр нашего класса
myFirstClass secondButton(BUTTON_PIN_2, true);     // создаём экземпляр нашего класса


int lightsMap[] = {0, 0};
int curMode = 0;


void setup() {
  pinMode(LIGHT_FIRST,  OUTPUT);
  pinMode(LIGHT_SECOND, OUTPUT);
  pinMode(BUTTON_PIN_1, INPUT);
  pinMode(BUTTON_PIN_2, INPUT);
}

boolean switchLight = false;
void loop() {
  if (firstButton.funkMyClickCheck() == "short" || secondButton.funkMyClickCheck() == "short") {
    curMode++;
    curMode=curMode%4;
  }


  if (curMode == 0) {
    analogWrite(LIGHT_FIRST,  0); 
    analogWrite(LIGHT_SECOND, 0);
  }
  else if (curMode == 1) {
    analogWrite(LIGHT_FIRST,  255); 
    analogWrite(LIGHT_SECOND, 255);
  }
  else if (curMode == 2) {
    analogWrite(LIGHT_FIRST,  255); 
    analogWrite(LIGHT_SECOND, 0);
  }
  else if (curMode == 3) {
    analogWrite(LIGHT_FIRST,  0); 
    analogWrite(LIGHT_SECOND, 255);
  }

}
