/*****************************************************************************************************************************************/
void web_server() {
    // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  //Serial.println("new client");

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  //Serial.println(request);
  client.flush();
 
  // Match the request
   if (request.indexOf("/SWITCH1") != -1)  {
    lightStat[0] =!lightStat[0];
    delay(10);
    digitalWrite(lights[0], lightStat[0]);
    delay(10);
    //Send_my_status();
    statusChanged = true;
    
    if (lightStat[0]) resp1 = "CooHooOff";
    else resp1 = "CooHooOn";
  }

  else if (request.indexOf("/BOUNCE") != -1)  {
    resp1 = "BOUNCE=\n";
    resp1 += "Pin1=";
    resp1 += bounce[buttons[1]];
    resp1 += ",\nPin2=";
    resp1 += bounce[buttons[2]];
    resp1 += ",\nPin3=";
    resp1 += bounce[buttons[3]];
  }

  /*else if (request.indexOf("/UINT") != -1)  {
    myuint();
  }*/
 
  else if (request.indexOf("/BOOTING") != -1)  {    
    startTime = millis() + 10000;
    startBoot = true;
    resp1 = "BOOT ME";
  }

  String resp = "HTTP/1.1 200 OK\n";
  //resp = resp + "Content-Type: text/html\n";
  resp = resp + "\n";
  //resp = resp + millis();
  //resp = resp + "<!DOCTYPE HTML>\n";
  //resp = resp + "<html>\n";
  resp = resp + resp1;
  // resp = resp + "\n</html>";
  client.println(resp);

  float heap = ESP.getFreeHeap();

  delay(1);
  //Serial.println("Client disonnected");
  //Serial.println("");

  //client.stop();
}

/*****************************************************************************************************************************************/
void web_client(String request) {
 
 if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status

   HTTPClient http;    //Declare object of class HTTPClient
   http.setTimeout(10);
 
   http.begin(request);      //Specify request destination
   http.addHeader("Content-Type", "text/plain");  //Specify content-type header

   http.setTimeout(10);
   int httpCode = http.GET();   //Send the request
   //String payload = http.getString();                  //Get the response payload
 
   ////Serial.println(httpCode);   //Print HTTP return code
   ////Serial.println(payload);    //Print request response payload
 
   http.end();  //Close connection
 
 }else{
    //Serial.println("Error in WiFi connection");   
 }
 
}
