#include <SoftwareSerial.h>
#include <DFPlayer_Mini_Mp3.h>

#define LED_PIN_1 2
#define LED_PIN_2 3
#define LED_PIN_3 4
#define LED_PIN_4 5
#define LED_PIN_5 6
#define LED_PIN_6 7
#define CONTACT_PIN_1 8
#define CONTACT_PIN_2 9
#define CONTACT_PIN_3 10
#define CONTACT_PIN_4 11
#define CONTACT_PIN_5 12
#define CONTACT_PIN_6 13
//#define RELE A0

#define CHANGING_TIME_OUT 130
#define PAIRS_NUMBER 6

int currChannel = 0;
int prevChannel = 0;

int leds[PAIRS_NUMBER] = {2, 3, 4, 5, 6, 7};
int contacts[PAIRS_NUMBER] = {8, 9, 10, 11, 12, 13};

void setup() {
  Serial.begin(9600);
  pinMode(LED_PIN_1, OUTPUT);
  pinMode(LED_PIN_2, OUTPUT);
  pinMode(LED_PIN_3, OUTPUT);
  pinMode(LED_PIN_4, OUTPUT);
  pinMode(LED_PIN_5, OUTPUT); 
  pinMode(LED_PIN_6, OUTPUT);   
  pinMode(CONTACT_PIN_1, INPUT_PULLUP);
  pinMode(CONTACT_PIN_2, INPUT_PULLUP);
  pinMode(CONTACT_PIN_3, INPUT_PULLUP);
  pinMode(CONTACT_PIN_4, INPUT_PULLUP);
  pinMode(CONTACT_PIN_5, INPUT_PULLUP);
  pinMode(CONTACT_PIN_6, INPUT_PULLUP);  
//  pinMode(RELE, OUTPUT);
//  analogWrite(RELE, 1024);

  mp3_set_serial (Serial);      //set Serial for DFPlayer-mini mp3 module 
  delay(1);                     // delay 1ms to set volume
  mp3_set_volume (30);          // value 0~30

}

void loop()
{
  //Если есть контакт
  if (digitalRead(contacts[currChannel]) == LOW) {
    //Играем
    digitalWrite(leds[currChannel], HIGH);
    mp3_play (1);
    //    analogWrite(RELE, 0);
    
    //Ждем окончания лимита времени
    int i = 0;
    while (digitalRead(contacts[currChannel]) == LOW
           && i < CHANGING_TIME_OUT * 2){
      delay(500);
      i++;
    }

    //Умолкаем
    mp3_stop ();
    ChangingTheChannel();
    //    analogWrite(RELE, 1024);    
  }
  
  else {
    blinking();
  }
}

/*********************************************************************************/

void ChangingTheChannel()
{
    digitalWrite(leds[currChannel], LOW);
    while(currChannel == prevChannel) {
      currChannel = random(PAIRS_NUMBER);
    }
    prevChannel = currChannel;
 }
/*********************************************************************************/
void blinking()
{
  if (digitalRead(contacts[currChannel]) == HIGH) {
    digitalWrite(leds[currChannel], HIGH);
    delay(200);
  }

  if (digitalRead(contacts[currChannel]) == HIGH) {
    digitalWrite(leds[currChannel], LOW);
    delay(200);
  }

  if (digitalRead(contacts[currChannel]) == HIGH) {
    digitalWrite(leds[currChannel], HIGH);
    delay(200);
  }

  if (digitalRead(contacts[currChannel]) == HIGH) {
    digitalWrite(leds[currChannel], LOW);
    delay(500);
  }
}

