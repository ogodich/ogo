int lights[4] = {12,13,15,10};
int buttons[3] = {16,5,4};
int buttonLeds[3] = {0,2,14};
boolean lightStat[4] = {true, true, true, true};

void setup() {
  pinMode(buttons[0], INPUT); 
  pinMode(buttons[1], INPUT); 
  pinMode(buttons[2], INPUT); 

  pinMode(buttonLeds[0], OUTPUT);
  pinMode(buttonLeds[1], OUTPUT);
  pinMode(buttonLeds[2], OUTPUT);
  digitalWrite(buttonLeds[0], HIGH);
  digitalWrite(buttonLeds[1], HIGH);
  digitalWrite(buttonLeds[2], HIGH);
  
  pinMode(lights[0], OUTPUT);
  pinMode(lights[1], OUTPUT);
  pinMode(lights[2], OUTPUT);
  pinMode(lights[3], OUTPUT);    
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[3], HIGH);
  Serial.begin(115200);

}

void loop() {
  //delay(1000);
  int i;
for (i = 0; i < 5; i = i + 1) {
  //Serial.println(lightStat[i]);

}
  //Serial.println(millis());
  
  //Первая кнопка-------------------------------------------------------------------------------------------------
  if (return_button(buttons[0]) > 2) {
    lightStat[0] =!lightStat[0];
    digitalWrite(lights[0], lightStat[0]);
    digitalWrite(buttonLeds[0], lightStat[0]);
  }
  //Вторая кнопка-------------------------------------------------------------------------------------------------
  if (return_button(buttons[1]) > 2) {
    lightStat[1] =!lightStat[1];
    digitalWrite(lights[1], lightStat[1]);
    digitalWrite(buttonLeds[1], lightStat[1]);
  }
 
 //Третья кнопка-------------------------------------------------------------------------------------------------
  int butStatus=return_button(buttons[2]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) {
      if(lightStat[3]==false) lightStat[2] = true; //Если включен свет по длинному нажатию, по короткому его тоже выключаем
      else lightStat[2] =!lightStat[2];
      lightStat[3] = true;
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      lightStat[3] =!lightStat[3];
      lightStat[2] = true;
    }
   
   //Обрабатываем общий индикатор на два света
   if (lightStat[2] == true && lightStat[3] && true) digitalWrite(buttonLeds[2], true);
   else digitalWrite(buttonLeds[2], false);

   //Непосредственно включаем свет в соответствии с переменными
   digitalWrite(lights[2], lightStat[2]);
   digitalWrite(lights[3], lightStat[3]);
  }
}

/*
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
*/
//Функция возвращает количество итераций по 5 милисекунд за время нажатой кнопки
int return_button(int buttonPin) {
  int i = 0;
  boolean buttonDown = digitalRead(buttonPin)==HIGH;
  if (buttonDown) {
    while (digitalRead(buttonPin)==HIGH)
    {
      delay(5);
      i=i+1;
    }
  //delay(1000);
  }
  return i;
}
