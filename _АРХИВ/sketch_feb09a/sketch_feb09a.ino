void setup() {
  Serial.begin(9600);
  IntToRGB (16777215, "white");
  IntToRGB (16711680, "red");
  IntToRGB (524032, "green");
  IntToRGB (767, "blue");
  IntToRGB (16765440, "yellow");  
}

void loop() {

}

void IntToRGB (uint32_t sourceInt, String color) {
  int r = (sourceInt/256)/256;
  int g = (sourceInt/256)%256;
  int b = sourceInt%256;

  
  Serial.print("IntToRGB:");
  Serial.print("sourceInt");
  Serial.print("(");
  Serial.print(color);
  Serial.print(")=");
  Serial.print(sourceInt);
  Serial.print(", r=");
  Serial.print(r);
  Serial.print(", g=");
  Serial.print(g);
  Serial.print(", b=");
  Serial.print(b);
  Serial.println("");  
}

