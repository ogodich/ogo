/* Node MCU D1 mini
Pin  Function  ESP-8266 Pin
TX  TXD TXD
RX  RXD RXD
A0  Analog input, max 3.3V input  A0
D0  IO  GPIO16
D1  IO, SCL GPIO5
D2  IO, SDA GPIO4
D3  IO, 10k Pull-up GPIO0
D4  IO, 10k Pull-up, BUILTIN_LED  GPIO2
D5  IO, SCK GPIO14
D6  IO, MISO  GPIO12
D7  IO, MOSI  GPIO13
D8  IO, 10k Pull-down, SS GPIO15 (Нельзя делать выходом, не стартует)
G Ground  GND
5V  5V  -
3V3 3.3V  3.3V
RST Reset RST
*/


/*
ESP 01 PINS
 1-2-3-4
 5-6-7-8
 antena
 antena

 1 - VCC
 2 - RST, reset on LOW
 3 - EN
 4 - TX, GPIO1, don't start on LOW
 5 - RX, GPIO3
 6 - BOOT, GPIO0, don't start on LOW
 7 - GPIO2, don't start on LOW
 8 - GND
*
*/


#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>

#define BOARD_NAME "NodeMCU_D1mini_ConnerSwitcher"

/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

WiFiServer server(80);

boolean startBoot = false;
double startTime = 0;

int light = 12;
int buttons[3] = {0,4,5};

String resp1;
String resp2;

void(* resetFunc) (void) = 0;

/*_______________________________________________________________________________________________________________________________*/
void setup() {
  Serial.begin(115200);

  //Serial.println("Booting");
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    //Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(BOARD_NAME);   //------------------------------------------------------------/

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printlnf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    //Serial.printlnf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  //Serial.println("Ready");
  //Serial.println("IP address: ");
  //Serial.println(WiFi.localIP());

  // Start the server
  server.begin();
  //Serial.println("Server started");
 
  //Print the IP address
  //Serial.println("Use this URL to connect: ");
  //Serial.println("http://");
  //Serial.println(WiFi.localIP());
  //Serial.println("/");

  pinMode(buttons[0], INPUT); 
  pinMode(buttons[1], INPUT); 
  pinMode(buttons[2], INPUT); 

  

  pinMode(light, OUTPUT);   
  digitalWrite(light, HIGH);

}

/*****************************************************************************************************************************************/
void loop() {
  my_switcher();
  if (startBoot) my_booting();
  else web_server();
}
/*****************************************************************************************************************************************/
void my_booting() {
  if (startTime > millis()) {
    ArduinoOTA.handle();
  }
  else {
    startBoot = false;
  }
}
/*****************************************************************************************************************************************/
void my_switcher() {
  //Первая кнопка-------------------------------------------------------------------------------------------------
  int butStatus=return_button(buttons[0]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) {
      web_client("http://192.168.1.12/SWITCH1");
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      digitalWrite(light, !digitalRead(light));
    }
  }
  //Вторая кнопка-------------------------------------------------------------------------------------------------
  butStatus=return_button(buttons[1]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) { 
      web_client("http://192.168.1.15/SWITCH1");
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      web_client("http://192.168.1.15/SWITCH1");
    }
  }
  //Третья кнопка-------------------------------------------------------------------------------------------------
  butStatus=return_button(buttons[2]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) {
      web_client("http://192.168.1.19/SWITCH1");
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      web_client("http://192.168.1.19/SWITCH1");
    }
  }

}
/*****************************************************************************************************************************************/
//Функция возвращает количество итераций по 5 милисекунд за время нажатой кнопки
int return_button(int buttonPin) {
  int i = 0;
  boolean buttonDown = digitalRead(buttonPin)==LOW;
  if (buttonDown) {
    while (digitalRead(buttonPin)==LOW)
    {
      delay(5);
      i=i+1;
    }
  //delay(1000);
  }
  return i;
}
/*****************************************************************************************************************************************/
void web_server() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  //Serial.println("new client");

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  //Serial.println(request);
  client.flush();
 
  // Match the request
  if (request.indexOf("/SWITCH1") != -1)  {
    digitalWrite(light, !digitalRead(light));
    resp1 = digitalRead(light);    
  }
  else if (request.indexOf("/BOOTING") != -1)  {    
    startTime = millis() + 10000;
    startBoot = true;
    resp1 = "BOOT ME";
  }
  else if (request.indexOf("/RESET") != -1)  {    
    resetFunc();
  }


  String resp = "HTTP/1.1 200 OK\n";
  resp = resp + "Content-Type: text/html\n";
  resp = resp + "\n";
  //resp = resp + "<!DOCTYPE HTML>\n";
  //resp = resp + "<html>\n";
  resp = resp + resp1;
  //resp = resp + "\n</html>";
  client.println(resp);
  
  //Serial.println("resp");
  //Serial.println(resp);
  //Serial.println("");

  float heap = ESP.getFreeHeap();;

  delay(1);
  //Serial.println("Client disonnected");
  //Serial.println("");

  //client.stop();

}

/*****************************************************************************************************************************************/

/*****************************************************************************************************************************************/
void web_client(String request) {
 
 if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
 
   HTTPClient http;    //Declare object of class HTTPClient
 
   http.begin(request);      //Specify request destination
   http.addHeader("Content-Type", "text/plain");  //Specify content-type header
 
   int httpCode = http.GET();   //Send the request
   String payload = http.getString();                  //Get the response payload

  /*if (payload.substring(0, 8) == "CooHooOn"){
    resp2 = "Coo Hoo On";
    CooHooStatus = true; //Вытяжку включили
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();
    
  }
  else if (payload.substring(0, 8) == "CooHooOf"){
    resp2 = "Coo Hoo OFF";
    CooHooStatus = false; //Вытяжку выключили
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();
    
  }
  else {
    resp2 = payload;
  }*/

  
   http.end();  //Close connection
 
 }else{
    //Serial.println("Error in WiFi connection");   
 }
 

}

