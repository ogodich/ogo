//---------------------------------------------------------------
void my_timer(int interval){
                                //когда получаем сообщение на установку включения/выключения таймера, делаем следующее
    timerCurLed = 0;            //сбрасываем текущую позицию таймера на ленте
    timerPrevLed = -1;          //сбрасываем техническую переменную предыдущей позиции таймера на ленте
    timerPassTime = 0;          //сбрасываем счетчик прошедшего времени

    if (interval == 0) runTimer = false;      //если пришел ноль, выключаем таймер
    else runTimer = true;                     //если пришло сообщение больше нуля, таймер нужно включить

    timerStop = millis() + interval * 60000;  //рассчитываем время окончания работы таймера

    timerInterval = interval * 60;            //рассчитываем длительность таймера в секундах
}

void run_my_timer() {
  long now = millis();
  if (now - timerRefresh > 1000) {
    timerRefresh = now;

    timerPassTime ++;                                               //инкремент пройденного времени

    double dblTimerCurLed = 212.0 / timerInterval * timerPassTime;  //рассчитываем текущую позицию последнего пикселя на ленте
    timerCurLed = dblTimerCurLed;                                   //(количество светодиодов ленты специально уменьшено,
    if (timerCurLed > 211) timerCurLed = 211;                       //...чтобы спереди были сопровождающие пиксели, а в конце расчитанный

    //snprintf (msg, 75, "timerInterval=%d, timerPassTime=%d, timerCurLed=%d",  timerInterval, timerPassTime, timerCurLed);
    //client.publish("strip/kitchen/timerLog", msg);

    if (timerCurLed != timerPrevLed){                               //меняем состояние пикселей только, если изменился рассчитанный адрес пикселя относительно прошлого раза
      presetStrip();                                                //Включаем текущий пресет ленты
      strip.setPixelColor(timerCurLed + 5,  strip.Color(100, 0, 0, 0));
      strip.setPixelColor(timerCurLed + 4,  strip.Color(100, 0, 0, 0));
      strip.setPixelColor(timerCurLed + 3,  strip.Color(0, 100, 0, 0));
      strip.setPixelColor(timerCurLed + 2,  strip.Color(0, 100, 0, 0));
      strip.setPixelColor(timerCurLed + 1,  strip.Color(0, 0, 100, 0));
      strip.setPixelColor(timerCurLed,      strip.Color(0, 0, 100, 0));
      strip.show();
    }

    //если время закончилось
    if (now > timerStop) {
      runTimer = false;
      timerCurLed = 0;            //сбрасываем текущую позицию таймера на ленте
      timerPrevLed = -1;          //сбрасываем техническую переменную предыдущей позиции таймера на ленте
      timerPassTime = 0;          //сбрасываем счетчик прошедшего времени
    }

  timerPrevLed = timerCurLed;
  /*---------------------------------------*/  
  }
}

