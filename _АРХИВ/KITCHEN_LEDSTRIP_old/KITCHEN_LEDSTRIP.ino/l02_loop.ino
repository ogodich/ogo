void loop(void){
  ArduinoOTA.handle();
  
  if (startBoot) my_booting();
  else {
    mqtt_loop();
    //getBright();

    myIrrGet();

    if (runTimer) run_my_timer();

    if (irrMode > 0) irrModeOff();
    if (irrMode > 0) run_irr_timer();
  
    /*if (debug != ""){
      debug.toCharArray(deb, 500);
      client.publish("debug", deb);
      debug = "";
    }*/
  }
  
}
