void loop(void){
  ArduinoOTA.handle();
  
  if (startBoot) my_booting();
  else {
    mqtt_loop();
    getBright();

    if (runTimer) run_my_timer();
  
    /*if (debug != ""){
      debug.toCharArray(deb, 500);
      client.publish("debug", deb);
      debug = "";
    }*/
  }
  
}
