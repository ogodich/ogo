void stripAnimation_1(){
  while (runLedAnimation){
      my_pixel_fade_in (11);
      my_pixel_fade_in (35);
      my_pixel_fade_in (59);
      my_pixel_fade_in (84);
      my_pixel_fade_in (107);
      my_pixel_fade_in (125);
      my_pixel_fade_in (146);
      my_pixel_fade_in (169);
      my_pixel_fade_in (198);

      stripAnimation_2();
      
    }
  }

void stripAnimation_2(){
  while (runLedAnimation){
      my_pixel_fade_out (11);
      my_pixel_fade_out (146);
      my_pixel_fade_out (84);
      my_pixel_fade_in (146);      
      my_pixel_fade_out (35);
      my_pixel_fade_in (84);
      my_pixel_fade_out (198);
      my_pixel_fade_in (11);
      my_pixel_fade_out (59);
      my_pixel_fade_in (35);  
      my_pixel_fade_out (107);
      my_pixel_fade_in (198);      
      my_pixel_fade_out (125);
  }
}

void my_pixel_fade_in (int pixel){
  for(int i=0; i<=30; i++){
    strip.setPixelColor(pixel, strip.Color(255 * i/100, 50 * i/100, 0, 150 * i/100));
      strip.show();
      delay(50);
      if(!runLedAnimation) break;
  }
  for(int i=30; i<=70; i++){
    strip.setPixelColor(pixel, strip.Color(255 * i/100, 50 * i/100, 0, 150 * i/100));
      strip.show();
      delay(20);
      if(!runLedAnimation) break;
  }
  for(int i=70; i<=100; i++){
    strip.setPixelColor(pixel, strip.Color(255 * i/100, 50 * i/100, 0, 150 * i/100));
      strip.show();
      delay(10);
      if(!runLedAnimation) break;
  }
}

void my_pixel_fade_out (int pixel){
  for(int i=0; i<=30; i++){
    strip.setPixelColor(pixel, strip.Color(255 - 255 * i/100, 50 - 50 * i/100, 0, 150 - 150 * i/100));
      strip.show();
      delay(80);
      if(!runLedAnimation) break;
  }
  for(int i=30; i<=70; i++){
    strip.setPixelColor(pixel, strip.Color(255 - 255 * i/100, 50 - 50 * i/100, 0, 150 - 150 * i/100));
      strip.show();
      delay(20);
      if(!runLedAnimation) break;
  }
  for(int i=70; i<=100; i++){
    strip.setPixelColor(pixel, strip.Color(255 - 255 * i/100, 50 - 50 * i/100, 0, 150 - 150 * i/100));
      strip.show();
      delay(10);
      if(!runLedAnimation) break;
  }
}


// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

void pulseWhite(uint8_t wait) {
  for(int j = 0; j < 256 ; j++){
      for(uint16_t i=0; i<strip.numPixels(); i++) {
          strip.setPixelColor(i, strip.Color(0,0,0, neopix_gamma[j] ) );
        }
        delay(wait);
        strip.show();
      }

  for(int j = 255; j >= 0 ; j--){
      for(uint16_t i=0; i<strip.numPixels(); i++) {
          strip.setPixelColor(i, strip.Color(0,0,0, neopix_gamma[j] ) );
        }
        delay(wait);
        strip.show();
      }
}


void rainbowFade2White(uint8_t wait, int rainbowLoops, int whiteLoops) {
  float fadeMax = 100.0;
  int fadeVal = 0;
  uint32_t wheelVal;
  int redVal, greenVal, blueVal;

  for(int k = 0 ; k < rainbowLoops ; k ++){
    if(runLedAnimation){
      for(int j=0; j<256; j++) { // 5 cycles of all colors on wheel
        if(runLedAnimation){
          for(int i=0; i< strip.numPixels(); i++) {
            if(runLedAnimation){
              wheelVal = Wheel(((i * 256 / strip.numPixels()) + j) & 255);
      
              redVal = red(wheelVal) * float(fadeVal/fadeMax);
              greenVal = green(wheelVal) * float(fadeVal/fadeMax);
              blueVal = blue(wheelVal) * float(fadeVal/fadeMax);
      
              strip.setPixelColor( i, strip.Color( redVal, greenVal, blueVal ) );
            }
          }
    
          //First loop, fade in!
          if(k == 0 && fadeVal < fadeMax-1) {
              fadeVal++;
          }
    
          //Last loop, fade out!
          else if(k == rainbowLoops - 1 && j > 255 - fadeMax ){
              fadeVal--;
          }
    
            strip.show();
            delay(wait);
        }
      }
    }
  }



  delay(500);


  for(int k = 0 ; k < whiteLoops ; k ++){
  if(runLedAnimation){
    for(int j = 0; j < 256 ; j++){
    if(runLedAnimation){
        for(uint16_t i=0; i < strip.numPixels(); i++) {
            strip.setPixelColor(i, strip.Color(0,0,0, neopix_gamma[j] ) );
          }
          strip.show();
        }
  }

        delay(2000);
    for(int j = 255; j >= 0 ; j--){
    if(runLedAnimation){
        for(uint16_t i=0; i < strip.numPixels(); i++) {
            strip.setPixelColor(i, strip.Color(0,0,0, neopix_gamma[j] ) );
          }
          strip.show();
        }
    }
  }
  }

  delay(500);


}

void whiteOverRainbow(uint8_t wait, uint8_t whiteSpeed, uint8_t whiteLength ) {
  
  if(whiteLength >= strip.numPixels()) whiteLength = strip.numPixels() - 1;

  int head = whiteLength - 1;
  int tail = 0;

  int loops = 3;
  int loopNum = 0;

  static unsigned long lastTime = 0;


  while(runLedAnimation){
    for(int j=0; j<256; j++) {
      if (runLedAnimation) {
        for(uint16_t i=0; i<strip.numPixels(); i++) {
          if((i >= tail && i <= head) || (tail > head && i >= tail) || (tail > head && i <= head) ){
            strip.setPixelColor(i, strip.Color(0,0,0, 255 ) );
          }
          else{
            strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
          }
          
        }
  
        if(millis() - lastTime > whiteSpeed) {
          head++;
          tail++;
          if(head == strip.numPixels()){
            loopNum++;
          }
          lastTime = millis();
        }
  
        if(loopNum == loops) return;
      
        head%=strip.numPixels();
        tail%=strip.numPixels();
          strip.show();
          delay(wait);
      }
    }
  }
  
}
void fullWhite() {
  
    for(uint16_t i=0; i<strip.numPixels(); i++) {
        strip.setPixelColor(i, strip.Color(0,0,0, 255 ) );
    }
      strip.show();
}


// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256 * 5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3,0);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3,0);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0,0);
}

uint8_t red(uint32_t c) {
  return (c >> 16);
}
uint8_t green(uint32_t c) {
  return (c >> 8);
}
uint8_t blue(uint32_t c) {
  return (c);
}
