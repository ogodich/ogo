

void setup(void){
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  wifiPrint();

  otaInit();

  //pinMode(leds[0], OUTPUT);
  //pinMode(leds[1], OUTPUT);
  //pinMode(leds[2], OUTPUT);

  //Первая лента
  strip.begin(); // This initializes the NeoPixel library.
  strip.setBrightness(255); // set brightness
  strip.show(); // Initialize all pixels to 'off'

  //Вторая лента
  strip2.begin(); // This initializes the NeoPixel library.
  strip2.setBrightness(255); // set brightness
  strip2.show(); // Initialize all pixels to 'off'

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  //client.subscribe("radio");
  //client.subscribe("motivation_green");
  //client.subscribe("motivation_red");
  //client.subscribe("motivation_blue");

  irrecv.enableIRIn();  // Start the receiver
  while (!Serial)  // Wait for the serial connection to be establised.
    delay(50);

  attachInterrupt(INTERRUPT_PIN, myInterrupt, RISING);

// fastLedStart
//  delay(3000); // sanity delay
//  FastLED.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
//  FastLED.setBrightness( BRIGHTNESS );
// fastLedStop


}
