

/* Node MCU D1 mini
  Pin  Function  ESP-8266 Pin
  TX  TXD TXD
  RX  RXD RXD
  A0  Analog input, max 3.3V input  A0
  D0  IO  GPIO16
  D1  IO, SCL GPIO5                         - работает выходом для NodeMCU 1.0 (ESP-12E Module), не работает для Wemos D1 R1
  D2  IO, SDA GPIO4
  D3  IO, 10k Pull-up GPIO0
  D4  IO, 10k Pull-up, BUILTIN_LED  GPIO2
  D5  IO, SCK GPIO14
  D6  IO, MISO  GPIO12
  D7  IO, MOSI  GPIO13
  D8  IO, 10k Pull-down, SS GPIO15 (Нельзя делать выходом, не стартует)
  G Ground  GND
  5V  5V  -
  3V3 3.3V  3.3V
  RST Reset RST


  some_string.toInt()


Временная шпаргалка для определения точек включения избранных светодиодов
11
35
59
84
107
125
146
169
198
  
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <Adafruit_NeoPixel.h>
/*#ifdef __AVR__
  #include <avr/power.h>
  #endif
*/

#include <ArduinoSort.h>

#include <PubSubClient.h>

#include <stdlib.h>

#include <ArduinoJson.h>

#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRutils.h>

// fastLedStart
//#include <FastLED.h>


//*************************************************************************************************************

#define PIN            D1
#define PIN2           D7
#define RECV_PIN       D2
#define INTERRUPT_PIN  D5
#define BOARD_NAME "LED_STRIP"

//fastLed
//#define LED_PIN     5
//#define COLOR_ORDER GRB
//#define CHIPSET     WS2811
//#define NUM_LEDS    217
//#define BRIGHTNESS  200
//#define FRAMES_PER_SECOND 60
//fastLed

//*************************************************************************************************************
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

Adafruit_NeoPixel strip = Adafruit_NeoPixel(217, PIN, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(70, PIN2, NEO_GRBW + NEO_KHZ800);
boolean switchedStrip = false;
boolean switchedStrip2 = false;

int mLeds[217];
int countLeds[10];

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;

boolean startBoot = false;
double startTime = 0;

char msg[50];
int value = 0;

long lastBrightMsg = 0;
int currentBrightness = 0;
int prevBrightness = 0;

int motivationBrightness = 0;
int secondStripBrigth = 110;

String debug;
char deb[150];

boolean stripConfParts[7];
int     stripConfR [6][6];
int     stripConfG [6][6];
int     stripConfB [6][6];
int     stripConfW [6][6];
int     stripConfBr[6][6];
int currPreset = 1;

boolean runTimer = false;
long timerRefresh = 0;
long timerStop = 0;
int timerCurLed = 0;
int timerPrevLed = -1;
int timerPassTime = 0;
int timerInterval = 0;

IRrecv irrecv(RECV_PIN);
decode_results results;


String irrPrevVal = "";
String irrCurrVal = "";
int irrCount = 0;
long irrLastTime = 0;
boolean irrRecieved = false;
uint64_t iCurrVal;
uint64_t iPrevVal;

int irrMode = 0;
long irrModeTimer = 0;

long irrModeAnimationTimer = 0;
boolean irrAnimationTrig = false;

boolean runLedAnimation = false;

byte neopix_gamma[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };


// fastLed
//bool gReverseDirection = false;

//CRGB leds[NUM_LEDS];

//boolean fire2012Start  = false;
// fastLedStop
