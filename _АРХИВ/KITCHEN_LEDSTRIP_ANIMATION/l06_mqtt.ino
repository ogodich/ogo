void mqtt_loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

 /* long now = millis();
  if (now - lastMsg > 3000) {
    lastMsg = now;
    snprintf (msg, 75, "%ld, %d", currentBrightness, motivationBrightness);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("getBright/kitchen", msg);
  }*/
}

//*********************************************************************************************************************************
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic_2", "reconnect");
      // ... and resubscribe

      client.subscribe("boot/ledstrip");
      client.subscribe("media_volume");
      client.subscribe("strip/conf/#");
      client.subscribe("strip/presets/kitchen");
      client.subscribe("strip/kitchen/setPreset");
      client.subscribe("light/kitchen/led_strip");
      client.subscribe("light/kitchen/led_strip2_bright");
      client.subscribe("light/kitchen/led_strip2");
      client.subscribe("strip/kitchen/timer");
      client.subscribe("strip/kitchen/specificLedOn");
      

    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  String top = String(topic);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  String call = "";
  char charCall[length];
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call += (char)payload[i];
    charCall[i] = (char)payload[i];
  }
  charCall[length]=0;
  Serial.println();


//*****************
  if (strcmp(topic, "boot/ledstrip") == 0) {
    startTime = millis() + 10000;
    startBoot = true;
  }
//*****************
  else if (strcmp(topic, "motivation/red") == 0) {
    new_motivation(2, call.toInt());
    countLeds[2] = call.toInt();
  }
  else if (strcmp(topic, "motivation/blue") == 0) {
    new_motivation(4, call.toInt());
    countLeds[4] = call.toInt();
  }
//*****************
  else if ((strcmp(topic, "motivation/add") == 0 || strcmp(topic, "motivation/remove") == 0)) {
    if      (strcmp(topic, "motivation/add") == 0)                                   countLeds[call.toInt()]++;
    else if (strcmp(topic, "motivation/remove") == 0 && countLeds[call.toInt()] > 0) countLeds[call.toInt()]--;
    new_motivation(call.toInt(), countLeds[call.toInt()]);

    snprintf (msg, 75, "%ld", countLeds[call.toInt()]);
    if (call.toInt() == 4) client.publish("motivation/blue", msg, true);
    else if (call.toInt() == 2) client.publish("motivation/red", msg, true);
    else if (call.toInt() == 3) client.publish("motivation/green", msg, true);
  }
//*****************
  else if (strcmp(topic, "strip/hex") == 0) {
    String rr = "0x";
    rr += (char)payload[1];
    rr += (char)payload[2];
    char r[50];
    rr.toCharArray(r, 50);

    String gg = "0x";
    gg += (char)payload[3];
    gg += (char)payload[4];
    char g[50];
    gg.toCharArray(g, 50);

    String bb = "0x";
    bb += (char)payload[5];
    bb += (char)payload[6];
    char b[50];
    bb.toCharArray(b, 50);

    PowerStrip(strtoul(r, NULL, 16),
                 strtoul(g, NULL, 16),
                 strtoul(b, NULL, 16),
                 0x0);
    /*String ipaddress = WiFi.localIP().toString();
      char ipchar[ipaddress.length()+1];
      ipaddress.toCharArray(ipchar,ipaddress.length()+1);
      client.publish("deviceIP", ipchar);*/
  }
//*****************
  else if (strcmp(topic, "strip/decimal") == 0) {
    uint32_t sourceInt = call.toInt();
    int r = (sourceInt / 256) / 256;
    int g = (sourceInt / 256) % 256;
    int b = sourceInt % 256;

    PowerStrip(r, g, b, 0);
  }
//*****************
  else if (strcmp(topic, "light/kitchen/led_strip") == 0) {
    if (call == "2") {
      switchedStrip = !switchedStrip;
      if (switchedStrip) client.publish("strip/kitchen/setPreset", "1", true);
      else               client.publish("strip/kitchen/setPreset", "0", true);
    }
    else client.publish("strip/kitchen/setPreset", charCall, true);
  }
//*****************ВКЛЮЧАЕМ ВТОРУЮ ЛЕНТУ
  else if (strcmp(topic, "light/kitchen/led_strip2") == 0) {
    if      (call == "1") {
      secondStripBrigth = 100;
      PowerStrip2(190 * secondStripBrigth / 100, 45 * secondStripBrigth / 100, 45 * secondStripBrigth / 100, 120 * secondStripBrigth / 100);
      switchedStrip2 = true;
    }
    else if (call == "0") {
      PowerStrip2(0, 0, 0, 0);
      switchedStrip2 = false;
    }
    else if (call == "-1") {
      switchedStrip2 = !switchedStrip2;
      if (switchedStrip2) client.publish("light/kitchen/led_strip2", "1", true);
      else                client.publish("light/kitchen/led_strip2", "0", true);
    }
  }
//*****************ЯРКОСТЬ ВТОРОЙ ЛЕНТЫ
  else if (strcmp(topic, "light/kitchen/led_strip2_bright") == 0) {
    secondStripBrigth = call.toInt();
    PowerStrip2(190 * secondStripBrigth / 100, 45 * secondStripBrigth / 100, 45 * secondStripBrigth / 100, 120 * secondStripBrigth / 100);
    strip2.show();
  }


//*****************  //*****************  //*****************  //*****************  //*****************  //*****************  //*****************  //*****************  
//*****************
  //Выбрать пресет
  else if (strcmp(topic, "strip/kitchen/setPreset") == 0) {
    currPreset = call.toInt();

    //Если выбран расширенный профиль
    if (currPreset > 5) {
      Serial.print("currPreset=");
      Serial.println(currPreset);
      switch(currPreset){
        case 6:
          //научиться делать цикл из фиксированных значений
          strip.setPixelColor(11, strip.Color(255, 50, 0, 150));
          strip.setPixelColor(35, strip.Color(255, 50, 0, 150));
          strip.setPixelColor(59, strip.Color(255, 50, 0, 150));
          strip.setPixelColor(84, strip.Color(255, 50, 0, 150));
          strip.setPixelColor(107, strip.Color(255, 50, 0, 150));
          strip.setPixelColor(125, strip.Color(255, 50, 0, 150));
          strip.setPixelColor(146, strip.Color(255, 50, 0, 150));
          strip.setPixelColor(169, strip.Color(255, 50, 0, 150));
          strip.setPixelColor(198, strip.Color(255, 50, 0, 150));
          strip.show();
        break;
        case 10:
          runLedAnimation = true;
          whiteOverRainbow(20,75,5);
        break;
        case 11:
          runLedAnimation = true;
          //rainbowFade2White(3,3,1);
          stripAnimation_1();
        break;
        case 12:
        break;

      }
    }
    //Если выбран обычный профиль
    else if (currPreset > 0) {
      //Включаем ленту
      presetStrip();
      //Глобальная отметка включения:
      switchedStrip = true;
    }
    else {
      PowerStrip(0, 0, 0, 0);
      //Глобальная отметка включения:
      switchedStrip = false;
    }
  }
//*****************
  //При включении/перезагрузке получаем настроечный jSon и заполняем настроечные массивы
  else if (currPreset >0 && currPreset <=5 && strcmp(topic, "strip/presets/kitchen") == 0) {
    //Serial.println(charCall);

    //Создаем jSon объект
    DynamicJsonBuffer jsonBuffer;
    JsonObject& jObj = jsonBuffer.parseObject(charCall);
    //jObj.prettyPrintTo(Serial);

    //Сохраняю настройки
    for(int y=1; y<=5; y++){
      for(int i=0; i<6; i++){
        stripConfR [y][i]=jObj[String(y)]["r"][i];
        stripConfG [y][i]=jObj[String(y)]["g"][i];
        stripConfB [y][i]=jObj[String(y)]["b"][i];
        stripConfW [y][i]=jObj[String(y)]["w"][i];
        stripConfBr[y][i]=jObj[String(y)]["br"][i];
      }
    }
    
    //После получения и обработки jSon отписываюсь от этой очереди, чтобы не читать сообщения, которые сам же и отправляю
    //Таким образом получаю их только при запуске контроллера или реконнекте к MQTT
    client.unsubscribe("strip/presets/kitchen");

    //Освобождаем память буфера
    jsonBuffer.clear();
  }
//*****************
  //обработчик выбора частей для настройки
  else if (top.indexOf("strip/conf/parts") != -1 ) {
    int part = (top.substring(top.indexOf("parts/") + 6)).toInt();
    stripConfParts[part] = call.toInt();
  }
//*****************
  //обработчик RGB
  else if (currPreset >0 && currPreset <=5 && strcmp(topic, "strip/conf/rgb") == 0) {
    uint32_t sourceInt = call.toInt();
    int r = (sourceInt / 256) / 256;
    int g = (sourceInt / 256) % 256;
    int b = sourceInt % 256;
    for (int i=1; i<=6; i++) {
      if (stripConfParts[i]) {
        stripConfR[currPreset][i-1] = r;
        stripConfG[currPreset][i-1] = g;
        stripConfB[currPreset][i-1] = b;
      }
    }
    //Отправляем JSon обратно в очередь
    savePresets();
    //Включаем ленту
    presetStrip();
  }
//*****************
  //обработчик белого цвета
  else if (currPreset >0 && currPreset <=5 && strcmp(topic, "strip/conf/white") == 0) {
    for (int i=1; i<=6; i++) {
      if (stripConfParts[i]) stripConfW[currPreset][i-1] = call.toInt();
    }
    //Отправляем JSon обратно в очередь
    savePresets();
    //Включаем ленту
    presetStrip();
  }
//*****************
  //обработчик яркости
  else if (currPreset >0 && currPreset <=5 && strcmp(topic, "strip/conf/bright") == 0) {
    for (int i=1; i<=6; i++) {
      if (stripConfParts[i]) stripConfBr[currPreset][i-1] = call.toInt();
    }
    //Отправляем JSon обратно в очередь
    savePresets();
    //Включаем ленту
    presetStrip();
  }  
//*****************
  //Ластик
  else if (currPreset >0 && currPreset <=5 && strcmp(topic, "strip/conf/clearCurrPreset") == 0) {
    for (int i=1; i<=6; i++) {
      if (stripConfParts[i]) {
        stripConfR[currPreset][i-1] = 0;
        stripConfG[currPreset][i-1] = 0;
        stripConfB[currPreset][i-1] = 0;
        stripConfW[currPreset][i-1] = 0;
        stripConfBr[currPreset][i-1] = 100;
      }
    }

    client.unsubscribe("strip/conf/#");
    client.publish("strip/conf/parts/1", "1", true);
    client.publish("strip/conf/parts/2", "1", true);
    client.publish("strip/conf/parts/3", "1", true);
    client.publish("strip/conf/parts/4", "1", true);
    client.publish("strip/conf/parts/5", "1", true);
    client.publish("strip/conf/parts/6", "1", true);     
    client.publish("strip/conf/rgb", "0", false);
    client.publish("strip/conf/white", "0", false);
    client.publish("strip/conf/bright", "100", false);
    client.subscribe("strip/conf/#");

    //Отправляем JSon обратно в очередь
    savePresets();
    //Включаем ленту
    presetStrip();
  }
//*****************

//***************** ТАЙМЕР
  else if (strcmp(topic, "strip/kitchen/timer") == 0) {    
    my_timer(call.toInt());
  }
//***************** Адресное зажигание светодиода на ленте
  else if (strcmp(topic, "strip/kitchen/specificLedOn") == 0) {   
    DynamicJsonBuffer specificLedOnBuf;
    JsonObject& specificLedOnObj = specificLedOnBuf.parseObject(charCall);
    int specLed  = specificLedOnObj["led"];
    int specColR = specificLedOnObj["red"];
    int specColG = specificLedOnObj["green"];
    int specColB = specificLedOnObj["blue"];
    int specColW = specificLedOnObj["white"];
    strip.setPixelColor(specLed, strip.Color(specColR, specColG, specColB, specColW));
    strip.show();
  }
 
}   //*********************************************************************************************************************************
void savePresets(){
  char chrOutPresets[1000];
  memset(&chrOutPresets, 0, sizeof(chrOutPresets));
 
  StaticJsonBuffer<500> jsonOutBuffer;
  
  for (int y=1; y<=5; y++){      
    JsonObject& jOut = jsonOutBuffer.createObject();

    //Создаем jМассивы
    JsonArray& jRed     = jOut.createNestedArray("r");
    JsonArray& jGreen   = jOut.createNestedArray("g");
    JsonArray& jBlue    = jOut.createNestedArray("b");
    JsonArray& jWhite   = jOut.createNestedArray("w");
    JsonArray& jBright  = jOut.createNestedArray("br");

    //Наполняем jМассивы
    for(int i=0; i<6; i++){
      jRed.add    (stripConfR[y][i]);
      jGreen.add  (stripConfG[y][i]);
      jBlue.add   (stripConfB[y][i]);
      jWhite.add  (stripConfW[y][i]);
      jBright.add (stripConfBr[y][i]);
    }

    char chrOnePreset[200];
    jOut.printTo(chrOnePreset);

    //Очистка jБуфера
    jsonOutBuffer.clear();
    
    sprintf(chrOutPresets, "%s%s%d:%s%s",
                                          chrOutPresets,
                                          (y==1?"{":""),
                                          y,
                                          chrOnePreset,
                                          (y!=5?",":"}"
                                          ));
    //Очистка текстового буфера внутри цикла
    memset(&chrOnePreset, 0, sizeof(chrOnePreset));
  } //конец внешнего цикла

  Serial.println();
  Serial.println(chrOutPresets);

  client.publish("strip/presets/kitchen", chrOutPresets, true);
  //Очистка текстового буфера снаружи цикла
  memset(&chrOutPresets, 0, sizeof(chrOutPresets));
}

//Непосредственное зажигание ленты по частям
void presetStrip() {
/*
1 - 23
2 - 49
3 - 26
4 - 60
5 - 23
6 - 36

*/
  
  for (int currPart=0; currPart<6; currPart++) {
    int start = 0;
    int stp = 0;
    switch (currPart) {
      case 5:
        start = 0;
        stp = 23;
        break;
      case 4:
        start = 23;
        stp = 72;
        break;
      case 3:
        start = 72;
        stp = 98;
        break;
      case 2:
        start = 98;
        stp = 158;
        break;
      case 1:
        start = 158;
        stp = 181;
        break;
      case 0:
        start = 181;
        stp = 217;
        break;        
    }
    for (int i=start; i<stp; i++) {
      int r = stripConfR[currPreset][currPart];
      int g = stripConfG[currPreset][currPart];
      int b = stripConfB[currPreset][currPart];
      int w = stripConfW[currPreset][currPart];
      strip.setPixelColor(i, strip.Color(r, g, b, w));
    }
  
  } //конец основного цикла
  strip.show();
}

//*******
