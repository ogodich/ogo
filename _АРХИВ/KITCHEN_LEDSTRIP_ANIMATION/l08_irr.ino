void myIrrGet() {
  //-----------------------------------
  //пришел сигнал
  if (irrecv.decode(&results)) {
    irrCurrVal = uint64_to_string(results.value);

    //Если пришел новый код обнуляем счетчик
    if (irrPrevVal != irrCurrVal) irrCount=1;
    //Если уже известный, делаем ++
    else irrCount++;

    irrPrevVal = irrCurrVal;
    irrLastTime = millis();
    irrRecieved = true;
    irrecv.resume();
  }

  //после сигнала------------------------
  if(irrRecieved && millis()-irrLastTime > 200){
    //начало обработчика сигнала
    //Serial.print("val=");
    //Serial.print(irrCurrVal);
    //Serial.print(",irrCount=");
    //Serial.println(irrCount);

    char myStrToCharArray[50];
    irrCurrVal.toCharArray(myStrToCharArray, 50);
    snprintf (msg, 75, "irrCurrVal=%s, irrCount=%d",  myStrToCharArray, irrCount);
    client.publish("strip/kitchen/irrLog", msg);

    if (irrMode == 1){
      if     (irrCurrVal == "3772784863") client.publish("radio", "2");
      else if(irrCurrVal == "3772817503") client.publish("volume_change", "-1");
      else if(irrCurrVal == "3772801183") client.publish("volume_change", "1");
      else if(irrCurrVal == "3772780783") client.publish("light/kitchen/led_strip", "2");
      else if(irrCurrVal == "3772813423") client.publish("light/kitchen/led_strip2", "-1");
      else if(irrCurrVal == "3772797103") client.publish("light/toilet/1", "2");
      else if(irrCurrVal == "3772788943") client.publish("light/bath/1", "-1");
      else if(irrCurrVal == "3772821583") client.publish("light/bath/2", "-1");
  
      else if(irrCurrVal == "3772790473") irrSetMode(1);
      else if(irrCurrVal == "3772786903") irrSetMode(2);
      irrSetMode(1);  //не выходим из текущего режима ещё какое-то время, после нажатия любой кнопки
    }

    //Режим установки яркости верхней ленты
    if (irrMode == 5){
      if     (irrCurrVal == "3772784863") client.publish("light/kitchen/led_strip2_bright", "1");
      else if(irrCurrVal == "3772817503") client.publish("light/kitchen/led_strip2_bright", "5");
      else if(irrCurrVal == "3772801183") client.publish("light/kitchen/led_strip2_bright", "10");
      else if(irrCurrVal == "3772780783") client.publish("light/kitchen/led_strip2_bright", "20");
        
      else if(irrCurrVal == "3772790473") irrSetMode(1);
      else if(irrCurrVal == "3772786903") irrSetMode(2);
      irrSetMode(5);  //не выходим из текущего режима ещё какое-то время, после нажатия любой кнопки
    }

    //Режим выбора профиля основной лелнты
    if (irrMode == 6){
      if     (irrCurrVal == "3772784863") client.publish("strip/kitchen/setPreset", "1", true);
      else if(irrCurrVal == "3772817503") client.publish("strip/kitchen/setPreset", "2", true);
      else if(irrCurrVal == "3772801183") client.publish("strip/kitchen/setPreset", "3", true);
      else if(irrCurrVal == "3772780783") client.publish("strip/kitchen/setPreset", "4", true);
      else if(irrCurrVal == "3772813423") client.publish("strip/kitchen/setPreset", "5", true);
      
        
      else if(irrCurrVal == "3772790473") irrSetMode(1);
      else if(irrCurrVal == "3772786903") irrSetMode(2);
      irrSetMode(6);  //не выходим из текущего режима ещё какое-то время, после нажатия любой кнопки
    }

    //------------------------------------------------------------------------------------------------
    else if (irrMode == 0){  
      //1
      if     (irrCurrVal == "3772784863") client.publish("light/kitchen/main", "-1");
      //2
      else if(irrCurrVal == "3772817503") client.publish("light/kitchen/corner", "2");
      //3
      else if(irrCurrVal == "3772801183") client.publish("light/kitchen/coohoo", "2");
      
      //4
      //для долгого нажатия включаем режим выбора профиля основной ленты
      else if(irrCurrVal == "3772780783" && irrCount > 5) irrSetMode(6);
      //короткое нажатие
      else if(irrCurrVal == "3772780783") client.publish("light/kitchen/led_strip", "2");

      //5
      //для долгого нажатия включаем режим яркость верхней ленты
      else if(irrCurrVal == "3772813423" && irrCount > 5) irrSetMode(5);
      //Короткое нажатие
      else if(irrCurrVal == "3772813423") client.publish("light/kitchen/led_strip2", "-1");      

      //6
      else if(irrCurrVal == "3772797103") client.publish("light/toilet/1", "2");

      //7
      else if(irrCurrVal == "3772788943" && irrCount > 5) client.publish("light/bath/2", "-1");
      else if(irrCurrVal == "3772788943") client.publish("light/bath/1", "-1");

      //8
      else if(irrCurrVal == "3772821583") client.publish("light/hall_strip", "2");

      //9
      else if(irrCurrVal == "3772805263") client.publish("remote_socket/1", "-1");

      //красная кнопка
      else if(irrCurrVal == "3772790473") irrSetMode(1);
      //зелёная кнопка
      else if(irrCurrVal == "3772786903") irrSetMode(2);
    }
    //------------------------------------------------------------------------------------------------

    //конец обработчика сигнала----------
    irrCount = 0;
    irrRecieved = false; 
  }
}
//--------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------------------------------------
void irrModeOff(){
  if (millis() > irrModeTimer){
    irrMode = 0;
    presetStrip();                                                //Включаем текущий пресет ленты
  }
}

//--------------------------------------------------------------------------------------------------------------------------------------------
void irrSetMode (int modeNum){
  irrMode = modeNum;
  irrModeTimer = millis() + 5000;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
void run_irr_timer() {
  long now = millis();
  if (now - irrModeAnimationTimer > 1000) {
    irrModeAnimationTimer = now;
    
    irrAnimationTrig = !irrAnimationTrig;
    if (irrAnimationTrig) {
      switch (irrMode){

        case 1:
        strip.setPixelColor(216,  strip.Color(255,0,0,0));
        strip.setPixelColor(0,    strip.Color(255,0,0,0));
        break;

        case 2:
        strip.setPixelColor(216,  strip.Color(0,255,0,0));
        strip.setPixelColor(0,    strip.Color(0,255,0,0));
        break;

        case 5:
        strip.setPixelColor(216,  strip.Color(100,100,100,100));
        strip.setPixelColor(0,    strip.Color(100,100,100,100));
        break;
      }
    }
    else {
      strip.setPixelColor(216, strip.Color(0,0,0,0));
      strip.setPixelColor(0,   strip.Color(0,0,0,0));
    }
    strip.show();
    
  }
}
