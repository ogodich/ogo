void setup()  {
  pinMode(3,INPUT);
  pinMode(5,OUTPUT);
  pinMode(13,OUTPUT);
}

int myMap=0;
void loop(){
  playNote("C",1, 250);
  playNote("C",1, 250);
  playNote("C",1, 250);
  playNote("E",1, 250);
  playNote("G",1, 250);
  playNote("C",1, 750);
 
  playNote("C",1, 250);
  playNote("E",1, 250);
  playNote("C",2, 500);
  playNote("H",2, 500);
  playNote("G",1, 250);
  playNote("E",1, 750);
 
  playNote("C",2, 250);
  playNote("H",2, 250);
  playNote("D",2, 250);
  playNote("C",2, 250);
  playNote("H",2, 250);
  playNote("C",2, 750);
 
  playNote("H",2, 250);
  playNote("A",2, 250);
  playNote("A",2, 500);
  playNote("G#",1, 500);
  playNote("H",2, 250);
  playNote("F",1, 500);
  playNote("E",1, 750);
  
  //----
  playNote("A",1, 250);
  playNote("H",1, 250);
  playNote("C",1, 250);
  playNote("E",1, 250);
  playNote("G",1, 250);
  playNote("D",2, 750);

  playNote("E",1, 250);
  playNote("G",1, 250);
  playNote("D",2, 500);
  playNote("E",1, 250);
  playNote("D",2, 250);
  playNote("C",2, 750);
  
  playNote("C",2, 250);
  playNote("C",2, 250);
  playNote("C",2, 250);
  playNote("H",2, 250);
  playNote("A",2, 250);
  playNote("A",2, 500);
  playNote("G",1, 750);

  playNote("G",1, 250);
  playNote("G",1, 250);
  playNote("A",2, 500);
  playNote("G",1, 500);
  playNote("F",1, 250);
  playNote("F",1, 500);
  playNote("E",1, 750);

  //------
  playNote("A",1, 250);
  playNote("H",1, 250);
  playNote("C",1, 250);
  playNote("E",1, 250);
  playNote("G",1, 250);
  playNote("G",2, 750);
  
  playNote("F",2, 250);
  playNote("G",2, 250);
  playNote("F",2, 500);
  playNote("E",2, 500);
  playNote("D",1, 250);
  playNote("C",2, 750);

  playNote("C",2, 250);
  playNote("C",2, 250);
  playNote("C",2, 250);
  playNote("H",2, 250);
  playNote("A",2, 250);
  playNote("A",2, 500);
  playNote("G",1, 750);

  playNote("G",1, 250);
  playNote("G",1, 250);
  playNote("A",2, 500);
  playNote("H",2, 500);
  playNote("D",2, 250);
  playNote("D",2, 500);
  playNote("C",2, 750);

  delay(3000);
}

//-----------------------------------------------------
void playFreq(float val, int time){
  myMap=map(val, 240.0, 269.0, 1000.0, 1121.0);
  tone(13, myMap, time);
  delay(time);
}
//-----------------------------------------------------
void playNote(String Note, int octav, int time){
  float myFreq = 0.0;
  if      (Note=="C")     {myFreq = 240.0 * octav;}
  else if (Note=="D")     {myFreq = 269.0 * octav;}
  else if (Note=="E")     {myFreq = 302.3 * octav;}
  else if (Note=="F")     {myFreq = 320.1 * octav;}
  else if (Note=="G")     {myFreq = 359.4 * octav;}
  else if (Note=="G#")    {myFreq = 380.3 * octav;}
  else if (Note=="A")     {myFreq = 201.4 * octav;}
  else if (Note=="H")     {myFreq = 226.2 * octav;}
  
if (myFreq > 0) {
    playFreq(myFreq, time);
  }
}

