void mqtt_loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

 /* long now = millis();
  if (now - lastMsg > 3000) {
    lastMsg = now;
    snprintf (msg, 75, "%ld, %d", currentBrightness, motivationBrightness);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("getBright/kitchen", msg);
  }*/
}

//*********************************************************************************************************************************
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic_2", "reconnect");
      // ... and resubscribe

      client.subscribe("boot/ledstrip");
      client.subscribe("motivation/+");
      client.subscribe("strip/conf/#");
      client.subscribe("strip/presets/kitchen");
      client.subscribe("light/kitchen/led_strip");

    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  String top = String(topic);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  char charCall[length];
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call += (char)payload[i];
    charCall[i] = (char)payload[i];
  }
  charCall[length]=0;
  Serial.println();

//*********************************************************************************************************************************
  if (strcmp(topic, "strip/presets/kitchen") == 0) {
    //Serial.println(charCall);

    //Создаем jSon объект
    DynamicJsonBuffer jsonBuffer;
    JsonObject& jObj = jsonBuffer.parseObject(charCall);
    //jObj.prettyPrintTo(Serial);

    //Сохраняю настройки
    for(int y=1; y<=5; y++){
      for(int i=0; i<6; i++){
        stripConfR [y][i]=jObj[String(y)]["r"][i];
        stripConfG [y][i]=jObj[String(y)]["g"][i];
        stripConfB [y][i]=jObj[String(y)]["b"][i];
        stripConfW [y][i]=jObj[String(y)]["w"][i];
        stripConfBr[y][i]=jObj[String(y)]["br"][i];
      }
    }

    Serial.println();
    for(int i=0; i<6; i++){
      Serial.print("w");
      Serial.print(i);
      Serial.print("=");
      Serial.println(stripConfW[1][i]);
    }
    
    //После получения и обработки jSon отписываюсь от этой очереди, чтобы не читать сообщения, которые сам же и отправляю
    //Таким образом получаю их только при запуске контроллера или реконнекте к MQTT
    client.unsubscribe("strip/presets/kitchen");

    //Освобождаем память буфера
    jsonBuffer.clear();
  }

//*******************************************************
  /*else if (strcmp(topic, "strip/conf/publishPresetes") == 0) {
    char chrPresets[] = "{1:{\"r\":[255,255,255,255,255,255],\"g\":[255,255,255,255,255,255],\"b\":[255,255,255,255,255,255],\"w\":[255,255,255,255,255,255],\"br\":[100,100,100,100,100,100]},2:{\"r\":[255,255,255,255,255,255],\"g\":[255,255,255,255,255,255],\"b\":[255,255,255,255,255,255],\"w\":[255,255,255,255,255,255],\"br\":[100,100,100,100,100,100]},3:{\"r\":[255,255,255,255,255,255],\"g\":[255,255,255,255,255,255],\"b\":[255,255,255,255,255,255],\"w\":[255,255,255,255,255,255],\"br\":[100,100,100,100,100,100]},4:{\"r\":[255,255,255,255,255,255],\"g\":[255,255,255,255,255,255],\"b\":[255,255,255,255,255,255],\"w\":[255,255,255,255,255,255],\"br\":[100,100,100,100,100,100]},5:{\"r\":[255,255,255,255,255,255],\"g\":[255,255,255,255,255,255],\"b\":[255,255,255,255,255,255],\"w\":[255,255,255,255,255,255],\"br\":[100,100,100,100,100,100]}}";
    client.publish("strip/presets/kitchen", chrPresets, true);
  }*/

  else if (strcmp(topic, "strip/conf/publishPresetes") == 0) { 
    savePresets();
  }

//************
}



//*********************************************************************************************************************
void savePresets(){
  char chrOutPresets[1000];
  memset(&chrOutPresets, 0, sizeof(chrOutPresets));
 
  StaticJsonBuffer<500> jsonOutBuffer;
  
  for (int y=1; y<=5; y++){      
    JsonObject& jOut = jsonOutBuffer.createObject();

    //Создаем jМассивы
    JsonArray& jRed     = jOut.createNestedArray("r");
    JsonArray& jGreen   = jOut.createNestedArray("g");
    JsonArray& jBlue    = jOut.createNestedArray("b");
    JsonArray& jWhite   = jOut.createNestedArray("w");
    JsonArray& jBright  = jOut.createNestedArray("br");

    //Наполняем jМассивы
    for(int i=0; i<6; i++){
      jRed.add    (stripConfR[y][i]);
      jGreen.add  (stripConfG[y][i]);
      jBlue.add   (stripConfB[y][i]);
      jWhite.add  (stripConfW[y][i]);
      jBright.add (stripConfBr[y][i]);
    }

    //Serial.println(y);
    //jOut.printTo(Serial);
    //Serial.println();
    char chrOnePreset[200];
    jOut.printTo(chrOnePreset);

    //Очистка jБуфера
    jsonOutBuffer.clear();
    
    sprintf(chrOutPresets, "%s%s%d:%s%s",
                                          chrOutPresets,
                                          (y==1?"{":""),
                                          y,
                                          chrOnePreset,
                                          (y!=5?",":"}"
                                          ));
    //Очистка текстового буфера внутри цикла
    memset(&chrOnePreset, 0, sizeof(chrOnePreset));
  } //конец внешнего цикла

  //Serial.println();
  //Serial.println(chrOutPresets);

  client.publish("strip/presets/kitchen", chrOutPresets, true);
  //Очистка текстового буфера снаружи цикла
  memset(&chrOutPresets, 0, sizeof(chrOutPresets));
}


