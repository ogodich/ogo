#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

const char* ssid = "ScorpGOS";
const char* password = "9031124945";

int ledPin = 0;
WiFiServer server(80);

boolean startBoot = false;
double startTime = 0;

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Start the server
  server.begin();
  Serial.println("Server started");
 
  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");

}

/*****************************************************************************************************************************************/
void loop() {
if (startBoot) my_booting();
else web_server();
}
/*****************************************************************************************************************************************/
void my_booting() {
  if (startTime > millis()) {
    digitalWrite(ledPin, true);
    ArduinoOTA.handle();
  }
  else {
    digitalWrite(ledPin, false);
    startBoot = false;
  }
}
/*****************************************************************************************************************************************/
void web_server() {
    // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  Serial.println("new client");
/*
  while(!client.available()){
    delay(1);
    timeout++;
    if (timeout > 500)
    {
      client.flush();
      client.stop();
      break;
    }
  }
 */
  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();
 
  // Match the request
 
  int value = LOW;
  if (request.indexOf("/LED=ON") != -1)  {
    digitalWrite(ledPin, HIGH);
    value = HIGH;
  }
  if (request.indexOf("/LED=OFF") != -1)  {
    digitalWrite(ledPin, LOW);
    value = LOW;
  }
  if (request.indexOf("/LED=CHANGE") != -1)  {
    digitalWrite(ledPin, !digitalRead(ledPin));
    value = digitalRead(ledPin);
  }
  if (request.indexOf("/BOOTING") != -1)  {    
    startTime = millis() + 10000;
    startBoot = true;
  }
 
// Set ledPin according to the request
//digitalWrite(ledPin, value);
 
  // Return the response
  /*client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); //  do not forget this one
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("test");
  client.println("</html>");
*/


String resp = "HTTP/1.1 200 OK\n";
resp = resp + "Content-Type: text/html\n";
resp = resp + "\n";
resp = resp + "<!DOCTYPE HTML>\n";
resp = resp + "<html>\n";
resp = resp + millis();
resp = resp + "\n";
resp = resp + startTime;
resp = resp + "\n";
resp = resp + startBoot;
resp = resp + "\n</html>";
client.println(resp);

Serial.println("resp");
Serial.println(resp);
Serial.println("");

  float heap = ESP.getFreeHeap();;

  delay(1);
  Serial.println("Client disonnected");
  Serial.println("");

  //client.stop();

}

