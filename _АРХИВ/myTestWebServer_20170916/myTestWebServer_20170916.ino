/*
static const uint8_t D0   = 16; *
static const uint8_t D1   = 5;  *
static const uint8_t D2   = 4;  *
static const uint8_t D3   = 0;  *
static const uint8_t D4   = 2;  *
static const uint8_t D5   = 14; *
static const uint8_t D6   = 12; *
static const uint8_t D7   = 13;
static const uint8_t D8   = 15;
static const uint8_t D9   = 3;  *
static const uint8_t D10  = 1;  *
.... = 10                       *
*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>
/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

WiFiServer server(80);

boolean startBoot = false;
double startTime = 0;

int lights[4] = {12,3,1,10};
int buttons[3] = {16,5,4};
int indicators[3] = {0,2,14};
boolean lightStat[4] = {true, true, true, true};
boolean CooHooStatus = false;

String resp1;
String resp2;

/*_______________________________________________________________________________________________________________________________*/
void setup() {
  Serial.begin(115200);

  Serial.println("Booting");
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   //ArduinoOTA.setHostname("LuaV1_TestSwitcher_ServerClient");
   ArduinoOTA.setHostname("LuaV2_CorridorSwitcher_ServerClient");   //------------------------------------------------------------/

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Start the server
  server.begin();
  Serial.println("Server started");
 
  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");


  pinMode(buttons[0], INPUT); 
  pinMode(buttons[1], INPUT); 
  pinMode(buttons[2], INPUT); 

  pinMode(indicators[0], OUTPUT);
  pinMode(indicators[1], OUTPUT);
  pinMode(indicators[2], OUTPUT);
  digitalWrite(indicators[0], HIGH);
  digitalWrite(indicators[1], HIGH);
  digitalWrite(indicators[2], HIGH);
  
  pinMode(lights[0], OUTPUT);
  pinMode(lights[1], OUTPUT);
  pinMode(lights[2], OUTPUT);
  pinMode(lights[3], OUTPUT);    
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[3], HIGH);

}

/*****************************************************************************************************************************************/
void loop() {
  my_switcher();
  if (startBoot) my_booting();
  else web_server();
}
/*****************************************************************************************************************************************/
void my_booting() {
  if (startTime > millis()) {
    ArduinoOTA.handle();
  }
  else {
    startBoot = false;
  }
}
/*****************************************************************************************************************************************/
void my_switcher() {
  //Первая кнопка-------------------------------------------------------------------------------------------------
  int butStatus=return_button(buttons[0]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) {
      lightStat[0] =!lightStat[0];
      digitalWrite(lights[0], lightStat[0]);
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      web_client("http://192.168.1.15/SWITCH1");
    }
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();
  }
  //Вторая кнопка-------------------------------------------------------------------------------------------------
  if (return_button(buttons[1]) > 2) {
    lightStat[1] =!lightStat[1];
    digitalWrite(lights[1], lightStat[1]);
    digitalWrite(indicators[1], lightStat[1]);
  }
 
 //Третья кнопка-------------------------------------------------------------------------------------------------
  butStatus=return_button(buttons[2]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) {
      if(lightStat[3]==false) lightStat[2] = true; //Если включен свет по длинному нажатию, по короткому его тоже выключаем
      else lightStat[2] =!lightStat[2];
      lightStat[3] = true;
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      lightStat[3] =!lightStat[3];
      lightStat[2] = true;
    }
   
   //Обрабатываем общий индикатор на два света
   if (lightStat[2] == true && lightStat[3] == true) digitalWrite(indicators[2], true);
   else digitalWrite(indicators[2], false);

   //Непосредственно включаем свет в соответствии с переменными
   digitalWrite(lights[2], lightStat[2]);
   digitalWrite(lights[3], lightStat[3]);
  }
}
/*****************************************************************************************************************************************/
//Функция возвращает количество итераций по 5 милисекунд за время нажатой кнопки
int return_button(int buttonPin) {
  int i = 0;
  boolean buttonDown = digitalRead(buttonPin)==HIGH;
  if (buttonDown) {
    while (digitalRead(buttonPin)==HIGH)
    {
      delay(5);
      i=i+1;
    }
  //delay(1000);
  }
  return i;
}
/*****************************************************************************************************************************************/
void web_server() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  Serial.println("new client");

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();
 
  // Match the request
  int value = LOW;
  if (request.indexOf("/SWITCH1") != -1)  {
    lightStat[0] =!lightStat[0];
    digitalWrite(lights[0], lightStat[0]);
    Refresh_Indicators();
    if (lightStat[0]) resp1 = "red OFF\n";
    else resp1 = "green ON\n";
  }
  if (request.indexOf("/SWITCH2") != -1)  {
    lightStat[1] =!lightStat[1];
    digitalWrite(lights[1], lightStat[1]);
    digitalWrite(indicators[1], lightStat[1]);
  }
  if (request.indexOf("/SWITCH3") != -1)  {
    lightStat[2] =!lightStat[2];
    digitalWrite(lights[2], lightStat[2]);
    digitalWrite(lights[3], true);
    digitalWrite(indicators[2], lightStat[2]);
  }
  if (request.indexOf("/SWITCH4") != -1)  {
    lightStat[3] =!lightStat[3];
    digitalWrite(lights[3], lightStat[3]);
    digitalWrite(lights[2], true);
    digitalWrite(indicators[2], lightStat[3]);
  }
  if (request.indexOf("/COOHOOON") != -1)  {
    CooHooStatus = true; //Вытяжку включили
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();
}
  if (request.indexOf("/COOHOOOFF") != -1)  {
    CooHooStatus = false; //Вытяжку включили
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();
}

  if (request.indexOf("/STATUS_ALL") != -1)  {
    resp1 = "\nKITCHEN___";
    resp1 += !lightStat[0];

    resp1 += "\nWC_________";
    resp1 += !lightStat[1];

    resp1 += "\nBATH-1_____";
    resp1 += !lightStat[2];

    resp1 += "\nBATH-2_____";
    resp1 += !lightStat[3];
  
  
  }
  if (request.indexOf("/STATUS1") != -1)  {
    if (lightStat[0]) resp1 = "red OFF\n";
    else resp1 = "green ON\n";
  }
  if (request.indexOf("/STATUS2") != -1)  {
    if (lightStat[1]) resp1 = "red OFF\n";
    else resp1 = "green ON\n";
  }
  if (request.indexOf("/STATUS3") != -1)  {
    if (lightStat[2]) resp1 = "red OFF\n";
    else resp1 = "green ON\n";
  }
  if (request.indexOf("/STATUS4") != -1)  {
    if (lightStat[3]) resp1 = "red OFF\n";
    else resp1 = "green ON\n";
  }
  
  if (request.indexOf("/BOOTING") != -1)  {    
    startTime = millis() + 10000;
    startBoot = true;
    resp1 = "BOOT ME";
  }

  if (request.indexOf("/TEST") != -1)  {
    resp1 = resp2;
  }
  

  String resp = "HTTP/1.1 200 OK\n";
  resp = resp + "Content-Type: text/html\n";
  resp = resp + "\n";
  //resp = resp + "<!DOCTYPE HTML>\n";
  //resp = resp + "<html>\n";
  resp = resp + resp1;
  //resp = resp + "\n</html>";
  client.println(resp);
  
  Serial.println("resp");
  Serial.println(resp);
  Serial.println("");

  float heap = ESP.getFreeHeap();;

  delay(1);
  Serial.println("Client disonnected");
  Serial.println("");

  //client.stop();

}

/*****************************************************************************************************************************************/
void Refresh_Indicators() {
  //если горит вытяжка и не горит основной свет, зажигаем индикатор в полсилы
  if (CooHooStatus == true){//-------------------------------
    if (lightStat[0] == true) {
      analogWrite(indicators[0], 255);
    }
    else {
      analogWrite(indicators[0], 0);
    }
  } ///------------------------------------------------------
  else {
    if (lightStat[0] == true) {
      analogWrite(indicators[0], 0);
      digitalWrite(indicators[0], true);
    }
    else {
      analogWrite(indicators[0], 0);
      digitalWrite(indicators[0], false);
    }
  }
  
}
/*****************************************************************************************************************************************/
void web_client(String request) {
 
 if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
 
   HTTPClient http;    //Declare object of class HTTPClient
 
   http.begin(request);      //Specify request destination
   http.addHeader("Content-Type", "text/plain");  //Specify content-type header
 
   int httpCode = http.GET();   //Send the request
   String payload = http.getString();                  //Get the response payload

   resp2 = payload;

  if (payload == "CooHooOn"){
    resp2 = "Coo Hoo On";
  }
  else if (payload == "CooHooOff"){
    resp2 = "Coo Hoo OFF";
  }
  else {
    resp2 = "null";
  }

  
   //Serial.println(httpCode);   //Print HTTP return code
   //Serial.println(payload);    //Print request response payload
 
   http.end();  //Close connection
 
 }else{
    Serial.println("Error in WiFi connection");   
 }
 

}

