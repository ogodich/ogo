#define CLOCK_DISPLAY           10
#define BRIGHTNESS_SENSOR_SIGN  A7
#define BRIGHTNESS_SENSOR_VCC   11

//int brightnessBoarder = 200;
int brightnessBoarder = 100;
boolean switchDisplay = false;

void setup() {
  //Serial.begin(9600);
  pinMode(CLOCK_DISPLAY, OUTPUT);
  pinMode(BRIGHTNESS_SENSOR_VCC, OUTPUT);  
  pinMode(BRIGHTNESS_SENSOR_SIGN, INPUT);
  
  digitalWrite(BRIGHTNESS_SENSOR_VCC, HIGH);
}

void loop() {
  digitalWrite(BRIGHTNESS_SENSOR_VCC, HIGH);
  int currBrightness = analogRead(BRIGHTNESS_SENSOR_SIGN);

  /*Serial.print("currBrightness=");
  Serial.print(currBrightness);
  Serial.print(",");  
  Serial.print("brightnessBoarder=");
  Serial.print(brightnessBoarder);
  Serial.println(".");*/  

  digitalWrite(CLOCK_DISPLAY, currBrightness > brightnessBoarder);
}
