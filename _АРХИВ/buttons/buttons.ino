#define LED_PIN_1        13
#define LED_PIN_2        7
#define BUTTON_PIN_1     11
#define BUTTON_PIN_2     12

#include <myFirstClass.h>
myFirstClass myFC_bt_1(12);     // создаём экземпляр нашего класса


boolean light = false;
boolean light2 = false;

/*===============================================================================*/
void setup() {

  Serial.begin(9600);
  pinMode(LED_PIN_1, OUTPUT);
  pinMode(LED_PIN_2, OUTPUT);
  
  pinMode(BUTTON_PIN_1, INPUT_PULLUP);
  pinMode(BUTTON_PIN_2, INPUT_PULLUP);
}
/*===============================================================================*/


boolean firstLedStatus=false;
boolean secondLedStatus=false;
/*********************************************************************************/
void loop() {
  funkMyButtonTesting();
  //funkMyDelayTesting();
}
/*********************************************************************************/
void funkMyDelayTesting()
{
  if (funkMyDelay(0, 1000)) {
    firstLedStatus = !firstLedStatus;
    digitalWrite(LED_PIN_1, firstLedStatus);
  }

  if (funkMyDelay(1, 2000)) {
    secondLedStatus = !secondLedStatus;
    digitalWrite(LED_PIN_2, secondLedStatus);
  }  
}
/*********************************************************************************/
int timersMap[10] = {0};
boolean funkMyDelay(int parTimerNumber, int parDelay) {
  int now = millis(); 
  boolean result=false;
  if (now % parDelay == 0 && timersMap[parTimerNumber]!=now)  result=true;  
  timersMap[parTimerNumber]=now;
  return result;
}
/*********************************************************************************/
void funkMyButtonTesting(){
  String click = myFC_bt_1.funkMyClickCheck();

  if (click == "short"){
    light=!light;
    digitalWrite(LED_PIN_1, light);
  }
  else if (click == "long"){
    light2=!light2;
    digitalWrite(LED_PIN_2, light2);
  }
}
/*********************************************************************************/
