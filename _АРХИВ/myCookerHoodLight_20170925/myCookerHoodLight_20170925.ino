/* Node MCU D1 mini
Pin  Function  ESP-8266 Pin
TX  TXD TXD
RX  RXD RXD
A0  Analog input, max 3.3V input  A0
D0  IO  GPIO16
D1  IO, SCL GPIO5
D2  IO, SDA GPIO4
D3  IO, 10k Pull-up GPIO0
D4  IO, 10k Pull-up, BUILTIN_LED  GPIO2
D5  IO, SCK GPIO14
D6  IO, MISO  GPIO12
D7  IO, MOSI  GPIO13
D8  IO, 10k Pull-down, SS GPIO15 (Нельзя делать выходом, не стартует)
G Ground  GND
5V  5V  -
3V3 3.3V  3.3V
RST Reset RST
*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>

#define BOARD_NAME "NodeMCU_D1mini_CooHoo"
#define KORR_IP "192.168.1.120"


/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

WiFiServer server(80);

boolean startBoot = false;
double startTime = 0;

int lights[1] = {14};
int buttons[4] = {4, 12, 13, 5};

boolean lightStat[1] = {true};

String resp1;

boolean buttonIs = false;
boolean buttonWas = false;

boolean statusChanged = false;

long timer1 = 0;
/*_______________________________________________________________________________________________________________________________*/
void setup() {
  Serial.begin(115200);
  //Serial.println("Booting");
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    //Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

//  pinMode(ledPin, OUTPUT);
//  digitalWrite(ledPin, LOW);

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  //Serial.println("Ready");
  //Serial.print("IP address: ");
  //Serial.println(WiFi.localIP());

  // Start the server
  server.begin();
  //Serial.println("Server started");
 
  // Print the IP address
  //Serial.print("Use this URL to connect: ");
  //Serial.print("http://");
  //Serial.print(WiFi.localIP());
  //Serial.println("/");

  pinMode(buttons[0], INPUT);
  pinMode(buttons[1], INPUT);
  pinMode(buttons[2], INPUT);
  pinMode(buttons[3], INPUT);
  
  pinMode(lights[0], OUTPUT);
  digitalWrite(lights[0], true);

}

/*****************************************************************************************************************************************/
void loop() {
//my_multy_switcher();
my_switcher();
if (startBoot) my_booting();
else {
  web_server();
  Send_my_status();
  }
}
/*****************************************************************************************************************************************/
void my_booting() {

  Serial.println(millis());
  
  if (startTime > millis()) {
    ArduinoOTA.handle();
  }
  else {
//    digitalWrite(ledPin, false);
    startBoot = false;
  }
}
/*****************************************************************************************************************************************/
void my_switcher() {
  //Первая кнопка-------------------------------------------------------------------------------------------------
  buttonIs = digitalRead(buttons[0]);
  if (buttonIs != buttonWas) {
    for (int i = 0; i < 3; i = i + 1) {
      if (buttonIs != buttonWas){
        delay(200);
        buttonIs = digitalRead(buttons[0]);
      }
    }
  }
  if (buttonIs != buttonWas) {
    lightStat[0] =!lightStat[0];
    digitalWrite(lights[0], lightStat[0]);
    statusChanged = true; //Отправка оповещения
  }
  buttonWas = buttonIs;

  //Вторая кнопка-------------------------------------------------------------------------------------------------
  int butStatus=return_button(buttons[1]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 70) {
      lightStat[0] = !lightStat[0];
      digitalWrite(lights[0], lightStat[0]);
      statusChanged = true; //Отправка оповещения
    }
    //Длинное нажатие
    if (butStatus >= 70) {
      web_client("http://192.168.1.121/CHANGECOL0r1");      
    }
    butStatus = 0;
  }

  //Третья кнопка-------------------------------------------------------------------------------------------------
  butStatus=return_button(buttons[2]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 70) { 
      web_client("http://192.168.1.120/SWITCH1");
    }
    //Длинное нажатие
    if (butStatus >= 70) {
      web_client("http://192.168.1.120/SWITCH1");
    }
    butStatus = 0;
  }
  //Четвертая кнопка-------------------------------------------------------------------------------------------------
  butStatus=return_button(buttons[3]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 70) { 
      web_client("http://192.168.1.121/SWITCH1");
    }
    //Длинное нажатие
    if (butStatus >= 70) {
      web_client("http://192.168.1.121/SWITCH2");
    }
    butStatus = 0;
  }

  
}
/*****************************************************************************************************************************************/
void web_server() {
    // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  //Serial.println("new client");

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  //Serial.println(request);
  client.flush();
 
  // Match the request
   if (request.indexOf("/SWITCH1") != -1)  {
    lightStat[0] =!lightStat[0];
    delay(10);
    digitalWrite(lights[0], lightStat[0]);
    delay(10);
    //Send_my_status();
    statusChanged = true;
    
    if (lightStat[0]) resp1 = "CooHooOff";
    else resp1 = "CooHooOn";
  }
 
  else if (request.indexOf("/BOOTING") != -1)  {    
    startTime = millis() + 10000;
    startBoot = true;
    resp1 = "BOOT ME";
  }

  String resp = "HTTP/1.1 200 OK\n";
  //resp = resp + "Content-Type: text/html\n";
  resp = resp + "\n";
  //resp = resp + millis();
  //resp = resp + "<!DOCTYPE HTML>\n";
  //resp = resp + "<html>\n";
  resp = resp + resp1;
  // resp = resp + "\n</html>";
  client.println(resp);

  float heap = ESP.getFreeHeap();

  delay(1);
  //Serial.println("Client disonnected");
  //Serial.println("");

  //client.stop();
}
/*****************************************************************************************************************************************/
void Send_my_status() {
  if (/*millis() > timer1 && */statusChanged){
    delay(10);
    
      String URL = "http://";
       URL = URL + KORR_IP;
       URL = URL + "/COOHOO_";
       URL = URL + !lightStat[0];
      
      web_client(URL);
    
    //timer1 = millis() + 2000;
    statusChanged = false;
  }
}

/*****************************************************************************************************************************************/
void web_client(String request) {
 
 if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status

   HTTPClient http;    //Declare object of class HTTPClient
   http.setTimeout(10);
 
   http.begin(request);      //Specify request destination
   http.addHeader("Content-Type", "text/plain");  //Specify content-type header

   http.setTimeout(10);
   int httpCode = http.GET();   //Send the request
   //String payload = http.getString();                  //Get the response payload
 
   ////Serial.println(httpCode);   //Print HTTP return code
   ////Serial.println(payload);    //Print request response payload
 
   http.end();  //Close connection
 
 }else{
    //Serial.println("Error in WiFi connection");   
 }
 
}
/*****************************************************************************************************************************************/
//Функция возвращает количество итераций по 5 милисекунд за время нажатой кнопки
int return_button(int buttonPin) {
  int i = 0;
  boolean buttonDown = digitalRead(buttonPin)==LOW;
  if (buttonDown) {
    while (digitalRead(buttonPin)==LOW)
    {
      delay(5);
      i=i+1;
    }
  //delay(1000);
  }
  return i;
}

