// ArduinoJson - arduinojson.org
// Copyright Benoit Blanchon 2014-2018
// MIT License
//
// This example shows how to deserialize a JSON document with ArduinoJson.

#include <ArduinoJson.h>

    char myVar[]="{\"settings\":{1:1,2:2}}";
    StaticJsonBuffer<200> buffer1;
    JsonObject& jRep = buffer1.parseObject(myVar);

String strJSon = "{1:{\"w\":[111,111,111]},2:{\"w\":[11,12,13]}}";

void setup() {
  // Initialize serial port
  Serial.begin(9600);
  while (!Serial) continue;

  // Memory pool for JSON object tree.
  //
  // Inside the brackets, 200 is the size of the pool in bytes.
  // Don't forget to change this value to match your JSON document.
  // Use arduinojson.org/assistant to compute the capacity.
  StaticJsonBuffer<200> jsonBuffer;
  StaticJsonBuffer<200> myJBuff;

  // StaticJsonBuffer allocates memory on the stack, it can be
  // replaced by DynamicJsonBuffer which allocates in the heap.
  //
  // DynamicJsonBuffer  jsonBuffer(200);

  // JSON input string.
  //
  // It's better to use a char[] as shown here.
  // If you use a const char* or a String, ArduinoJson will
  // have to make a copy of the input in the JsonBuffer.
  char json[] = "{1:{\"w\":[10,90,250]},2:{\"w\":[11,12,13]}}";
      //"{1:  {1:{\"stat\":1,\"w\":255,\"g\":100},   2:{\"stat\":0,\"w\":100,\"g\":30},   3:{\"stat\":1,\"w\":10,\"g\":30}}, 2:[1,2,3]}";
      //"{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}";

  // Root of the object tree.
  //
  // It's a reference to the JsonObject, the actual bytes are inside the
  // JsonBuffer with all the other nodes of the object tree.
  // Memory is freed when jsonBuffer goes out of scope.
  JsonObject& root = jsonBuffer.parseObject(json);

  // Test if parsing succeeds.
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return;
  }
  root.prettyPrintTo(Serial);

  Serial.println("---");

  /*for (int i=1; i<=3; i++){
    boolean stat = root["1"][String(i)]["stat"];    
    if(stat){
      int w = root["1"][String(i)]["w"];
      Serial.println(w);
    }
  }

  for (int i=0; i<3; i++){
    int test = root["2"][i];
    if(test == 2) root["2"][i]=222;
  }

  for (int i=0; i<3; i++){
    int test = root["2"][i];
    Serial.println(test);
  }*/



  /*JsonObject& jParams = myJBuff.createObject();
  JsonObject& jParts = myJBuff.createObject();

  jParams["w"] = 255;
  jParams["r"] = 100;
  jParts["1"] = jParams;
  jParts["2"] = jsonBuffer.parseObject("{\"w\":99}");
  jParts.prettyPrintTo(Serial);
  Serial.println("");
  Serial.println("---");
  Serial.println(jParts["2"]["w"].as<char*>());*/


  // Fetch values.
  //
  // Most of the time, you can rely on the implicit casts.
  // In other case, you can do root["time"].as<long>();
  /*const char* sensor = root["sensor"];
  long time = root["time"];
  double latitude = root["data"][0];
  double longitude = root["data"][1];*/

  // Print values.
  //Serial.println(sensor);
  //Serial.println(time);

  
  /*Serial.println(root[i]["white"][0].as<char*>());
  Serial.println(root[i]["white"][1].as<char*>());
  Serial.println(root[i]["red"][0].as<char*>());
  Serial.println(root[i]["red"][1].as<char*>());*/


 
  
  /*Serial.println();
  for (int y=1; y<=2; y++){
    Serial.println(root["1"][String(y)]["w"].as<char*>());*/




    jRep.prettyPrintTo(Serial);
    Serial.println();
    

    test();
    test2();
  }

  void test (){
    char myVar2[]="{3:3,4:4}";
    //StaticJsonBuffer<200> buffer2;
    jRep["settings"] = buffer1.parseObject(myVar2);
    jRep.prettyPrintTo(Serial);
    jRep.prettyPrintTo(Serial);
  }

  void test2 (){
    strJSon = "{1:{\"w\":[10,90,250]},2:{\"w\":[11,12,13]}}";
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& jPresets = jsonBuffer.parseObject(strJSon);
    jPresets.prettyPrintTo(Serial);
    if (!jPresets.success()) {
      Serial.println("parseObject() failed");
      return;
    }
  }


void loop() {
  // not used in this example
}

// See also
// --------
//
// The website arduinojson.org contains the documentation for all the functions
// used above. It also includes an FAQ that will help you solve any
// deserialization problem.
// Please check it out at: https://arduinojson.org/
//
// The book "Mastering ArduinoJson" contains a tutorial on deserialization.
// It begins with a simple example, like the one above, and then adds more
// features like deserializing directly from a file or an HTTP request.
// Please check it out at: https://arduinojson.org/book/
