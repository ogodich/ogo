/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald
 */


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin 13 as an output.
  pinMode(1, INPUT_PULLUP); 
  pinMode(2, INPUT_PULLUP); 
  pinMode(3, INPUT_PULLUP); 
  pinMode(4, INPUT_PULLUP); 



  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(A4, OUTPUT); 
  
    pinMode(13, OUTPUT);
 
//digitalWrite(A1, HIGH);
//digitalWrite(A2, HIGH);
//digitalWrite(A3, HIGH);
//digitalWrite(A4, HIGH);
}

// the loop function runs over and over again forever
void loop() {
  /*digitalWrite(A1, LOW);   // turn the LED on (LOW is the voltage level)
  delay(2000);              // wait for a second
  digitalWrite(A1, HIGH);    // turn the LED off by making the voltage HIGH
  delay(2000);              // wait for a second

  digitalWrite(A2, LOW);   // turn the LED on (LOW is the voltage level)
  delay(2000);              // wait for a second
  digitalWrite(A2, HIGH);    // turn the LED off by making the voltage HIGH
  delay(2000);              // wait for a second

  digitalWrite(A3, LOW);   // turn the LED on (LOW is the voltage level)
  delay(2000);              // wait for a second
  digitalWrite(A3, HIGH);    // turn the LED off by making the voltage HIGH
  delay(2000);              // wait for a second

  digitalWrite(A4, LOW);   // turn the LED on (LOW is the voltage level)
  delay(2000);              // wait for a second
  digitalWrite(A4, HIGH);    // turn the LED off by making the voltage HIGH
  delay(2000);              // wait for a second
*/
digitalWrite(A1, digitalRead(1)); 
digitalWrite(A2, digitalRead(2)); 
digitalWrite(A3, digitalRead(3)); 
digitalWrite(A4, digitalRead(12)); 

digitalWrite(13, digitalRead(12)); 

}
