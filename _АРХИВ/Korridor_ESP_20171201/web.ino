/*****************************************************************************************************************************************/
void web_server() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  //Serial.println("new client");

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  //Serial.println(request);
  client.flush();
 
  // Match the request
  if (request.indexOf("/SWITCH1") != -1)  {
    lightStat[0] =!lightStat[0];
    digitalWrite(lights[0], lightStat[0]);
    Refresh_Indicators();
    if (lightStat[0]) resp1 = "red OFF\n";
    else resp1 = "green ON\n";
  }
  else if (request.indexOf("/SWITCH2") != -1)  {
    lightStat[1] =!lightStat[1];
    digitalWrite(lights[1], lightStat[1]);
    digitalWrite(indicators[1], lightStat[1]);
  }
  else if (request.indexOf("/SWITCH3") != -1)  {
    lightStat[2] =!lightStat[2];
    digitalWrite(lights[2], lightStat[2]);
    digitalWrite(lights[3], true);
    digitalWrite(indicators[2], lightStat[2]);
  }
  else if (request.indexOf("/SWITCH4") != -1)  {
    lightStat[3] =!lightStat[3];
    digitalWrite(lights[3], lightStat[3]);
    digitalWrite(lights[2], true);
    digitalWrite(indicators[2], lightStat[3]);
  }
  else if (request.indexOf("/COOHOO_1") != -1)  {
    CooHooStatus = true; //Вытяжку включили
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();
  }
  else if (request.indexOf("/COOHOO_0") != -1)  {
    CooHooStatus = false; //Вытяжку включили
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();
  }
  else if (request.indexOf("/STATUS_ALL") != -1)  {
    resp1 = "\nKITCHEN___";
    resp1 += !lightStat[0];

    resp1 += "\nCOOHOO___";
    resp1 += CooHooStatus;

    resp1 += "\nWC_________";
    resp1 += !lightStat[1];

    resp1 += "\nBATH-1_____";
    resp1 += !lightStat[2];

    resp1 += "\nBATH-2_____";
    resp1 += !lightStat[3];
  }
  else if (request.indexOf("/BOOTING") != -1)  {    
    startTime = millis() + 10000;
    startBoot = true;
    resp1 = "BOOT ME";
  }
  else if (request.indexOf("/TEST") != -1)  {
    resp1 = resp2;
  }
  else if (request.indexOf("/BOUNCE") != -1)  {
    resp1 = "BOUNCE=\n";
    resp1 += "Pin0=";
    resp1 += bounce[buttons[0]];
    resp1 += ",\nPin1=";
    resp1 += bounce[buttons[1]];
    resp1 += ",\nPin2=";
    resp1 += bounce[buttons[2]];
  }
  

  String resp = "HTTP/1.1 200 OK\n";
  resp = resp + "Content-Type: text/html\n";
  resp = resp + "\n";
  //resp = resp + "<!DOCTYPE HTML>\n";
  //resp = resp + "<html>\n";
  resp = resp + resp1;
  //resp = resp + "\n</html>";
  client.println(resp);
  
  //Serial.println("resp");
  //Serial.println(resp);
  //Serial.println("");

  float heap = ESP.getFreeHeap();;

  delay(1);
  //Serial.println("Client disonnected");
  //Serial.println("");

  //client.stop();

}

/*****************************************************************************************************************************************/
void Refresh_Indicators() {
  //если горит вытяжка и не горит основной свет, зажигаем индикатор в полсилы
  if (CooHooStatus == true){//-------------------------------
    if (lightStat[0] == true) {
      analogWrite(indicators[0], 255);
    }
    else {
      analogWrite(indicators[0], 0);
    }
  } ///------------------------------------------------------
  else {
    if (lightStat[0] == true) {
      analogWrite(indicators[0], 0);
      digitalWrite(indicators[0], true);
    }
    else {
      analogWrite(indicators[0], 0);
      digitalWrite(indicators[0], false);
    }
  }
  
}
/*****************************************************************************************************************************************/
void web_client(String request) {
 
 if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
 
   HTTPClient http;    //Declare object of class HTTPClient
 
   http.begin(request);      //Specify request destination
   http.addHeader("Content-Type", "text/plain");  //Specify content-type header
 
   int httpCode = http.GET();   //Send the request
   String payload = http.getString();                  //Get the response payload

  /*if (payload.substring(0, 8) == "CooHooOn"){
    resp2 = "Coo Hoo On";
    CooHooStatus = true; //Вытяжку включили
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();
    
  }
  else if (payload.substring(0, 8) == "CooHooOf"){
    resp2 = "Coo Hoo OFF";
    CooHooStatus = false; //Вытяжку выключили
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();
    
  }
  else {
    resp2 = payload;
  }*/

  
   http.end();  //Close connection
 
 }else{
    //Serial.println("Error in WiFi connection");   
 }
 

}

