/*
ESP 01 PINS
 1-2-3-4
 5-6-7-8
 antena
 antena

 1 - VCC
 2 - RST, reset on LOW
 3 - EN
 4 - TX, GPIO1, don't start on LOW
 5 - RX, GPIO3
 6 - BOOT, GPIO0, don't start on LOW
 7 - GPIO2, don't start on LOW
 8 - GND
*
*/


#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
//#include <WiFiUdp.h>
//#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>
/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

WiFiServer server(80);

String resp1;
int buttons[3] = {3,1,0};
int light = 2;

  int x = 0;
  int y = 0;
  int z = 0;

/*_______________________________________________________________________________________________________________________________*/
void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Start the server
  server.begin();
  Serial.println("Server started");
 
  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");


  pinMode(buttons[0], INPUT); 
  pinMode(buttons[1], INPUT); 
  pinMode(buttons[2], INPUT); 
  pinMode(light, OUTPUT); 
  digitalWrite(light, HIGH);
}

/*****************************************************************************************************************************************/
void loop() {
  web_server();
  my_switcher();

 //my_multi_switcher(); 
}
/*****************************************************************************************************************************************/
void my_multi_switcher() {
 
  int i;

  boolean a = true;
  boolean b = true;
  boolean c = true;
  
  x = 0;
  y = 0;
  z = 0;
  
  while (digitalRead(buttons[0])==LOW /*|| digitalRead(buttons[1])==LOW || digitalRead(buttons[2])==LOW*/)
  {
    if (a && digitalRead(buttons[0])==LOW) x=x++;
    else a = false;
    
  /*  if (b && digitalRead(buttons[1])==LOW) y=y++;
    else b = false;

    if (c && digitalRead(buttons[2])==LOW) z=z++;
    else c = false;
   */ 
    delay(5);
    i=i+1;
  }

  if (x+y+z > 0){
    Serial.println ("x=" + x);
    Serial.print (", y=" + y);
    Serial.print (", z=" + z);
  }

}

void my_switcher() {
  //Первая кнопка-------------------------------------------------------------------------------------------------
  int butStatus=return_button(buttons[0]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) {
      digitalWrite(light, !digitalRead(light));
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      digitalWrite(light, !digitalRead(light));
    }
  }
  //Вторая кнопка-------------------------------------------------------------------------------------------------
  butStatus=return_button(buttons[1]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) { 
      web_client("http://192.168.1.15/SWITCH1");
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      web_client("http://192.168.1.15/SWITCH1");
    }
  }
  //Третья кнопка-------------------------------------------------------------------------------------------------
  butStatus=return_button(buttons[2]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) {
      web_client("http://192.168.1.19/SWITCH1");
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      web_client("http://192.168.1.19/SWITCH1");
    }
  }

}
/*****************************************************************************************************************************************/
//Функция возвращает количество итераций по 5 милисекунд за время нажатой кнопки
int return_button(int buttonPin) {
  int i = 0;
  boolean buttonDown = digitalRead(buttonPin)==LOW;
  if (buttonDown) {
    while (digitalRead(buttonPin)==LOW)
    {
      delay(5);
      i=i+1;
    }
  }
  return i;
}
/*****************************************************************************************************************************************/
void web_server() {
    // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  Serial.println("new client");

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();
 
  // Match the request
 if (request.indexOf("/TEST") != -1)  {
    resp1 = millis();
    /*resp1 += " button_0=";
    resp1 += digitalRead(buttons[0]);
    resp1 += ", button_1=";
    resp1 += digitalRead(buttons[1]);
    resp1 += ", button_2=";
    resp1 += digitalRead(buttons[2]);*/

    resp1 += "x=";
    resp1 += x;
    resp1 += "y=";
    resp1 += y;
    resp1 += "z=";
    resp1 += z;
    

    resp1 += "\n";
  }

if (request.indexOf("/SWITCH1") != -1)  {
    resp1 = millis();
    resp1 += " SWITCHED_1";

    digitalWrite(light, !digitalRead(light));

    resp1 += "\n";
  }



String resp = "HTTP/1.1 200 OK\n";
resp = resp + "Content-Type: text/html\n";
resp = resp + "\n";
//resp = resp + "<!DOCTYPE HTML>\n";
//resp = resp + "<html>\n";
resp = resp + resp1;
// resp = resp + "\n</html>";
client.println(resp);

Serial.println("resp");
Serial.println(resp);
Serial.println("");

  float heap = ESP.getFreeHeap();;

  delay(1);
  Serial.println("Client disonnected");
  Serial.println("");
}
/*****************************************************************************************************************************************/
void web_client(String request) {
 
 if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
 
   HTTPClient http;    //Declare object of class HTTPClient
 
   http.begin(request);      //Specify request destination
   http.addHeader("Content-Type", "text/plain");  //Specify content-type header
 
   int httpCode = http.GET();   //Send the request
   //String payload = http.getString();                  //Get the response payload
 
   //Serial.println(httpCode);   //Print HTTP return code
   //Serial.println(payload);    //Print request response payload
 
   http.end();  //Close connection
 
 }else{
    Serial.println("Error in WiFi connection");   
 }
 
}
