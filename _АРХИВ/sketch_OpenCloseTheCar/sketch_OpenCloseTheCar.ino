int RELEVCC = 10;
int RELE1 = 11;
int RELE2 = 12;
String message;

void setup()
{
  Serial.begin(9600);

  pinMode(RELEVCC, OUTPUT);
  pinMode(RELE1, OUTPUT);
  pinMode(RELE2, OUTPUT);
  
  digitalWrite(RELEVCC, LOW);
  digitalWrite(RELE1, HIGH);
  digitalWrite(RELE2, HIGH);
}


void loop()
{ 
  int i = 0;
  while (Serial.available()) {
    delay(10);
    char myChar = Serial.read();
    message = message + myChar;
  }

  if (message != "") {
    digitalWrite(RELEVCC, HIGH);    
    delay(500);
//    Serial.println(message);

    if (message == "V lesu rodilas' elochka") {
      digitalWrite(RELE1, LOW);
      delay(500);
      digitalWrite(RELE1, HIGH);
    }

    if (message == "V lesu ona rosla") {
      digitalWrite(RELE2, LOW);
      delay(500);
      digitalWrite(RELE2, HIGH);
    }
    message = "";
    digitalWrite(RELEVCC, LOW);    
    
  }
}
