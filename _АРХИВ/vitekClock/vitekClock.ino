#define CLOCK_DISPLAY      12
#define BRIGHTNESS_SENSOR  A0

int brightnessBoarder = 200;
boolean switchDisplay = false;

void setup() {
  Serial.begin(9600);
  pinMode(CLOCK_DISPLAY, OUTPUT);
  pinMode(BRIGHTNESS_SENSOR, INPUT);
}

void loop() {
  int currBrightness = analogRead(BRIGHTNESS_SENSOR);
//  delay(2000);

  Serial.print("currBrightness=");
  Serial.print(currBrightness);
  Serial.print(",");  
  Serial.print("brightnessBoarder=");
  Serial.print(brightnessBoarder);
  Serial.println(".");  

  digitalWrite(CLOCK_DISPLAY, currBrightness > brightnessBoarder);
  //digitalWrite(CLOCK_DISPLAY, false);

//  delay(2000);
  

  int intRes = readString().toInt();
  if (intRes > 0) {
//    Serial.println(intRes);
    brightnessBoarder = intRes;
  }
  
}

String readString()
{
  String message = "";
  while (Serial.available()) {
    delay(10);
    char myChar = Serial.read();
    message = message + myChar;
  }
  return message;
}
