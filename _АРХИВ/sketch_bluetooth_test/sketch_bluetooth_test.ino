int LED1 = 2;
int LED2 = 3;
String message;

void setup()
{
  Serial.begin(9600);

  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  
  digitalWrite(LED1, LOW);
  digitalWrite(LED2, LOW);
}


void loop()
{ 
  while (Serial.available()) {
    delay(10);
    char myChar = Serial.read();
    message = message + myChar;
  }

  if (message != "") {
    delay(500);
//    Serial.println(message);

    if (message == "V lesu rodilas' elochka") {
      digitalWrite(LED1, HIGH);
      delay(500);
      digitalWrite(LED1, LOW);
    }

    if (message == "V lesu ona rosla") {
      digitalWrite(LED2, HIGH);
      delay(500);
      digitalWrite(LED2, LOW);
    }
    message = "";
    
  }
}
