void mqtt_loop(){
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();
//*********************************************************************************************************************************
  if (strcmp(topic,"light/kitchen/main")==0){
    if(call=="2") {
      lightStat[0] = !lightStat[0];
      snprintf (msg, 75, "%d", lightStat[0]);
      client.publish("light/kitchen/main", msg, true);
    }
    else lightStat[0] = (call.toInt());
  }
 //*********************************************
  if (strcmp(topic,"light/kitchen/coohoo")==0 && call !="2"){
    CooHooStatus = call.toInt(); //Вытяжку переключили
    //Обрабатываем общий индикатор на два света
    //Refresh_Indicators();
  }
 //*********************************************
  if (strcmp(topic,"light/toilet/1")==0){
    if(call=="2") {
    }
    else lightStat[1] = (call.toInt());
  }
 //*********************************************
  if (strcmp(topic,"light/bath/1")==0){
    if (call=="1") {
      lightStat[3] = 0;
      snprintf (msg, 75, "0");
      client.publish("light/bath/2", msg, true);
    }
    lightStat[2] = call.toInt();
  }
 //********************************************* 
  if (strcmp(topic,"light/bath/2")==0){
    if (call=="1") {
      lightStat[2] = 0;
      snprintf (msg, 75, "0");
      client.publish("light/bath/1", msg, true);
    }
    lightStat[3] = call.toInt();
  }

  DoSwitch(false);
}
//*********************************************************************************************************************************
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic_2", "reconnect");
      // ... and resubscribe

      client.subscribe("light/kitchen/main");
      client.subscribe("light/kitchen/coohoo");   
      client.subscribe("light/bath/+");
      client.subscribe("light/toilet/+");   
      
        
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
