boolean someButtonPressed = false;

int S1 = 0;
int S2 = 0;
int S3 = 0;

int L1 = 0;
int L2 = 0;
int L3 = 0;


void my_multy_switcher(){
  multyButtons();

  
  if (S1>1) debug += "S1=" + S1;


  if (false) int nu=0;
  
  //КОМБИНАЦИИ----------------------------------------------------------------------------------------------  
  //else if (L1==1 && S2==1)  client.publish("motivation/add", "3");
  //else if (L1==1 && S3==1)  client.publish("motivation/remove", "3");
  
  else if (L2==1 && S1==1)  {
    client.publish("radio", "2");
  }

  else if (L1==1 && L2==1)  {
    client.publish("light/kitchen/led_strip2", "-1");
  }

  else if (L1==1 && L3==1)  {
    client.publish("light/kitchen/corner", "2");
  }

  //Понижаем громкость, если зажата 2 и нажата больше одного 1
  else if (L2==1 && S1>1)  {
    snprintf (msg, 5, "%d", (S1-1)*(-1));
    client.publish("volume_change", msg);
  }
  //Повышаем громкость, если зажата 2 и нажата больше одного 3
  else if (L2==1 && S3>1)  {
    snprintf (msg, 5, "%d", S3-1);
    client.publish("volume_change", msg);
  }

  
  //ПЕРВАЯ кнопка, Короткое нажатие----------------------------------------------------------------------------------------------  
  else if (S1==1) {
    lightStat[0] = !lightStat[0];
    DoSwitch(true);
  }
  //ПЕРВАЯ кнопка, Длинное нажатие----------------------------------------------------------------------------------------------  
  else if (L1==1) client.publish("light/kitchen/coohoo", "2");
  
  //ВТОРАЯ кнопка, Короткое нажатие-------------------------------------------------------------------------------------------------
  else if (S2==1) {
    lightStat[1] = !lightStat[1];
    DoSwitch(true);
  }
    //ВТОРАЯ кнопка, Длинное нажатие----------------------------------------------------------------------------------------------  
  else if (L2==1)           client.publish("light/kitchen/led_strip", "2");
  
  //ТРЕТЬЯ кнопка, Короткое нажатие-------------------------------------------------------------------------------------------------
  else if (S3==1){
    if (lightStat[3]) lightStat[3] = false;
    else lightStat[2] = !lightStat[2];
    DoSwitch(true);
  }
  //ТРЕТЬЯ кнопка, Длинное нажатие----------------------------------------------------------------------------------------------  
  else if (L3==1){
    lightStat[2] = false;
    lightStat[3] = !lightStat[3];
    DoSwitch(true);
  }

  //Если хоть что-то нажато, запускаю общий обработчик
  /*if (someButtonPressed){
    DoSwitch(true);
    someButtonPressed = false;
  }*/
}

void DoSwitch(boolean publish){
  //если горит вытяжка и не горит основной свет, зажигаем индикатор в полсилы
  digitalWrite(lights[0], !lightStat[0]);
  digitalWrite(lights[1], !lightStat[1]);
  digitalWrite(lights[2], !lightStat[2]);
  digitalWrite(lights[3], !lightStat[3]);

  digitalWrite(indicators[0], !lightStat[0]);
  digitalWrite(indicators[1], !lightStat[1]);
  if (!lightStat[0] && CooHooStatus) analogWrite(indicators[0], 255);
  if (lightStat[2] || lightStat[3]) digitalWrite(indicators[2], false);
  else                              digitalWrite(indicators[2], true);
  
  if (publish) {
    snprintf (msg, 75, "%d", lightStat[0]);
    client.publish("light/kitchen/main", msg, true);
    snprintf (msg, 75, "%d", lightStat[1]);
    client.publish("light/toilet/1", msg, true);
    snprintf (msg, 75, "%d", lightStat[2]);
    client.publish("light/bath/1", msg, true);
    snprintf (msg, 75, "%d", lightStat[3]);
    client.publish("light/bath/2", msg, true);
  }
}


void DebugButton (){
    debug = "\n\n\nlightStat[2]=";
    debug += lightStat[2];
    debug += "\nlightStat[3]=";
    debug += lightStat[3];
    debug += "\ndigitalRead(lights[2])=";
    debug += digitalRead(lights[2]);
    debug += "\ndigitalRead(lights[3])=";
    debug += digitalRead(lights[3]);
    debug += ".";
    
  
}


/******************************************************************************************************************/
void multyButtons() {
  int i = 0;
  int y = 0;
  int z = 0;

  int ii = 0;
  int yy = 0;
  int zz = 0;


  S1 = 0;
  S2 = 0;
  S3 = 0;

  L1 = 0;
  L2 = 0;
  L3 = 0;

  

  /************************************************************************/
  
  while (digitalRead(buttons[0]) || i>0 || digitalRead(buttons[1]) || y>0 || digitalRead(buttons[2]) || z>0){
    if ((digitalRead(buttons[0]))) i++;
    else {
      if(i > 40){
        L1++;
        someButtonPressed = true;
      }
      else if(i > 3){
        S1++;
        someButtonPressed = true;
      }
      i=0;    
    }

    if ((digitalRead(buttons[1]))) y++;
    else {
      if(y > 40){
        L2++;
        someButtonPressed = true;
      }
      else if(y > 3){
        S2++;
        someButtonPressed = true;
      }
      y=0;    
    }

    if ((digitalRead(buttons[2]))) z++;
    else {
      if(z > 40){
        L3++;
        someButtonPressed = true;
      }
      else if(z > 3){
        S3++;
        someButtonPressed = true;
      }
      z=0;    
    }
    delay(10);
  }
  /************************************************************************/

}


/*****************************************************************************************************************************************/
void Refresh_Indicators() {
  //если горит вытяжка и не горит основной свет, зажигаем индикатор в полсилы
  if (CooHooStatus == true){//-------------------------------
    if (lightStat[0] == false) {
      analogWrite(indicators[0], 255);
    }
    else {
      analogWrite(indicators[0], 0);
    }
  } ///------------------------------------------------------
  else {
    if (lightStat[0] == true) {
      analogWrite(indicators[0], 0);
      digitalWrite(indicators[0], true);
    }
    else {
      analogWrite(indicators[0], 0);
      digitalWrite(indicators[0], false);
    }
  }
  
}
