/*****************************************************************************************************************************************/
const int stage = 80;

void my_switcher() {
  //Первая кнопка-------------------------------------------------------------------------------------------------
  int butStatus=return_button(buttons[0]);
  if (butStatus > 5) {
    //Короткое нажатие
    if (butStatus < stage) {
      lightStat[0] =!lightStat[0];
      digitalWrite(lights[0], lightStat[0]);
    }
    //Длинное нажатие
    if (butStatus >= stage) {
      /*String URL = "http://";
             URL += COOHOO_IP;
             URL += "/SWITCH1";
      web_client(URL);*/
      client.publish("light/kitchen/led_strip", "2");
    }
    //Обрабатываем общий индикатор на два света
    Refresh_Indicators();

    bounce[buttons[0]]=0;
  }
  //Вторая кнопка-------------------------------------------------------------------------------------------------
  butStatus=return_button(buttons[1]);
  if (butStatus > 5) {
    if (butStatus < stage) {
      //Короткое нажатие
      lightStat[1] =!lightStat[1];
      digitalWrite(lights[1], lightStat[1]);
      digitalWrite(indicators[1], lightStat[1]);
    }
      //Длинное нажатие
    if (butStatus >= stage) {
      /*String URL = "http://";
             URL += LEDSTRIP_IP;
             URL += "/ledStrip?pow=100";
      web_client(URL);*/
      client.publish("light/kitchen/coohoo", "2");
    }
    bounce[buttons[1]]=0;
  }
  
 //Третья кнопка-------------------------------------------------------------------------------------------------
  butStatus=return_button(buttons[2]);
  if (butStatus > 5) {
    //Короткое нажатие
    if (butStatus < stage) {
      if(lightStat[3]==false) lightStat[2] = true; //Если включен свет по длинному нажатию, по короткому его тоже выключаем
      else lightStat[2] =!lightStat[2];
      lightStat[3] = true;
    }
    //Длинное нажатие
    if (butStatus >= stage) {
      lightStat[3] =!lightStat[3];
      lightStat[2] = true;
    }
   
   //Обрабатываем общий индикатор на два света
   if (lightStat[2] == true && lightStat[3] == true) digitalWrite(indicators[2], true);
   else digitalWrite(indicators[2], false);

   //Непосредственно включаем свет в соответствии с переменными
   digitalWrite(lights[2], lightStat[2]);
   digitalWrite(lights[3], lightStat[3]);
  
    bounce[buttons[2]]=0;
  }
}
/*****************************************************************************************************************************************/
//Функция возвращает количество итераций по 5 милисекунд за время нажатой кнопки
int return_button(int buttonPin) {
  int i = 0;
  boolean buttonDown = digitalRead(buttonPin)==HIGH;
  if (buttonDown) {
    while (digitalRead(buttonPin)==HIGH)
    {
      delay(5);
      i=i+1;
      bounce[buttonPin]++;
    }
  //delay(1000);
  }
  return i;
}
