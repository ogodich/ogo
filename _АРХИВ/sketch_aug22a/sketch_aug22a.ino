/*
Pin  Function  ESP-8266 Pin
TX  TXD TXD
RX  RXD RXD
A0  Analog input, max 3.3V input  A0
D0  IO  GPIO16
D1  IO, SCL GPIO5
D2  IO, SDA GPIO4
D3  IO, 10k Pull-up GPIO0
D4  IO, 10k Pull-up, BUILTIN_LED  GPIO2
D5  IO, SCK GPIO14
D6  IO, MISO  GPIO12
D7  IO, MOSI  GPIO13
D8  IO, 10k Pull-down, SS GPIO15
G Ground  GND
5V  5V  -
3V3 3.3V  3.3V
RST Reset RST
*/



#include <ESP8266WiFi.h>

#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>


extern "C" {
#include "user_interface.h"
    uint16 readvdd33(void);
    bool wifi_set_sleep_type(sleep_type_t);
    sleep_type_t wifi_get_sleep_type(void);
}


const char* ssid = "ScorpGOS";
//const char* ssid = "EratusGal - SsWifi";
const char* password = "9031124945";

uint16_t timeout=0;

int ledPin = 0; // GPIO13
WiFiServer server(80);
 
void setup() {
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);

  Serial.begin(115200);
  delay(10);
 
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
 
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
 
  // Start the server
  server.begin();
  Serial.println("Server started");
 
  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");

  wiFiBoot();
}
 
void loop() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  Serial.println("new client");
/*
  while(!client.available()){
    delay(1);
    timeout++;
    if (timeout > 500)
    {
      client.flush();
      client.stop();
      break;
    }
  }
 */
  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();
 
  // Match the request
 
  int value = LOW;
  if (request.indexOf("/LED=ON") != -1)  {
    digitalWrite(ledPin, HIGH);
    value = HIGH;
  }
  if (request.indexOf("/LED=OFF") != -1)  {
    digitalWrite(ledPin, LOW);
    value = LOW;
  }
  if (request.indexOf("/LED=CHANGE") != -1)  {
    digitalWrite(ledPin, !digitalRead(ledPin));
    value = digitalRead(ledPin);
  }
 
// Set ledPin according to the request
//digitalWrite(ledPin, value);
 
  // Return the response
  /*client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); //  do not forget this one
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("test");
  client.println("</html>");
*/

String resp = "HTTP/1.1 200 OK\n";
resp = resp + "Content-Type: text/html\n";
resp = resp + "\n";
resp = resp + "<!DOCTYPE HTML>\n";
resp = resp + "<html>\n";
resp = resp + "te%sst\n";
resp = resp + "</html>";
client.println(resp);

Serial.println("resp");
Serial.println(resp);
Serial.println("");

  float heap = ESP.getFreeHeap();;

  delay(1);
  Serial.println("Client disonnected");
  Serial.println("");

  //client.stop();

  ArduinoOTA.handle();
}

/*---------------------------------------------------------------------------------*/
void wiFiBoot() {
  //Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  //WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname("gosD1mini_10");

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
