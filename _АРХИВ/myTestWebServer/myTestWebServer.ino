#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

int ledPin = 9;
WiFiServer server(80);

boolean startBoot = false;
double startTime = 0;

int lights[4] = {12,13,15,10};
int buttons[3] = {16,5,4};
int buttonLeds[3] = {0,2,14};
boolean lightStat[4] = {true, true, true, true};

/*_______________________________________________________________________________________________________________________________*/
void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname("LuaV3_1");

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Start the server
  server.begin();
  Serial.println("Server started");
 
  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");


  pinMode(buttons[0], INPUT); 
  pinMode(buttons[1], INPUT); 
  pinMode(buttons[2], INPUT); 

  pinMode(buttonLeds[0], OUTPUT);
  pinMode(buttonLeds[1], OUTPUT);
  pinMode(buttonLeds[2], OUTPUT);
  digitalWrite(buttonLeds[0], HIGH);
  digitalWrite(buttonLeds[1], HIGH);
  digitalWrite(buttonLeds[2], HIGH);
  
  pinMode(lights[0], OUTPUT);
  pinMode(lights[1], OUTPUT);
  pinMode(lights[2], OUTPUT);
  pinMode(lights[3], OUTPUT);    
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[3], HIGH);
  Serial.begin(115200);

}

/*****************************************************************************************************************************************/
void loop() {
if (startBoot) my_booting();
else web_server();
}
/*****************************************************************************************************************************************/
void my_booting() {
  if (startTime > millis()) {
    digitalWrite(ledPin, true);
    ArduinoOTA.handle();
  }
  else {
    digitalWrite(ledPin, false);
    startBoot = false;
  }
}
/*****************************************************************************************************************************************/
void my_switcher() {
  //delay(1000);
  int i;
for (i = 0; i < 5; i = i + 1) {
  //Serial.println(lightStat[i]);

}
  //Serial.println(millis());
  
  //Первая кнопка-------------------------------------------------------------------------------------------------
  if (return_button(buttons[0]) > 2) {
    lightStat[0] =!lightStat[0];
    digitalWrite(lights[0], lightStat[0]);
    digitalWrite(buttonLeds[0], lightStat[0]);
  }
  //Вторая кнопка-------------------------------------------------------------------------------------------------
  if (return_button(buttons[1]) > 2) {
    lightStat[1] =!lightStat[1];
    digitalWrite(lights[1], lightStat[1]);
    digitalWrite(buttonLeds[1], lightStat[1]);
  }
 
 //Третья кнопка-------------------------------------------------------------------------------------------------
  int butStatus=return_button(buttons[2]);
  if (butStatus > 2) {
    //Короткое нажатие
    if (butStatus < 100) {
      if(lightStat[3]==false) lightStat[2] = true; //Если включен свет по длинному нажатию, по короткому его тоже выключаем
      else lightStat[2] =!lightStat[2];
      lightStat[3] = true;
    }
    //Длинное нажатие
    if (butStatus >= 100) {
      lightStat[3] =!lightStat[3];
      lightStat[2] = true;
    }
   
   //Обрабатываем общий индикатор на два света
   if (lightStat[2] == true && lightStat[3] && true) digitalWrite(buttonLeds[2], true);
   else digitalWrite(buttonLeds[2], false);

   //Непосредственно включаем свет в соответствии с переменными
   digitalWrite(lights[2], lightStat[2]);
   digitalWrite(lights[3], lightStat[3]);
  }
}
/*****************************************************************************************************************************************/
//Функция возвращает количество итераций по 5 милисекунд за время нажатой кнопки
int return_button(int buttonPin) {
  int i = 0;
  boolean buttonDown = digitalRead(buttonPin)==HIGH;
  if (buttonDown) {
    while (digitalRead(buttonPin)==HIGH)
    {
      delay(5);
      i=i+1;
    }
  //delay(1000);
  }
  return i;
}
/*****************************************************************************************************************************************/
void web_server() {
    // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  Serial.println("new client");
/*
  while(!client.available()){
    delay(1);
    timeout++;
    if (timeout > 500)
    {
      client.flush();
      client.stop();
      break;
    }
  }
 */
  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();
 
  // Match the request
 
  int value = LOW;
  if (request.indexOf("/LED=ON") != -1)  {
    digitalWrite(ledPin, HIGH);
    value = HIGH;
  }
  if (request.indexOf("/LED=OFF") != -1)  {
    digitalWrite(ledPin, LOW);
    value = LOW;
  }
  if (request.indexOf("/LED=CHANGE") != -1)  {
    digitalWrite(ledPin, !digitalRead(ledPin));
    value = digitalRead(ledPin);
  }
  if (request.indexOf("/BOOTING") != -1)  {    
    startTime = millis() + 10000;
    startBoot = true;
  }
 
// Set ledPin according to the request
//digitalWrite(ledPin, value);
 
  // Return the response
  /*client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); //  do not forget this one
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("test");
  client.println("</html>");
*/


String resp = "HTTP/1.1 200 OK\n";
resp = resp + "Content-Type: text/html\n";
resp = resp + "\n";
resp = resp + "<!DOCTYPE HTML>\n";
resp = resp + "<html>\n";
resp = resp + millis();
resp = resp + "\n";
resp = resp + startTime;
resp = resp + "\n";
resp = resp + startBoot;
resp = resp + "\n</html>";
client.println(resp);

Serial.println("resp");
Serial.println(resp);
Serial.println("");

  float heap = ESP.getFreeHeap();;

  delay(1);
  Serial.println("Client disonnected");
  Serial.println("");

  //client.stop();

}

