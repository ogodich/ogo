boolean button_1 = false;
boolean light_1 = false;
boolean wasLight_1 = false;

boolean button_2 = false;
boolean light_2 = false;
boolean wasLight_2 = false;

boolean button_3 = false;
boolean light_3 = false;
boolean wasLight_3 = false;

boolean light_4 = false;
boolean wasLight_4 = false;

void setup() {
  pinMode(2, INPUT); 
  pinMode(3, INPUT); 
  pinMode(4, INPUT); 

  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);

  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
  digitalWrite(7, LOW);
  
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT); 
   
  digitalWrite(9, HIGH);
  digitalWrite(10, HIGH);
  digitalWrite(11, HIGH);
  digitalWrite(12, HIGH);
  Serial.begin(9600);

}

void loop() {
  sub_button_1();
  sub_button_2();
  sub_button_3();

}

void sub_button_1() {
  button_1 = digitalRead(2)==HIGH;
  if (button_1) {
    int i = 0;
    while (digitalRead(2)==HIGH)
    {
      delay(20);
      i=i+1;
    }
    if (i>5) {
      button_1 = true;
      light_1 = !light_1;
    }
  }
  if (button_1){
   digitalWrite(9, light_1);
   digitalWrite(5, light_1);
  }
  wasLight_1 = light_1;
}

void sub_button_2() {
  button_2 = digitalRead(3)==HIGH;
  if (button_2) {
    int i = 0;
    while (digitalRead(3)==HIGH)
    {
      delay(20);
      i=i+1;
    }
    if (i>5) {
      button_2 = true;
      light_2 = !light_2;
    }
  }
  if (button_2) {
    digitalWrite(10, light_2);
    digitalWrite(6, light_2);    
  }
  wasLight_2 = light_2;
}

void sub_button_3() {
  button_3 = digitalRead(4)==HIGH;
  if (button_3) {
    int i = 0;
    while (digitalRead(4)==HIGH)
    {
      delay(20);
      i=i+1;
    }
    if (i>5 && i < 20) {
      light_3 = !light_3;
      light_4 = true;
      digitalWrite(11, light_3);
      digitalWrite(12, light_4);

      if (light_3 == true && light_4 && true){
        digitalWrite(7, true);
      }
      else digitalWrite(7, false);

     }
    else if (i>20) {
      light_4 = !light_4;
      light_3 = true;
      digitalWrite(11, light_3);
      digitalWrite(12, light_4);

      if (light_3 == true && light_4 && true){
        digitalWrite(7, true);
      }
      else digitalWrite(7, false);

    }
  }

}
