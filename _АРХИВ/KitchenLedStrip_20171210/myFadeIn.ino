void old_myFadeIn () {
  if (switchedStrip)  testLedStrip(0, 0, 0, 0);
  else                testLedStrip(130, 30, 10, 200);
  switchedStrip = !switchedStrip;
}

long timer2 = 0.0;
int i = 0;

void myFadeIn () {
  if (millis() > timer2){
    testLedStrip(map(i, 0, 255, 0, 130),
                 map(i, 0, 255, 0, 30),
                 map(i, 0, 255, 0, 10),
                 map(i, 0, 255, 0, 200)
                  );      
    i=i+2;
    timer2 = millis() + 10;
    
    if (i < 255) return;
    switchFadeIn = false;
    i = 0;
    strip.setPixelColor(0, strip.Color(0,200,0,0));
    strip.show();
  }
}

