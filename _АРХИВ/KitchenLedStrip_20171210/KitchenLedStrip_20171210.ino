#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>

#define BOARD_NAME "LuaV2_KitchenLenStrip"

#include <Adafruit_NeoPixel.h>
/*#ifdef __AVR__
  #include <avr/power.h>
#endif
*/
#define PIN            12

/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

WiFiServer server(80);

boolean startBoot = false;
double startTime = 0;

//int lights[1] = {14};
//int buttons[1] = {4};

//boolean lightStat[1] = {true};

String resp1;

//boolean buttonIs = false;
//boolean buttonWas = false;

boolean statusChanged = false;
boolean switched = false;
boolean switchedStrip = false;

long timer1 = 0;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(217, PIN, NEO_GRBW + NEO_KHZ800);

boolean switchFadeIn = false;

/*_______________________________________________________________________________________________________________________________*/
void setup() {
  Serial.begin(115200);
  //Serial.println("Booting");
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    //Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

//  pinMode(ledPin, OUTPUT);
//  digitalWrite(ledPin, LOW);

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();

  
  //Serial.println("Ready");
  //Serial.print("IP address: ");
  //Serial.println(WiFi.localIP());

  // Start the server
  server.begin();
  //Serial.println("Server started");

  strip.begin(); // This initializes the NeoPixel library.
  strip.setBrightness(255); // set brightness
  strip.show(); // Initialize all pixels to 'off'
}

/*****************************************************************************************************************************************/
boolean animate = false;
void loop() {
  if (startBoot) my_booting();
  else web_server();

  if (animate)   whiteOverRainbow(50,5000,5);
  if (switchFadeIn) myFadeIn ();
}
/*****************************************************************************************************************************************/
int curR = 0;
int curG = 0;
int curB = 0;
int curW = 0;

void testLedStrip(int r, int g, int b, int w) {
curR = r;
curG = g;
curB = b;
curW = w;
  for(int i=0;i<217;i++){
    strip.setPixelColor(i, strip.Color(r,g,b,w));    
  }
  strip.show(); // This sends the updated pixel color to the hardware.
}

void changeBright (char col, boolean dir){
  if (col == 'r') {
    curR = curR + 10;
    if (curR > 255) curR = 0;
  }

  if (col == 'g') {
    curG = curG + 10;
    if (curG > 255) curG = 0;
  }

  if (col == 'b') {
    curB = curB + 10;
    if (curB > 255) curB = 0;
  }

  if (col == 'w') {
    curW = curW + 10;
    if (curW > 255) curW = 0;
  }

  
  for(int i=0;i<217;i++){
    strip.setPixelColor(i, strip.Color(curR,curG,curB,curW));    
  }
  strip.show(); // This sends the updated pixel color to the hardware.
  
}
/*****************************************************************************************************************************************/
void my_booting() {
  if (startTime > millis()) {
    ArduinoOTA.handle();
  }
  else {
//    digitalWrite(ledPin, false);
    startBoot = false;
  }

}
/*****************************************************************************************************************************************/

