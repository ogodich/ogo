
void web_server() {
    // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  // Wait until the client sends some data
  //Serial.println("new client");

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  //Serial.println(request);
  client.flush();
 
  // Match the request
  if (request.indexOf("/SWITCH1") != -1)  {
    if (switchedStrip)  testLedStrip(0, 0, 0, 0);
    else                switchFadeIn = true; //testLedStrip(130, 30, 10, 200); 

    resp1 = switchedStrip;
    switchedStrip = !switchedStrip;
    
    animate = false;
  }

  else if (request.indexOf("/SWITCH2") != -1)  {    
    if (switchedStrip)  testLedStrip(0, 0, 0, 0);
    else                testLedStrip(13, 3, 1, 20); 

    resp1 = switchedStrip;
    switchedStrip = !switchedStrip;
    
    animate = false;
  }
  

  // Match the request
  else if (request.indexOf("/ADDRESS") != -1)  {
    resp1 = (request.substring(request.indexOf("*")+1, request.indexOf("|"))).toInt();
    int address = (request.substring(request.indexOf("*")+1, request.indexOf("|"))).toInt();

    if (switched) {
      strip.setPixelColor(address, strip.Color(0,0,0,0)); // Moderately bright green color.
      strip.show(); // This sends the updated pixel color to the hardware.
      }
    else {
      strip.setPixelColor(address, strip.Color(0,255,0,0)); // Moderately bright green color.
      strip.show(); // This sends the updated pixel color to the hardware.
    }
    switched = !switched;
  }

  // Match the request
  else if (request.indexOf("/COLOR") != -1)  {
      String command = request.substring(request.indexOf("0")+1, request.indexOf("2"));
      resp1 = command;
      
      if (command == "red") testLedStrip(255, 0, 0, 0);
      else if (command == "green") testLedStrip(0, 255, 0, 0);
      else if (command == "blue") testLedStrip(0, 0, 255, 0);
      else if (command == "whitea") testLedStrip(0, 0, 0, 255);      
      else if (command == "whiteb") testLedStrip(255, 255, 255, 0);
      else if (command == "whitered") testLedStrip(255, 0, 0, 255);
      else if (command == "whitegreen") testLedStrip(0, 255, 0, 255);
      else if (command == "whiteblue") testLedStrip(0, 0, 255, 255);
      else if (command == "rgbw") testLedStrip(255, 255, 255, 255);
      else if (command == "rgbwhalf") testLedStrip(130, 130, 130, 130);
  }

  // Match the request
  else if (request.indexOf("/CHANGECOL") != -1)  {
      String command = request.substring(request.indexOf("0")+1, request.indexOf("1"));
      resp1 = command;
      
      if (command == "r") changeBright('r', true);
      if (command == "g") changeBright('g', true);
      if (command == "b") changeBright('b', true);
      if (command == "w") changeBright('w', true);
  }

 
  else if (request.indexOf("/BOOTING") != -1)  {    
    startTime = millis() + 10000;
    startBoot = true;
    resp1 = "BOOT ME";
  }

  else if (request.indexOf("/ANIMATE") != -1)  {    
    resp1 = "ANIMATE";
    animate = !animate;
  }

  else if (request.indexOf("/FADEIN") != -1)  {    
    resp1 = "FADEIN";
    switchFadeIn = true;
  }

  String resp = "HTTP/1.1 200 OK\n";
  //resp = resp + "Content-Type: text/html\n";
  resp = resp + "\n";
  //resp = resp + millis();
  //resp = resp + "<!DOCTYPE HTML>\n";
  //resp = resp + "<html>\n";
  resp = resp + resp1;
  // resp = resp + "\n</html>";
  client.println(resp);

  float heap = ESP.getFreeHeap();

  delay(1);
  //Serial.println("Client disonnected");
  //Serial.println("");

  //client.stop();
}
