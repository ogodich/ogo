/*
static const uint8_t D0   = 16; * - к
static const uint8_t D1   = 5;  *
static const uint8_t D2   = 4;  * - к
static const uint8_t D3   = 0;  - не делать входом*
static const uint8_t D4   = 2;  * - не делать входом
static const uint8_t D5   = 14; * - к
static const uint8_t D6   = 12; *
static const uint8_t D7   = 13;
static const uint8_t D8   = 15;   - к
static const uint8_t D9   = 3;  *
static const uint8_t D10  = 1;  *
.... = 10                       *
*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif


#define BOARD_NAME "CORRIDOR_NEW"

#define SMART_LED_1            13


/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

boolean startBoot = false;
double startTime = 0;

//int lights[4] = {12,3,1,10};
int lights[4] = {D6,D9,1,10};

int buttons[5] = {D0, D2, D1, D5, D8};
boolean lightStatTest[8] = {false, false, false, false, false, false, false, false};
boolean lightStat[8] = {false, false, false, false, false, false, false, false};

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
//int value = 0;

boolean bounceStartPublish = false;
boolean bounceStartCount = false;
int bounceCount[5];
long bounceTimer = 0.0;

Adafruit_NeoPixel smartLed1 = Adafruit_NeoPixel(5, SMART_LED_1, NEO_GRB + NEO_KHZ800);

boolean testMode = false;









///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void setup() {
  Serial.begin(115200);
  //Serial.println("Booting");
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    //Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  client.subscribe("radio");

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
  ArduinoOTA.setPassword((const char *)"corridor");

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  //Serial.println("Ready");
  //Serial.print("IP address: ");
  //Serial.println(WiFi.localIP());

  // Start the server
  //server.begin();
  //Serial.println("Server started");
 
  // Print the IP address
  //Serial.print("Use this URL to connect: ");
  //Serial.print("http://");
  //Serial.print(WiFi.localIP());
  //Serial.println("/");

  pinMode(buttons[0], INPUT);
  pinMode(buttons[1], INPUT);
  pinMode(buttons[2], INPUT);
  pinMode(buttons[3], INPUT);
  pinMode(buttons[4], INPUT);
  
//  pinMode(lights[0], OUTPUT);
//  digitalWrite(lights[0], true);

  //Вторая лента
  smartLed1.begin(); // This initializes the NeoPixel library.
  smartLed1.setBrightness(255); // set brightness
  smartLed1.show(); // Initialize all pixels to 'off'

  pinMode(lights[0], OUTPUT);
  pinMode(lights[1], OUTPUT);
  pinMode(lights[2], OUTPUT);
  pinMode(lights[3], OUTPUT);    
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[3], HIGH);

}
void loop() {
  my_multy_switcher();
  mqtt_loop();
  ArduinoOTA.handle();
  check_bounce();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int S1 = 0;
int S2 = 0;
int S3 = 0;
int S4 = 0;
int S5 = 0;

int L1 = 0;
int L2 = 0;
int L3 = 0;
int L4 = 0;
int L5 = 0;



//String debug = "";

void my_multy_switcher(){
  multyButtons();


//==================================================================================================================
//TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  
//==================================================================================================================
  if (testMode){
    //ПЕРВАЯ кнопка-------------------------------------------------------------------------------------------------  
    if (S1==1) client.publish("light/test/1", "-1");
  
    //ВТОРАЯ кнопка-------------------------------------------------------------------------------------------------
    else if (S2==1) client.publish("light/test/2", "-1");
      
    //ТРЕТЬЯ кнопка-------------------------------------------------------------------------------------------------
    else if (S3==1) client.publish("light/test/3", "-1");
    else if (L3==1) client.publish("light/test/4", "-1");
  
    //ЧЕТВЕРТАЯ кнопка-------------------------------------------------------------------------------------------------
    else if (S4==1) client.publish("light/test/5", "-1");
    else if (L4==1) client.publish("light/test/6", "-1");
  
    //ПЯТАЯ кнопка-------------------------------------------------------------------------------------------------
    else if (S5==1) {
      if(lightStatTest[7]) client.publish("light/test/8", "0", true);
      else               client.publish("light/test/7", "-1");
    }
    else if (L5==1) client.publish("light/test/8", "-1");
  }

//==================================================================================================================
//REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  
//==================================================================================================================
  //КОМБИНАЦИИ-------------------------------------------------------------------------------------------------

  //Включаем РАДИО
  else if (L3==1 && S2==1)  {
    client.publish("radio", "2");
  }

  //Понижаем громкость, если зажата 2 и нажата больше одного 1
  else if (L3==1 && S2>1)  {
    snprintf (msg, 5, "%d", (S2-1)*(-1));
    client.publish("volume_change", msg);
  }
  //Повышаем громкость, если зажата 2 и нажата больше одного 3
  else if (L3==1 && S4>1)  {
    snprintf (msg, 5, "%d", S4-1);
    client.publish("volume_change", msg);
  }
  /*
  //ЛЕНТА. Кухня. Профиль 2
  else if (L1==1 && S4==2) client.publish("strip/kitchen/setPreset", "2");
  //ЛЕНТА. Кухня. Профиль 3
  else if (L1==1 && S4==3) client.publish("strip/kitchen/setPreset", "3");
  //ЛЕНТА. Кухня. Профиль 4
  else if (L1==1 && S4==4) client.publish("strip/kitchen/setPreset", "4");
  //ЛЕНТА. Прихожая. Профиль 1 / ВЫКЛ
  else if (L5==1 && L4==1) client.publish("light/hall/led_strip", "2");
  //ЛЕНТА. Прихожая. Профиль 2
  else if (L1==1 && S4==2) client.publish("strip/hall/setPreset", "2");
  //ЛЕНТА. Прихожая. Профиль 3
  else if (L1==1 && S4==3) client.publish("strip/hall/setPreset", "3");
  */

  //ПЕРВАЯ кнопка-------------------------------------------------------------------------------------------------  
  else if (S1==1) client.publish("light/toilet/1", "2");

  //ВТОРАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (S2==1) client.publish("light/kitchen/main", "-1");
    
  //ТРЕТЬЯ кнопка-------------------------------------------------------------------------------------------------
  else if (S3==1) client.publish("light/kitchen/corner", "2");
  else if (L3==1) client.publish("light/kitchen/coohoo", "2");

  //ЧЕТВЕРТАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (S4==1) client.publish("light/kitchen/led_strip", "2");
  else if (L4==1) client.publish("light/kitchen/led_strip2", "-1");

  //ПЯТАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (S5==1) {
    if(lightStat[7]) client.publish("light/bath/2", "0", true);
    else               client.publish("light/bath/1", "-1");
  }
  else if (L5==1) client.publish("light/bath/2", "-1");   
  

}//----------



/******************************************************************************************************************/
void multyButtons() {
  int i = 0;
  int y = 0;
  int z = 0;
  int j = 0;
  int k = 0;

  int ii = 0;
  int yy = 0;
  int zz = 0;
  int jj = 0;
  int kk = 0;


  S1 = 0;
  S2 = 0;
  S3 = 0;
  S4 = 0;
  S5 = 0;

  L1 = 0;
  L2 = 0;
  L3 = 0;
  L4 = 0;
  L5 = 0;

  

  /************************************************************************/
  
  while (digitalRead(buttons[0]) || i>0 || digitalRead(buttons[1]) || y>0 || digitalRead(buttons[2]) || z>0 || digitalRead(buttons[3]) || j>0 || digitalRead(buttons[4]) || k>0){
    if ((digitalRead(buttons[0]))){
      if(bounceStartCount) bounceCount[0] ++;
      i++;
    }
    else {
      if(i > 20){
        L1++;
      }
      else if(i > 2){
        S1++;
      }      
      i=0;    
    }

    if ((digitalRead(buttons[1]))) {
      if(bounceStartCount) bounceCount[1] ++;
      y++;
    }
    else {
      if(y > 20){
        L2++;
      }
      else if(y > 2){
        S2++;
      }
      y=0;    
    }

    if ((digitalRead(buttons[2]))) {
      if(bounceStartCount) bounceCount[2] ++;
      z++;
    }
    else {
      if(z > 20){
        L3++;
      }
      else if(z > 2){
        S3++;
      }
      z=0;    
    }

    if ((digitalRead(buttons[3]))) {
      if(bounceStartCount) bounceCount[3] ++;
      j++;
    }
    else {
      if(j > 20){
        L4++;
      }
      else if(j > 2){
        S4++;
      }
      j=0;    
    }

    if ((digitalRead(buttons[4]))) {
      if(bounceStartCount) bounceCount[4] ++;
      k++;
    }
    else {
      if(k > 20){
        L5++;
      }
      else if(k > 2){
        S5++;
      }
      k=0;    
    }
    
  //**************  
    delay(20);
  }
  /************************************************************************/

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void my_booting() {
  if (startTime > millis()) {
    ArduinoOTA.handle();
  }
  else {
    startBoot = false;
  }
}

void check_bounce(){
  if (bounceStartPublish){
    long now = millis();
    if (now - bounceTimer > 3000) {
      bounceTimer = now;
      snprintf (msg, 75, "1=%d, 2=%d, 3=%d, 4=%d, 5=%d", bounceCount[0], bounceCount[1], bounceCount[2], bounceCount[3], bounceCount[4]);
      Serial.println(msg);
      client.publish("bounce/corr/count", msg);
    }
  }
}

void refreshTestIndicators(){
  if      (lightStatTest[0])   smartLed1.setPixelColor(0, smartLed1.Color(0,200,0));
  else if (!lightStatTest[0])  smartLed1.setPixelColor(0, smartLed1.Color(250,0,0));

  if      (lightStatTest[1])   smartLed1.setPixelColor(4, smartLed1.Color(0,200,0));
  else if (!lightStatTest[1])  smartLed1.setPixelColor(4, smartLed1.Color(250,0,0));

  if      (lightStatTest[2] && lightStatTest[3])    smartLed1.setPixelColor(3, smartLed1.Color(200,200,0));
  else if (lightStatTest[2] && !lightStatTest[3])   smartLed1.setPixelColor(3, smartLed1.Color(0,200,0));
  else if (!lightStatTest[2] && lightStatTest[3])   smartLed1.setPixelColor(3, smartLed1.Color(0,0,200));
  else if (!lightStatTest[2] && !lightStatTest[3])  smartLed1.setPixelColor(3, smartLed1.Color(250,0,0));

  if      (lightStatTest[4] && lightStatTest[5])    smartLed1.setPixelColor(2, smartLed1.Color(200,200,0));
  else if (lightStatTest[4] && !lightStatTest[5])   smartLed1.setPixelColor(2, smartLed1.Color(0,200,0));
  else if (!lightStatTest[4] && lightStatTest[5])   smartLed1.setPixelColor(2, smartLed1.Color(0,0,200));
  else if (!lightStatTest[4] && !lightStatTest[5])  smartLed1.setPixelColor(2, smartLed1.Color(250,0,0));

  if      (lightStatTest[6] && !lightStatTest[7])   smartLed1.setPixelColor(1, smartLed1.Color(0,200,0));
  else if (!lightStatTest[6] && lightStatTest[7])   smartLed1.setPixelColor(1, smartLed1.Color(0,0,200));
  else if (!lightStatTest[6] && !lightStatTest[7])  smartLed1.setPixelColor(1, smartLed1.Color(250,0,0));

  smartLed1.show(); // This sends the updated pixel color to the hardware.
}

void refreshIndicators(){
  //Туалет
  if      (lightStat[0])   smartLed1.setPixelColor(0, smartLed1.Color(0,200,0));
  else if (!lightStat[0])  smartLed1.setPixelColor(0, smartLed1.Color(250,0,0));
  //Кухня
  if      (lightStat[1])   smartLed1.setPixelColor(4, smartLed1.Color(0,200,0));
  else if (!lightStat[1])  smartLed1.setPixelColor(4, smartLed1.Color(250,0,0));
  //Угловой, вытяжка
  if      (lightStat[2] && lightStat[3])    smartLed1.setPixelColor(3, smartLed1.Color(40,30,0));
  else if (lightStat[2] && !lightStat[3])   smartLed1.setPixelColor(3, smartLed1.Color(0,30,0));
  else if (!lightStat[2] && lightStat[3])   smartLed1.setPixelColor(3, smartLed1.Color(0,0,40));
  else if (!lightStat[2] && !lightStat[3])  smartLed1.setPixelColor(3, smartLed1.Color(40,0,0));
  //Ленты
  if      (lightStat[4] && lightStat[5])    smartLed1.setPixelColor(2, smartLed1.Color(40,30,0));
  else if (lightStat[4] && !lightStat[5])   smartLed1.setPixelColor(2, smartLed1.Color(0,30,0));
  else if (!lightStat[4] && lightStat[5])   smartLed1.setPixelColor(2, smartLed1.Color(0,0,40));
  else if (!lightStat[4] && !lightStat[5])  smartLed1.setPixelColor(2, smartLed1.Color(40,0,0));
  //Ванна
  if      (lightStat[6] && !lightStat[7])   smartLed1.setPixelColor(1, smartLed1.Color(0,200,0));
  else if (!lightStat[6] && lightStat[7])   smartLed1.setPixelColor(1, smartLed1.Color(0,0,200));
  else if (!lightStat[6] && !lightStat[7])  smartLed1.setPixelColor(1, smartLed1.Color(250,0,0));

  smartLed1.show(); // This sends the updated pixel color to the hardware.
}

void animation_1 (){
  for (int i=0; i<250; i++){
    smartLed1.setPixelColor(0, smartLed1.Color(250-i, i < 200 ? i : 200, 0));
    delay(3);    
    smartLed1.show();
    i=i+10;
  }
}

void animation_2 (){
  for (int i=0; i<250; i++){
    smartLed1.setPixelColor(0, smartLed1.Color(i, 200 - (i < 200 ? i : 200), 0));
    delay(3);    
    smartLed1.show();
    i=i+10;
  }
}

void switchLights(){
  digitalWrite(lights[0], !lightStat[1]);
  digitalWrite(lights[1], !lightStat[0]);
  digitalWrite(lights[2], !lightStat[6]);
  digitalWrite(lights[3], !lightStat[7]);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void mqtt_loop(){
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
 
     // ... and resubscribe
      //client.subscribe("light/kitchen/corner");
      //client.subscribe("radio");
      client.subscribe("bounce/corr/#");
      client.subscribe("light/test/#");

      client.subscribe("corridor/test_mode");
      
      client.subscribe("light/toilet/1");
      client.subscribe("light/bath/#");

      client.subscribe("light/kitchen/#");

      client.subscribe("strip/kitchen/setPreset");

      client.subscribe("test/animation");

      
      

      
      
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();
//*********************************************************************************************************************************
  if (strcmp(topic,"corridor/test_mode")==0){
    testMode = call.toInt();
  }

//==================================================================================================================
//TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  
//==================================================================================================================
  //*********************************************************************************************************************************
  if (strcmp(topic,"light/test/1")==0){
    if(call=="-1") {
      lightStatTest[0] = !lightStatTest[0];
      snprintf (msg, 75, "%d", lightStatTest[0]);
      client.publish("light/test/1", msg, true);
    }
    else lightStatTest[0] = call.toInt();
    refreshTestIndicators();
  }
  //*********************************************************************************************************************************
  if (strcmp(topic,"light/test/2")==0){
    if(call=="-1") {
      lightStatTest[1] = !lightStatTest[1];
      snprintf (msg, 75, "%d", lightStatTest[1]);
      client.publish("light/test/2", msg, true);
    }
    else lightStatTest[1] = call.toInt();
    refreshTestIndicators();
  }
  //*********************************************************************************************************************************
  if (strcmp(topic,"light/test/3")==0){
    if(call=="-1") {
      lightStatTest[2] = !lightStatTest[2];
      snprintf (msg, 75, "%d", lightStatTest[2]);
      client.publish("light/test/3", msg, true);
    }
    else lightStatTest[2] = call.toInt();
    refreshTestIndicators();
  }
  //*********************************************************************************************************************************
  if (strcmp(topic,"light/test/4")==0){
    if(call=="-1") {
      lightStatTest[3] = !lightStatTest[3];
      snprintf (msg, 75, "%d", lightStatTest[3]);
      client.publish("light/test/4", msg, true);
    }
    else lightStatTest[3] = call.toInt();
    refreshTestIndicators();
  }
  //*********************************************************************************************************************************
  if (strcmp(topic,"light/test/5")==0){
    if(call=="-1") {
      lightStatTest[4] = !lightStatTest[4];
      snprintf (msg, 75, "%d", lightStatTest[4]);
      client.publish("light/test/5", msg, true);
    }
    else lightStatTest[4] = call.toInt();
    refreshTestIndicators();
  }
  //*********************************************************************************************************************************
  if (strcmp(topic,"light/test/6")==0){
    if(call=="-1") {
      lightStatTest[5] = !lightStatTest[5];
      snprintf (msg, 75, "%d", lightStatTest[5]);
      client.publish("light/test/6", msg, true);
    }
    else lightStatTest[5] = call.toInt();
    refreshTestIndicators();
  }
  //*********************************************************************************************************************************
  if (strcmp(topic,"light/test/7")==0){
    if(call=="-1") {
      lightStatTest[6] = !lightStatTest[6];
      snprintf (msg, 75, "%d", lightStatTest[6]);
      client.publish("light/test/7", msg, true);
    }
    else {
      lightStatTest[6] = call.toInt();
      if (call.toInt() == 1){
        lightStatTest[7] = false;
        client.publish("light/test/8", "0", true);        
      }
    }
    refreshTestIndicators();
  }
  //*********************************************************************************************************************************
  if (strcmp(topic,"light/test/8")==0){
    if(call=="-1") {
      lightStatTest[7] = !lightStatTest[7];
      snprintf (msg, 75, "%d", lightStatTest[7]);
      client.publish("light/test/8", msg, true);
    }
    else {
      lightStatTest[7] = call.toInt();
      if (call.toInt() == 1){
        lightStatTest[6] = false;
        client.publish("light/test/7", "0", true);
      }
    }
    refreshTestIndicators();
  }
  
  if (strcmp(topic,"test/animation")==0){
    if(call=="1") animation_1 ();
    if(call=="2") animation_2 ();
  }

//==================================================================================================================
//REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  
//==================================================================================================================
  
  if (strcmp(topic,"light/toilet/1")==0){
    if(call=="-1" || call=="2") {
      lightStat[0] = !lightStat[0];
      snprintf (msg, 75, "%d", lightStat[0]);
      client.publish("light/toilet/1", msg, true);
    }
    else {
      lightStat[0] = call.toInt();    
      if (lightStat[0]) animation_1();
      else              animation_2();
    }
    refreshIndicators();
    switchLights();
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/kitchen/main")==0){
    if(call=="-1" || call=="2") {
      lightStat[1] = !lightStat[1];
      snprintf (msg, 75, "%d", lightStat[1]);
      client.publish("light/kitchen/main", msg, true);
    }
    else lightStat[1] = call.toInt();
    refreshIndicators();
    switchLights();    
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/kitchen/corner")==0){
    if(call=="1" || call=="0") {
      lightStat[2] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/kitchen/coohoo")==0){
    if(call=="1" || call=="0") {
      lightStat[3] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------

  if (strcmp(topic,"strip/kitchen/setPreset")==0){
    if(call.toInt()==0) lightStat[4] = false;
    if(call.toInt()>0)  lightStat[4] = true;
    refreshIndicators();
  }//-----------------------------------------------

  if (strcmp(topic,"light/kitchen/led_strip2")==0){
    if(call=="1" || call=="0") {
      lightStat[5] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------

  if (strcmp(topic,"light/bath/1")==0){
    if(call=="-1") {
      lightStat[6] = !lightStat[6];
      snprintf (msg, 75, "%d", lightStat[6]);
      client.publish("light/bath/1", msg, true);
    }
    else {
      lightStat[6] = call.toInt();
      if (call.toInt() == 1){
        lightStat[7] = false;
        client.publish("light/bath/2", "0", true);        
      }
    }
    refreshIndicators();
    switchLights();    
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/bath/2")==0){
    if(call=="-1") {
      lightStat[7] = !lightStat[7];
      snprintf (msg, 75, "%d", lightStat[7]);
      client.publish("light/bath/2", msg, true);
    }
    else {
      lightStat[7] = call.toInt();
      if (call.toInt() == 1){
        lightStat[6] = false;
        client.publish("light/bath/1", "0", true);
      }
    }
    refreshIndicators();
    switchLights();    
  }//-----------------------------------------------














//*********************************************************************************************************************************
  if (strcmp(topic,"bounce/corr/startPublish")==0){
    if(call=="1") bounceStartPublish = true;
    else if(call=="0") bounceStartPublish = false;
  }
//*********************************************************************************************************************************
  if (strcmp(topic,"bounce/corr/startCount")==0){
    if(call=="1") bounceStartCount = true;
    else if(call=="0") {
      bounceCount[0] = 0;
      bounceCount[1] = 0;
      bounceCount[2] = 0;
      bounceCount[3] = 0;
      bounceCount[4] = 0;
      bounceStartCount = false;
    }
  }
  //*********
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
