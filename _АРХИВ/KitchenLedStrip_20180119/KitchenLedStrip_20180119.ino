#include <ArduinoSort.h>

/* Node MCU D1 mini
Pin  Function  ESP-8266 Pin
TX  TXD TXD
RX  RXD RXD
A0  Analog input, max 3.3V input  A0
D0  IO  GPIO16
D1  IO, SCL GPIO5
D2  IO, SDA GPIO4
D3  IO, 10k Pull-up GPIO0
D4  IO, 10k Pull-up, BUILTIN_LED  GPIO2
D5  IO, SCK GPIO14
D6  IO, MISO  GPIO12
D7  IO, MOSI  GPIO13
D8  IO, 10k Pull-down, SS GPIO15 (Нельзя делать выходом, не стартует)
G Ground  GND
5V  5V  -
3V3 3.3V  3.3V
RST Reset RST


some_string.toInt()
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <Adafruit_NeoPixel.h>
/*#ifdef __AVR__
  #include <avr/power.h>
#endif
*/

#define PIN            12
#define BOARD_NAME "NodeMCU_D1mini_NEW_LED_STRIP"

const char* ssid = "ScorpGOS";
const char* password = "9031124945";

ESP8266WebServer server(80);

int leds[] = {2, 4, 5};

Adafruit_NeoPixel strip = Adafruit_NeoPixel(217, PIN, NEO_GRBW + NEO_KHZ800);
boolean switchedStrip = false;

int mLeds[217];

String queryArgs{};
