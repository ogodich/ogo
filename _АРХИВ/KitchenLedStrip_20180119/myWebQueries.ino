void myWebQueries (){
  server.on("/inline", [](){
    String message = "Hello, Oleg,";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET)?"GET":"POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i=0; i<server.args(); i++){
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(200, "text/plain", message);
    Serial.println(message);
  });

//*****************************************************************************************************
  server.on("/myLeds", [](){
    String message = "myLeds";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i=0; i<server.args(); i++){
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(200, "text/plain", message);

  myLeds(server.arg(0).toInt(), server.arg(1).toInt(), server.arg(2).toInt());
  });

//*****************************************************************************************************
  server.on("/ledStrip", [](){
    String message = "myLeds";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i=0; i<server.args(); i++){
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }

    int k1 = 100;
    if (server.argName(0) == "pow" && server.arg(0).toInt() > 0 && server.arg(0).toInt() <= 100) k1 = server.arg(0).toInt();
    
    if (switchedStrip)  testLedStrip(0, 0, 0, 0);
    else                testLedStrip(130/100*k1, 30/100*k1, 10/100*k1, 200/100*k1); //testLedStrip(130, 30, 10, 200); 

    switchedStrip = !switchedStrip;

    //Включаем мотивационные светодиоды для Насти
    showMotivation();
    
    message += "\nswitchedStrip: ";
    if (switchedStrip) message += "switched ON";
    else               message += "switched OFF";
    message += "\n";            
    


    server.send(200, "text/plain", message);
    
  });

//*****************************************************************************************************
  server.on("/anyLeds", [](){
    String message = "anyLeds";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i=0; i<server.args(); i++){
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
//      queryNameArgs[i]=server.argName(i);
//      queryArgs[i]=server.arg(i);
    }
    message += anyLeds();
    server.send(200, "text/plain", message);

    
    
  });


//*****************************************************************************************************
  server.on("/motivation", [](){
    String message="<style>.my_button1 {width: 300px;height: 200px; background-color: blue; font-size: 40pt;}.my_button2 {width: 300px;height: 200px; background-color: GREEN; font-size: 40pt;}.my_button3 {width: 300px;height: 200px; background-color: RED; font-size: 40pt;}</style>";
    message+="<form action='/' method='post'><input type='hidden' name='add' value='4' /><p><input type='submit' class='my_button1' value='BLUE + '></p></form>";
    message+="<form action='/' method='post'><input type='hidden' name='remove' value='4' /><p><input type='submit' class='my_button1' value='BLUE - '></p></form>";
    message+="<BR>";
    message+="<form action='/' method='post'><input type='hidden' name='add' value='3' /><p><input type='submit' class='my_button2' value='GREEN + '></p></form>";
    message+="<form action='/' method='post'><input type='hidden' name='remove' value='3' /><p><input type='submit' class='my_button2' value='GREEN - '></p></form>";
    message+="<BR>";
    message+="<form action='/' method='post'><input type='hidden' name='add' value='2' /><p><input type='submit' class='my_button3' value='RED + '></p></form>";
    message+="<form action='/' method='post'><input type='hidden' name='remove' value='2' /><p><input type='submit' class='my_button3' value='RED - '></p></form></html>";

    motivation(server.argName(0), server.arg(0).toInt());

    server.send(200, "html", message);
  });
//*****************************************************************************************************
  server.on("/motivationManager", [](){
    String myResp = "<html><p>BLUE:";
    myResp += "<input type=button value=+ onclick=window.location.href='http://192.168.1.111/motivation?add=4' />";
    myResp += "<input type=button value=- onclick=window.location.href='http://192.168.1.111/motivation?remove=4' /></p>";
    myResp += "<p>GREEN:<input type=button value=+ onclick=window.location.href='http://192.168.1.111/motivation?add=3' />";
    myResp += "<input type=button value=- onclick=window.location.href='http://192.168.1.111/motivation?remove=3' /></p>";
    myResp += "<p>RED:<input type=button value=+ onclick=window.location.href='http://192.168.1.111/motivation?add=2' />";
    myResp += "<input type=button value=- onclick=window.location.href='http://192.168.1.111/motivation?remove=2' /></p></html>";

    server.send(200, "html", myResp);
  });


}
//*****************************************************************************************************

void myLeds(int r, int g, int b){
  analogWrite(leds[0], r);
  analogWrite(leds[1], g);
  analogWrite(leds[2], b);
}

void testLedStrip(int r, int g, int b, int w) {
  for(int i=0;i<217;i++){
    strip.setPixelColor(i, strip.Color(r,g,b,w));    
  }
  strip.show(); // This sends the updated pixel color to the hardware.
}

//*****************************************************************************************************

String motivation(String oper, int col) {
  for(int i=0; i<217; i++){
    //Прибавляем один светодиод выбранного цвета
    if(oper=="add" && mLeds[i]==0){
      mLeds[i]=col;
      break;
    }
    //Отнимаем последний светоидод выбранного цвета
    if(oper=="remove" && mLeds[i]==col){
      mLeds[i]=1; //Присваиваем единицу. Ноль нельзя, чтобы не трогать остальную ленту
      break;
    }  
  }

  //Сортируем: зелёные - красные - пустые
  sortArrayReverse(mLeds, 216);

  //Отчет, отладка
  String myResp = "";
  for(int i=0; i<217; i++){
    if(mLeds[i]>0){
      myResp += i;    
      myResp += "=";
      myResp += mLeds[i];
      myResp += ",";
    }
  }

  /*myResp += "<html><p>BLUE:";
  myResp += "<input type=button value=+ onclick=window.location.href='http://192.168.1.111/motivation?add=4' />";
  myResp += "<input type=button value=- onclick=window.location.href='http://192.168.1.111/motivation?remove=4' /></p>";
  myResp += "<p>GREEN:<input type=button value=+ onclick=window.location.href='http://192.168.1.111/motivation?add=3' />";
  myResp += "<input type=button value=- onclick=window.location.href='http://192.168.1.111/motivation?remove=4' /></p>";
  myResp += "<p>RED:<input type=button value=+ onclick=window.location.href='http://192.168.1.111/motivation?add=3' />";
  myResp += "<input type=button value=- onclick=window.location.href='http://192.168.1.111/motivation?remove=4' /></p></html>";
  */
  //Непосредственно зажигаем светодиоды
  showMotivation();

  //Превращаем единицы в нули, чтобы освободить нежужные ячейки массива
  for(int i=0; i<217; i++){
    if(mLeds[i]==1) mLeds[i]=0;
  }
  return myResp;
}

void showMotivation(){
  for(int i=0; i<217; i++){
    if (mLeds[i]==4)                strip.setPixelColor(216-i, strip.Color(0,0,110,0));
    else if(mLeds[i]==3){
      if(i%2!=0 || mLeds[i+1]==0)   strip.setPixelColor(216-i, strip.Color(0,60,0,0));
      else                          strip.setPixelColor(216-i, strip.Color(20,60,0,20));
    }
    else if(mLeds[i]==2)            strip.setPixelColor(216-i, strip.Color(110,0,0,0));
    else if(mLeds[i]==1)            strip.setPixelColor(216-i, strip.Color(0,0,0,0));
  }
  strip.show();
}
//*****************************************************************************************************
String anyLeds(){
  String mess="\nanyLeds says:";
  mess+="\nserver.argName(0)=";
  mess+=server.argName(0);
  mess+="\nserver.arg(0)=";
  mess+=server.arg(0);
  if(server.args() > 4) mess+="\nserver.args() > 4=";
  if(server.args() > 4) mess+= "yes";
  mess+="\n1%2=";
  mess+= 1%2;
  mess+="\n2%2=";
  mess+= 2%2;
  mess+="\n3%2=";
  mess+= 3%2;
  mess+="\n4%2=";
  mess+= 4%2;

  


  if (server.args() > 4){
    strip.setPixelColor(server.arg(0).toInt(), strip.Color(server.arg(1).toInt(),
                                                           server.arg(2).toInt(),
                                                           server.arg(3).toInt(),
                                                           server.arg(4).toInt()
                                                           )
                       );
    strip.show();
  }

  return mess;  
}

