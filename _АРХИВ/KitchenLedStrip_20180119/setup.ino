

void setup(void){
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  wifiPrint();
  myWebQueries();
  
  server.begin();
  Serial.println("HTTP server started");

  otaInit();

  pinMode(leds[0], OUTPUT);
  pinMode(leds[1], OUTPUT);
  pinMode(leds[2], OUTPUT);

  strip.begin(); // This initializes the NeoPixel library.
  strip.setBrightness(255); // set brightness
  strip.show(); // Initialize all pixels to 'off'

}
