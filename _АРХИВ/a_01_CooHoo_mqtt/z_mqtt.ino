void mqtt_loop(){
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  /*long now = millis();
  if (now - lastMsg > 10000) {
    lastMsg = now;
    ++value;
    snprintf (msg, 75, "hello world #%ld", value);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("outTopic", msg);
  } */ 
}

boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();

  /*if (String((char*)topic) == "radio"){
    if ((char)payload[0] == '1') radioStatus = 1;
    else if ((char)payload[0] == '0') radioStatus = 0;
  }*/

//*********************************************************************************************************************************
  if (strcmp(topic,"light/kitchen/coohoo")==0){
    if(call=="2") {
        lightStat[0] = !lightStat[0];
        snprintf (msg, 75, "%d", lightStat[0]);
        client.publish("light/kitchen/coohoo", msg, true);
    }
    else {
      lightStat[0] = (call.toInt());
      digitalWrite(lights[0], !lightStat[0]);
    }
  }
  //*********
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "reconnecting");
      // ... and resubscribe
      client.subscribe("light/kitchen/coohoo");
      client.subscribe("radio");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
