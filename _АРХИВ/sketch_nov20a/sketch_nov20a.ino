int pin = 13;
boolean start = false;
 
void setup()
{
  pinMode(pin, INPUT);
  attachInterrupt(0, myInterrupt, RISING);
  Serial.begin(115200);
}
 
void loop()
{
  Serial.print("loop - ");

  if(digitalRead(pin)) start = true;
  if (start) allways();

  Serial.println(millis());
  delay(1000);

}

void allways(){
  while(start){
    Serial.print("allways - ");
    Serial.println(millis());
    delay(1000);
  }
}

void myInterrupt()
{
    Serial.println("myInterrupt");
    start = false;
}
