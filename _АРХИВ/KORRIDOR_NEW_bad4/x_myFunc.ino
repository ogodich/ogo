void my_booting() {
  if (startTime > millis()) {
    ArduinoOTA.handle();
  }
  else {
    startBoot = false;
  }
}

void check_bounce(){
  if (bounceStartPublish){
    long now = millis();
    if (now - bounceTimer > 3000) {
      bounceTimer = now;
      snprintf (msg, 75, "1=%d, 2=%d, 3=%d, 4=%d, 5=%d", bounceCount[0], bounceCount[1], bounceCount[2], bounceCount[3], bounceCount[4]);
      Serial.println(msg);
      client.publish("bounce/corr/count", msg);
    }
  }
}

void refreshTestIndicators(){
}

void refreshIndicators(){
  //Туалет
  if      (lightStat[0])   smartLed1.setPixelColor(0, smartLed1.Color(0,200,0));
  else if (!lightStat[0])  smartLed1.setPixelColor(0, smartLed1.Color(250,0,0));
  //Кухня
  if      (lightStat[1])   smartLed1.setPixelColor(4, smartLed1.Color(0,200,0));
  else if (!lightStat[1])  smartLed1.setPixelColor(4, smartLed1.Color(250,0,0));
  //Угловой, вытяжка
  if      (lightStat[2] && lightStat[3])    smartLed1.setPixelColor(3, smartLed1.Color(40,30,0));
  else if (lightStat[2] && !lightStat[3])   smartLed1.setPixelColor(3, smartLed1.Color(0,30,0));
  else if (!lightStat[2] && lightStat[3])   smartLed1.setPixelColor(3, smartLed1.Color(0,0,40));
  else if (!lightStat[2] && !lightStat[3])  smartLed1.setPixelColor(3, smartLed1.Color(40,0,0));
  //Ленты
  if      (lightStat[4] && lightStat[5])    smartLed1.setPixelColor(2, smartLed1.Color(40,30,0));
  else if (lightStat[4] && !lightStat[5])   smartLed1.setPixelColor(2, smartLed1.Color(0,30,0));
  else if (!lightStat[4] && lightStat[5])   smartLed1.setPixelColor(2, smartLed1.Color(0,0,40));
  else if (!lightStat[4] && !lightStat[5])  smartLed1.setPixelColor(2, smartLed1.Color(40,0,0));
  //Ванна
  if      (lightStat[6] && !lightStat[7])   smartLed1.setPixelColor(1, smartLed1.Color(0,200,0));
  else if (!lightStat[6] && lightStat[7])   smartLed1.setPixelColor(1, smartLed1.Color(0,0,200));
  else if (!lightStat[6] && !lightStat[7])  smartLed1.setPixelColor(1, smartLed1.Color(250,0,0));

  smartLed1.show(); // This sends the updated pixel color to the hardware.
}

void animation_1 (){
  for (int i=0; i<250; i++){
    smartLed1.setPixelColor(0, smartLed1.Color(250-i, i < 200 ? i : 200, 0));
    delay(3);    
    smartLed1.show();
    i=i+10;
  }
}

void animation_2 (){
  for (int i=0; i<250; i++){
    smartLed1.setPixelColor(0, smartLed1.Color(i, 200 - (i < 200 ? i : 200), 0));
    delay(3);    
    smartLed1.show();
    i=i+10;
  }
}

void switchLights(){
  digitalWrite(lights[0], !lightStat[1]);
  digitalWrite(lights[1], !lightStat[0]);
  digitalWrite(lights[2], !lightStat[6]);
  digitalWrite(lights[3], !lightStat[7]);

}
