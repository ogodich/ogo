void mqtt_loop(){
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
 
     // ... and resubscribe
      //client.subscribe("light/kitchen/corner");
      //client.subscribe("radio");
      client.subscribe("bounce/corr/#");
      client.subscribe("light/test/#");

      client.subscribe("corridor/test_mode");
      
      client.subscribe("light/toilet/1");
      client.subscribe("light/bath/#");

      client.subscribe("light/kitchen/#");

      client.subscribe("strip/kitchen/setPreset");

      client.subscribe("test/animation");

      
      

      
      
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//boolean var = false;
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();
//*********************************************************************************************************************************
  if (strcmp(topic,"corridor/test_mode")==0){
    testMode = call.toInt();
  }

//==================================================================================================================
//TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  
//==================================================================================================================


//==================================================================================================================
//REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  
//==================================================================================================================
  
  if (strcmp(topic,"light/toilet/1")==0){
    if(call=="-1" || call=="2") {
      lightStat[0] = !lightStat[0];
      snprintf (msg, 75, "%d", lightStat[0]);
      client.publish("light/toilet/1", msg, true);
    }
    else {
      lightStat[0] = call.toInt();    
      if (lightStat[0]) animation_1();
      else              animation_2();
    }
    refreshIndicators();
    switchLights();
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/kitchen/main")==0){
    if(call=="-1" || call=="2") {
      lightStat[1] = !lightStat[1];
      snprintf (msg, 75, "%d", lightStat[1]);
      client.publish("light/kitchen/main", msg, true);
    }
    else lightStat[1] = call.toInt();
    refreshIndicators();
    switchLights();    
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/kitchen/corner")==0){
    if(call=="1" || call=="0") {
      lightStat[2] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/kitchen/coohoo")==0){
    if(call=="1" || call=="0") {
      lightStat[3] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------

  if (strcmp(topic,"strip/kitchen/setPreset")==0){
    if(call.toInt()==0) lightStat[4] = false;
    if(call.toInt()>0)  lightStat[4] = true;
    refreshIndicators();
  }//-----------------------------------------------

  if (strcmp(topic,"light/kitchen/led_strip2")==0){
    if(call=="1" || call=="0") {
      lightStat[5] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------

  if (strcmp(topic,"light/bath/1")==0){
    if(call=="-1") {
      lightStat[6] = !lightStat[6];
      snprintf (msg, 75, "%d", lightStat[6]);
      client.publish("light/bath/1", msg, true);
    }
    else {
      lightStat[6] = call.toInt();
      if (call.toInt() == 1){
        lightStat[7] = false;
        client.publish("light/bath/2", "0", true);        
      }
    }
    refreshIndicators();
    switchLights();    
  }//-----------------------------------------------
  
  if (strcmp(topic,"light/bath/2")==0){
    if(call=="-1") {
      lightStat[7] = !lightStat[7];
      snprintf (msg, 75, "%d", lightStat[7]);
      client.publish("light/bath/2", msg, true);
    }
    else {
      lightStat[7] = call.toInt();
      if (call.toInt() == 1){
        lightStat[6] = false;
        client.publish("light/bath/1", "0", true);
      }
    }
    refreshIndicators();
    switchLights();    
  }//-----------------------------------------------














//*********************************************************************************************************************************
  if (strcmp(topic,"bounce/corr/startPublish")==0){
    if(call=="1") bounceStartPublish = true;
    else if(call=="0") bounceStartPublish = false;
  }
//*********************************************************************************************************************************
  if (strcmp(topic,"bounce/corr/startCount")==0){
    if(call=="1") bounceStartCount = true;
    else if(call=="0") {
      bounceCount[0] = 0;
      bounceCount[1] = 0;
      bounceCount[2] = 0;
      bounceCount[3] = 0;
      bounceCount[4] = 0;
      bounceStartCount = false;
    }
  }
  //*********
}
