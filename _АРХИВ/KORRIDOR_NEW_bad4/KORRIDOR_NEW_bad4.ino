/*
static const uint8_t D0   = 16; * - кнопка
static const uint8_t D1   = 5;  * - кнопка
static const uint8_t D2   = 4;  * - кнопка
static const uint8_t D3   = 0;  - - РЕЛЕ?             - не делать входом*
static const uint8_t D4   = 2;  * - не делать входом
static const uint8_t D5   = 14; * - кнопка
static const uint8_t D6   = 12; * - реле
static const uint8_t D7   = 13;   - светодиоды
static const uint8_t D8   = 15;   - кнопка
static const uint8_t D9   = 3;  * - реле
static const uint8_t D10  = 1;  * - долго весело реле, но случилась проблема
                     SDD3 = 10  * - реле
                     SDD2 = 9
*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif


#define BOARD_NAME "CORRIDOR_NEW_1"

#define SMART_LED_1            13


/*_______________________________________________________________________________________________________________________________*/
const char* ssid = "ScorpGOS";
const char* password = "9031124945";

boolean startBoot = false;
double startTime = 0;

//int lights[4] = {12,3,1,10};
//int lights[4] = {D6,D9,1,10};
int lights[4] = {D6,D9,10,D3};


int buttons[5] = {D0, D2, D1, D5, D8};
boolean lightStatTest[8] = {false, false, false, false, false, false, false, false};
boolean lightStat[8] = {false, false, false, false, false, false, false, false};

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
//int value = 0;

boolean bounceStartPublish = false;
boolean bounceStartCount = false;
int bounceCount[5];
long bounceTimer = 0.0;

Adafruit_NeoPixel smartLed1 = Adafruit_NeoPixel(5, SMART_LED_1, NEO_GRB + NEO_KHZ800);

boolean testMode = false;
