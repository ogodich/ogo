int S1 = 0;
int S2 = 0;
int S3 = 0;
int S4 = 0;
int S5 = 0;

int L1 = 0;
int L2 = 0;
int L3 = 0;
int L4 = 0;
int L5 = 0;



//String debug = "";

void my_multy_switcher(){
  multyButtons();


//==================================================================================================================
//TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  --  TEST  
//==================================================================================================================
  if (testMode){
    }

//==================================================================================================================
//REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  
//==================================================================================================================
  //КОМБИНАЦИИ-------------------------------------------------------------------------------------------------

  //Включаем РАДИО
  else if (L3==1 && S2==1)  {
    client.publish("radio", "2");
  }

  //Понижаем громкость, если зажата 2 и нажата больше одного 1
  else if (L3==1 && S2>1)  {
    snprintf (msg, 5, "%d", (S2-1)*(-1));
    client.publish("volume_change", msg);
  }
  //Повышаем громкость, если зажата 2 и нажата больше одного 3
  else if (L3==1 && S4>1)  {
    snprintf (msg, 5, "%d", S4-1);
    client.publish("volume_change", msg);
  }
  
  //ЛЕНТА. Кухня. Профиль 2
  else if (L1==1 && S4==2) client.publish("strip/kitchen/setPreset", "2");
  //ЛЕНТА. Кухня. Профиль 3
  else if (L1==1 && S4==3) client.publish("strip/kitchen/setPreset", "3");
  //ЛЕНТА. Кухня. Профиль 4
  else if (L1==1 && S4==4) client.publish("strip/kitchen/setPreset", "4");
  //ЛЕНТА. Прихожая. Профиль 1 / ВЫКЛ
  else if (L5==1 && L4==1) client.publish("light/hall_strip", "2");
  //ЛЕНТА. Прихожая. Профиль 2
  else if (L5==1 && S4==2) client.publish("strip/hall/setPreset", "2");
  //ЛЕНТА. Прихожая. Профиль 3
  else if (L5==1 && S4==3) client.publish("strip/hall/setPreset", "3");

  //Умная розетка 1 (гирлянда на кухне)
  else if (L1==1 && L2==1) client.publish("remote_socket/1", "-1");

  //ПЕРВАЯ кнопка-------------------------------------------------------------------------------------------------  
  else if (S1==1) client.publish("light/toilet/1", "2");

  //ВТОРАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (S2==1) {
    client.publish("light/kitchen/main", "-1");
    delay(300);
    client.publish("strip/kitchen/specificLedOn", "{'led':'181','red':'180','green':'0','blue':'0','white':'0'}");
  }
    
  //ТРЕТЬЯ кнопка-------------------------------------------------------------------------------------------------
  else if (S3==1) client.publish("light/kitchen/corner", "2");
  else if (L3==1) client.publish("light/kitchen/coohoo", "2");

  //ЧЕТВЕРТАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (S4==1) {
    client.publish("light/kitchen/led_strip", "2");
    delay(300);
    client.publish("strip/kitchen/specificLedOn", "{'led':'181','red':'180','green':'0','blue':'0','white':'0'}");
  }
  else if (L4==1) client.publish("light/kitchen/led_strip2", "-1");

  //ПЯТАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (S5==1) {
    if(lightStat[7]) client.publish("light/bath/2", "0", true);
    else               client.publish("light/bath/1", "-1");
  }
  else if (L5==1) client.publish("light/bath/2", "-1");   
  

}//----------



/******************************************************************************************************************/
void multyButtons() {
  int i = 0;
  int y = 0;
  int z = 0;
  int j = 0;
  int k = 0;

  int ii = 0;
  int yy = 0;
  int zz = 0;
  int jj = 0;
  int kk = 0;


  S1 = 0;
  S2 = 0;
  S3 = 0;
  S4 = 0;
  S5 = 0;

  L1 = 0;
  L2 = 0;
  L3 = 0;
  L4 = 0;
  L5 = 0;

  

  /************************************************************************/
  
  while (digitalRead(buttons[0]) || i>0 || digitalRead(buttons[1]) || y>0 || digitalRead(buttons[2]) || z>0 || digitalRead(buttons[3]) || j>0 || digitalRead(buttons[4]) || k>0){
    if ((digitalRead(buttons[0]))){
      if(bounceStartCount) bounceCount[0] ++;
      i++;
    }
    else {
      if(i > 20){
        L1++;
      }
      else if(i > 2){
        S1++;
      }      
      i=0;    
    }

    if ((digitalRead(buttons[1]))) {
      if(bounceStartCount) bounceCount[1] ++;
      y++;
    }
    else {
      if(y > 20){
        L2++;
      }
      else if(y > 2){
        S2++;
      }
      y=0;    
    }

    if ((digitalRead(buttons[2]))) {
      if(bounceStartCount) bounceCount[2] ++;
      z++;
    }
    else {
      if(z > 20){
        L3++;
      }
      else if(z > 2){
        S3++;
      }
      z=0;    
    }

    if ((digitalRead(buttons[3]))) {
      if(bounceStartCount) bounceCount[3] ++;
      j++;
    }
    else {
      if(j > 20){
        L4++;
      }
      else if(j > 2){
        S4++;
      }
      j=0;    
    }

    if ((digitalRead(buttons[4]))) {
      if(bounceStartCount) bounceCount[4] ++;
      k++;
    }
    else {
      if(k > 20){
        L5++;
      }
      else if(k > 2){
        S5++;
      }
      k=0;    
    }
    
  //**************  
    delay(20);
  }
  /************************************************************************/

}
