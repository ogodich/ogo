#define LIGHT_FIRST    5
#define LIGHT_SECOND   6
#define BUTTON_PIN_1   7
//#define BUTTON_PIN_2   8
#define IR_RM_PIN      11

#include <myFirstClass.h>
#include <IRremote.h>

myFirstClass firstButton(BUTTON_PIN_1, true);     // создаём экземпляр нашего класса
//myFirstClass secondButton(BUTTON_PIN_2, true);     // создаём экземпляр нашего класса

IRrecv irrecv(IR_RM_PIN);
decode_results results;

//int lightsMap[] = {0, 0};
int curMode = 0;


void setup() {
  pinMode(LIGHT_FIRST,  OUTPUT);
  pinMode(LIGHT_SECOND, OUTPUT);
  pinMode(BUTTON_PIN_1, INPUT);
//  pinMode(BUTTON_PIN_2, INPUT);

  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
}

//boolean switchLight = false;
int brightnessLevel = 255;
void loop() {
  long myRes;
  if (irrecv.decode(&results)) {
    myRes = (results.value);
    //Serial.println(results.value, HEX);
    Serial.println(myRes);
    irrecv.resume(); // Receive the next value
  }

  //ПЕРЕКЛЮЧЕНИЕ ПРОГРАММЫ КНОПКОЙ
  if (firstButton.funkMyClickCheck() == "short" /*|| secondButton.funkMyClickCheck() == "short"*/) {
    curMode++;
    curMode=curMode%3;
    brightnessLevel = 255;
  }
  //ПУЛЬТ - ВЫКЛ
  else if (myRes == -189126264) curMode=0;
  //ПУЛЬТ - ТУСКЛЕЕ
  else if (myRes == -2085514394)  {
    brightnessLevel-=20;
    if (brightnessLevel < 0) brightnessLevel = 0;
  }
  //ПУЛЬТ - ЯРЧЕ
  else if (myRes == 1752382022)  {
    brightnessLevel+=20;
    if (brightnessLevel > 255) brightnessLevel = 255;
  }
  //ПУЛЬТ - СЛЕДУЮЩАЯ ПРОГРАММА
  else if (myRes == 1595074756)  {
    curMode++;
    curMode=curMode%3;
    brightnessLevel = 255;
  }
  //ПУЛЬТ - ПРЕДЫДУЩАЯ ПРОГРАММА
  else if (myRes == 412973352)  {
    curMode--;
    if (curMode < 0) curMode = 3;
    brightnessLevel = 255;
  }



//Serial.println(brightnessLevel);


  if (curMode == 0) {
    analogWrite(LIGHT_FIRST,  0); 
    analogWrite(LIGHT_SECOND, 0);
  }
  else if (curMode == 1) {
    analogWrite(LIGHT_FIRST,  brightnessLevel); 
    analogWrite(LIGHT_SECOND, brightnessLevel);
  }
  else if (curMode == 2) {
    analogWrite(LIGHT_FIRST,  brightnessLevel); 
    analogWrite(LIGHT_SECOND, 0);
  }
  /*else if (curMode == 3) {
    analogWrite(LIGHT_FIRST,  0); 
    analogWrite(LIGHT_SECOND, 255);
  }*/

//delay(100);
}
