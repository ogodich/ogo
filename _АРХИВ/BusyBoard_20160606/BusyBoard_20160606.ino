//Подключил к доске плеер
#include <SoftwareSerial.h>
#include <DFPlayer_Mini_Mp3.h>

#define LED_PIN_1 2
#define LED_PIN_2 3
#define LED_PIN_3 4
#define LED_PIN_4 5
#define LED_PIN_5 6
#define LED_PIN_6 7
#define CONTACT_PIN_1 8
#define CONTACT_PIN_2 9
#define CONTACT_PIN_3 10
#define CONTACT_PIN_4 11
#define CONTACT_PIN_5 12
#define CONTACT_PIN_6 13
#define PLAYER_STATE A0

#define CHANGING_TIME_OUT 130
#define PAIRS_NUMBER 6

int currChannel = 0;
int prevChannel = 0;

int leds[PAIRS_NUMBER] = {2, 3, 4, 5, 6, 7};
int contacts[PAIRS_NUMBER] = {8, 9, 10
, 11, 12, 13};

void setup() {
  Serial.begin(9600);
  pinMode(LED_PIN_1, OUTPUT);
  pinMode(LED_PIN_2, OUTPUT);
  pinMode(LED_PIN_3, OUTPUT);
  pinMode(LED_PIN_4, OUTPUT);
  pinMode(LED_PIN_5, OUTPUT); 
  pinMode(LED_PIN_6, OUTPUT);   
  pinMode(CONTACT_PIN_1, INPUT_PULLUP);
  pinMode(CONTACT_PIN_2, INPUT_PULLUP);
  pinMode(CONTACT_PIN_3, INPUT_PULLUP);
  pinMode(CONTACT_PIN_4, INPUT_PULLUP);
  pinMode(CONTACT_PIN_5, INPUT_PULLUP);
  pinMode(CONTACT_PIN_6, INPUT_PULLUP);  
  pinMode(PLAYER_STATE, INPUT);

  mp3_set_serial (Serial);      //set Serial for DFPlayer-mini mp3 module 
  delay(1);                     // delay 1ms to set volume
  mp3_set_volume (30);          // value 0~30

}

boolean isContact  = false;
boolean wasContact = false;
boolean isPlaying  = false;
boolean wasPlaying = false;
void loop()
{
  
  
  //Определяем включенный контакт без учета дребезга
  for (int i=0; i<PAIRS_NUMBER; i++) {
    isContact = digitalRead(contacts[i]) == LOW;
    if (isContact) {
      currChannel = i;
      break;
    }
  }
  
  //Если контакта нет, зажигаем все лампочки (много раз)
  if (!isContact){
    for (int i=0; i<PAIRS_NUMBER; i++) {
      digitalWrite(leds[i], HIGH);
    }
  }
  
  //Если есть контакт, а раньше не было, боремся с дребезгом
  if (isContact && !wasContact) {
    delay(100);
    isContact = digitalRead(contacts[currChannel]) == LOW;
  }
  
  //Если всё нормально, гасим все лампочки, кроме текущего и играем (один раз)
  if (isContact && !wasContact) {
    for (int i=0; i<PAIRS_NUMBER; i++) {
      if (i!=currChannel) digitalWrite(leds[i], LOW);
    }
    mp3_next ();
  }
  
  //Если контакта нет, не поём
  if (!isContact/* && wasContact*/) {
    mp3_stop ();
  }
  
  //Если плеер замолчал, делаем ему проверку на дребезг
  isPlaying = !(digitalRead(PLAYER_STATE));
  if (!isPlaying && wasPlaying) {
    delay(1000);
    isPlaying = !(digitalRead(PLAYER_STATE));
  }
  
  //Если плеер замолчал, зажигаем все лампочки, кроме той, где контакт (один раз)
  if (isContact && !isPlaying && wasPlaying) {
//    delay(1000);
    for (int i=0; i<PAIRS_NUMBER; i++) {
      digitalWrite(leds[i], (i!=currChannel));
    }
  }
  

  //сохраняем значение контакта и плеера для следующего витка
  wasContact = isContact;
  wasPlaying = isPlaying;  
  
  
  /*
  
  //Считываем состояние контакта и боремся с дребезгом
  isContact = digitalRead(contacts[currChannel]) == LOW;
  if (isContact) {
    delay(200);
    isContact = digitalRead(contacts[currChannel]) == LOW;
  }
  
  //Считываем состояние плеера
  isPlaying = !(digitalRead(PLAYER_STATE));
  
  //Если появился контакт, начинаем петь (один раз)
  if (isContact && !wasContact) {
    digitalWrite(leds[currChannel], HIGH);
    //mp3_play (1);
    delay(10);
    mp3_next ();
    delay(10);    
  }
  
  //Если пропал контакт, прекращаем петь (один раз)
  else if (!isContact && wasContact) {
    delay(10);
    mp3_stop ();
    delay(10);
    ChangingTheChannel();
  }

  //Если плеер замолчал сам, переключаемся (один раз)
  else if (!isPlaying && wasPlaying) {
    delay(10);
    mp3_stop ();
    delay(10);
    ChangingTheChannel();
  }
  
  //Если контакта нет, бликуем (много раз)
  else if (!isContact) {
    blinking();
  }  

  //сохраняем значение контакта и плеера для следующего витка
  wasContact = isContact;
  wasPlaying = isPlaying;  
*/}

/*********************************************************************************/

void ChangingTheChannel()
{
    digitalWrite(leds[currChannel], LOW);
    while(currChannel == prevChannel) {
      currChannel = random(PAIRS_NUMBER);
    }
    prevChannel = currChannel;
 }
/*********************************************************************************/
void blinking()
{
  if (digitalRead(contacts[currChannel]) == HIGH) {
    digitalWrite(leds[currChannel], HIGH);
    delay(200);
  }

  if (digitalRead(contacts[currChannel]) == HIGH) {
    digitalWrite(leds[currChannel], LOW);
    delay(200);
  }

  if (digitalRead(contacts[currChannel]) == HIGH) {
    digitalWrite(leds[currChannel], HIGH);
    delay(200);
  }

  if (digitalRead(contacts[currChannel]) == HIGH) {
    digitalWrite(leds[currChannel], LOW);
    delay(500);
  }
}

