#ifndef MYLIB_H
#define MYLIB_H

#if ARDUINO >= 100
  #include <Arduino.h>
#else
  #include <WProgram.h>
#endif

class AsyncTaskTable{
	public:
		boolean done;
		char topic[50];
		char value[50];
		boolean retain;
		
};


class AsyncTask{
	public:
		AsyncTask();
		char topic[50];
		char value[50];
		boolean retain;
		
		void addTask(char _topic[50], char _value[50], boolean _retain);
		boolean doTask();
		void printTable();
};

//========================================================================================================================
class ButtonHandler{
  private:
    boolean pressed=false;
    long timerStart=0;
	static const int arrSize=10;

  public:
    int buttonsNumber = 10;
	int buttonPins[arrSize];
    int shortPresses[arrSize], longPresses[arrSize];
        
    void showPresses();
};
//========================================================================================================================

#endif