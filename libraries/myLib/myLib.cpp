#include <myLib.h>
//class AsyncTask
		const int tableSize = 5;
		AsyncTaskTable atTable[tableSize];
		int currItemAddNo = 0;
		

		AsyncTask::AsyncTask(){
			currItemAddNo = 0;
			
			for(int i=0; i<tableSize; i++){
				atTable[i].done=true;
				//atTable[i].topic="";
				//atTable[i].value="";
				sprintf(atTable[i].topic,"");
				sprintf(atTable[i].value,"");					
				atTable[i].retain=false;
			}
		}
		
		void AsyncTask::addTask(char _topic[50], char _value[50], boolean _retain){
			atTable[currItemAddNo].done = false;
			//atTable[currItemAddNo].topic = _topic;
			//atTable[currItemAddNo].value = _value;
			sprintf(atTable[currItemAddNo].topic,"%s",_topic);
			sprintf(atTable[currItemAddNo].value,"%s",_value);					
			atTable[currItemAddNo].retain = _retain;
			Serial.print(millis());
			Serial.print("\tAsyncTask::addTask()\t");
			Serial.print("Item added at place #");
			Serial.println(currItemAddNo);

			currItemAddNo++;
			currItemAddNo = currItemAddNo>tableSize-1 ? 0 : currItemAddNo;			
		}
		
		boolean AsyncTask::doTask(){
			for(int i=0; i<tableSize; i++){
				if(atTable[i].done==false){
					char msg[100];
					sprintf(msg,"%d\tAsyncTask::doTask()\tI have prepared %s message '%s' to the topic '%s'",
					  millis(),
					  atTable[i].retain ? "retain" : "not retain",
					  atTable[i].value,
					  atTable[i].topic);
					Serial.println(msg);

					atTable[i].done=true;
					//topic=&atTable[i].topic[0u];
					//value=&atTable[i].value[0u];
					sprintf(topic,"%s",atTable[i].topic);
					sprintf(value,"%s",atTable[i].value);
					retain=atTable[i].retain;
					return true;
					break;
				}				
			}
			return false;
		}
				
		void AsyncTask::printTable(){
			for(int i=0; i<tableSize; i++){
				Serial.print(i);
				Serial.print(": ");
				Serial.print(atTable[i].done);
				Serial.print(", ");
				Serial.print(atTable[i].topic);
				Serial.print(", ");
				Serial.print(atTable[i].value);
				Serial.print(", ");
				Serial.println(atTable[i].retain);				
			}
			Serial.println("");
		}

//class ButtonHandler		
		void ButtonHandler::showPresses(){
			if(!pressed){
				  long _arrElapsed[buttonsNumber];
				  
				  for(int i=0; i<buttonsNumber; i++){
					longPresses[i]=0; shortPresses[i]=0; _arrElapsed[i]=0;
				  }
				  
				  //Подумать, как сделать грамотно
				  while(pressed
						|| digitalRead(buttonPins[0])
						|| digitalRead(buttonPins[1])
						|| digitalRead(buttonPins[2])
						|| digitalRead(buttonPins[3])
						|| digitalRead(buttonPins[4])
						){
						  if(millis()-timerStart > 29){
								  for(int i=0; i<buttonsNumber; i++){
										  if(digitalRead(buttonPins[i])){
											_arrElapsed[i]++;
											pressed=true;
										  }
										  else if(_arrElapsed[i]>0){
											if(_arrElapsed[i]>10)  longPresses[i]++;
											else                  shortPresses[i]++;
											_arrElapsed[i]=0;
											pressed=false;
										  }
								  }
								  timerStart=millis();
						  }
				  }
				  Serial.print("\n\nSHORTs:\t\t");
				  for(int i=0; i<buttonsNumber; i++){
					  Serial.print(i);
					  Serial.print("=");
					  Serial.print(shortPresses[i]);
					  Serial.print("\t");
				  }
				  Serial.print("\nLONGs:\t\t");
				  for(int i=0; i<buttonsNumber; i++){
					  Serial.print(i);
					  Serial.print("=");
					  Serial.print(longPresses[i]);
					  Serial.print("\t");
				  }
				  Serial.println();
			}     
		}


