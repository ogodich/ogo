/****************************************************************************************************************************************************************************/
void guestsAnimation(){
  if(!gameStarted){
          if(millis() % 100 == 0){
                  if(lightStat[0]) smartLed.setPixelColor(0, smartLed.Color(0,guestsAnimationBrightness,0));
                  else             smartLed.setPixelColor(0, smartLed.Color(guestsAnimationBrightness,0,0));
                  if(lightStat[6]) smartLed.setPixelColor(1, smartLed.Color(0,guestsAnimationBrightness,0));
                  else             smartLed.setPixelColor(1, smartLed.Color(guestsAnimationBrightness,0,0));
                  smartLed.show();
                  
                  if (animHiLow){
                    guestsAnimationBrightness += 5;
                    if(guestsAnimationBrightness > 255) {
                      animHiLow=!animHiLow;
                      guestsAnimationBrightness = 255;
                    }
                  }else{
                    guestsAnimationBrightness -= 5;
                    if(guestsAnimationBrightness < 100) {
                      animHiLow=!animHiLow;
                      guestsAnimationBrightness = 100;
                    }
                  }
          }
  }
}
/****************************************************************************************************************************************************************************/
void lock(String item, boolean action) {
  if(item=="bath")  {
    lock_bath = action;
    //anim_rainbow(1);
  }
  else if(item=="wc")  {
    lock_wc = action;
    //anim_rainbow(0);
  }
  refreshIndicators();
}
/****************************************************************************************************************************************************************************/
void anim_rainbow (int LED){
    for (int i=1; i<=8; i++){
      switch(i){
        case 1: smartLed.setPixelColor(LED, smartLed.Color(255,0,0)); break;      //каждый
        case 2: smartLed.setPixelColor(LED, smartLed.Color(255,47,0)); break;     //охотник
        case 3: smartLed.setPixelColor(LED, smartLed.Color(250,250,0)); break;    //желает
        case 4: smartLed.setPixelColor(LED, smartLed.Color(0,255,0)); break;      //знать
        case 5: smartLed.setPixelColor(LED, smartLed.Color(0,255,255)); break;    //где
        case 6: smartLed.setPixelColor(LED, smartLed.Color(0,0,255)); break;      //сидит
        case 7: smartLed.setPixelColor(LED, smartLed.Color(128,0,250)); break;    //фазан
        case 8: smartLed.setPixelColor(LED, smartLed.Color(255,255,255)); break;        
      }   
    smartLed.show();
    delay(40);
  }
}
/****************************************************************************************************************************************************************************/
void refreshIndicators(){
  if(!gameStarted){
    //Туалет
    if      (lock_wc)        mySetColor(0, 200,200,200);
    else if (lightStat[0] && offline)   mySetColor(0, 0,200,50);
    else if (!lightStat[0] && offline)  mySetColor(0, 250,0,50);
    else if (lightStat[0])   mySetColor(0, 0,200,0);
    else if (!lightStat[0])  mySetColor(0, 250,0,0);
    
    //Кухня
    if (guests)              smartLed.setPixelColor(4, smartLed.Color(0,0,0));
    else if (lightStat[1] && offline)   mySetColor(4, 0,200,50);
    else if (!lightStat[1] && offline)  mySetColor(4, 250,0,50);
    else if (lightStat[1])   mySetColor(4, 0,200,0);
    else if (!lightStat[1])  mySetColor(4, 250,0,0);
    //Угловой, вытяжка
    if (guests)                               smartLed.setPixelColor(3, smartLed.Color(0,0,0));
    else if (offline)                         smartLed.setPixelColor(3, smartLed.Color(0,0,0));
    else if (lightStat[2] && lightStat[3])    mySetColor(3, 250,200,0);
    else if (lightStat[2] && !lightStat[3])   mySetColor(3, 0,200,0);
    else if (!lightStat[2] && lightStat[3])   mySetColor(3, 0,0,200);
    else if (!lightStat[2] && !lightStat[3])  mySetColor(3, 250,0,0);
    //Ленты
    if (guests)                               smartLed.setPixelColor(2, smartLed.Color(0,0,0));
    else if (offline)                         smartLed.setPixelColor(2, smartLed.Color(0,0,0));    
    else if (lightStat[4] && lightStat[5])    mySetColor(2, 250,200,0);
    else if (lightStat[4] && !lightStat[5])   mySetColor(2, 0,200,0);
    else if (!lightStat[4] && lightStat[5])   mySetColor(2, 0,0,200);
    else if (!lightStat[4] && !lightStat[5])  mySetColor(2, 200,0,0);
    
    //Ванна
    if      (lock_bath)                       mySetColor(1, 200,200,200);
    else if (offline && lightStat[6] && !lightStat[7])   mySetColor(1, 0,200,50);
    else if (offline && !lightStat[6] && lightStat[7])   mySetColor(1, 50,50,200);
    else if (offline && !lightStat[6] && !lightStat[7])  mySetColor(1, 250,0,50);
    else if (lightStat[6] && !lightStat[7])   mySetColor(1, 0,200,0);
    else if (!lightStat[6] && lightStat[7])   mySetColor(1, 0,0,200);
    else if (!lightStat[6] && !lightStat[7])  mySetColor(1, 250,0,0);
  
    smartLed.show(); // This sends the updated pixel color to the hardware.
  }
}
/****************************************************************************************************************************************************************************/
void mySetColor(int pix, int r, int g, int b){
  smartLed.setPixelColor(pix, smartLed.Color(r * CurrenSwitchtBrightness / 100, g * CurrenSwitchtBrightness / 100, b * CurrenSwitchtBrightness / 100));
}
/****************************************************************************************************************************************************************************/
void animation_1 (){
  for (int i=0; i<250; i++){
    smartLed.setPixelColor(0, smartLed.Color(250-i, i < 200 ? i : 200, 0));
    delay(3);    
    smartLed.show();
    i=i+10;
  }
}
/****************************************************************************************************************************************************************************/
void animation_2 (){
  for (int i=0; i<250; i++){
    smartLed.setPixelColor(0, smartLed.Color(i, 200 - (i < 200 ? i : 200), 0));
    delay(3);    
    smartLed.show();
    i=i+10;
  }
}
/****************************************************************************************************************************************************************************/
void switchLights(){
  digitalWrite(lights[0], !lightStat[1]);
  digitalWrite(lights[1], !lightStat[0]);
  digitalWrite(lights[2], !lightStat[6]);
  digitalWrite(lights[3], !lightStat[7]);
}
