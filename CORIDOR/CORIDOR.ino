#define BOARD_NAME "CORIDOR"
#define BOARD_PASS "coridor"
//-=-=-=-=-=-=-=-=-=-=-=-

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#include <ArduinoJson.h>

#include <myLib.h>

#define SMART_LED_1            D4


/*_______________________________________________________________________________________________________________________________*/
void ICACHE_RAM_ATTR interruptButtons ();   //это обязательная хрень после перехода на IDE 1.8.10. Что-то связанное с памятью про прерывания

const String DEVICES[] = {"kitchen","wc","bath1","bath2"};

const char* ssid = "ScorpGOS";
const char* password = "9031124945";

boolean startBoot = false;
double startTime = 0;

int lights[4] = {D6,D9,10,D3};

const int numButtons = 5; const int buttons[numButtons] = {D7, D2, D1, D5, D8};

boolean lightStatTest[8] = {false, false, false, false, false, false, false, false};
boolean lightStat[8] = {false, false, false, false, false, false, false, false};

const char* mqtt_server = "192.168.1.1";
WiFiClient espClient;
PubSubClient client(espClient);
ButtonHandler   buttonHandler;
long lastMsg = 0;
char msg[50];

int CurrenSwitchtBrightness = 100;

Adafruit_NeoPixel smartLed = Adafruit_NeoPixel(5, SMART_LED_1, NEO_GRB + NEO_KHZ800);

boolean lock_bath = false;
boolean lock_wc = false;
boolean guests = false;

boolean animHiLow;
int guestsAnimationBrightness = 0;

boolean gameStarted = false;
int arrGameNumber[15] = {-1,    0,4,3,2,1,    -1,-1,-1,-1,-1,   -1,-1,-1,-1};

//home profiles-------------------------------------------
String currentProfileNo = "";
String jSonProfiles = "";
const int cDevices = sizeof(DEVICES) / sizeof(DEVICES[0]);
const char addLogTopic[]="profiles/log/add";
bool logEnable=false;
//--------------------------------------------------------

boolean offline = false;
long mqttLastReconnect = 0;
