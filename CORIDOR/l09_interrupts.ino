//процедура обработки прерывайний
void interruptButtons() {
  //Отображение информации о нажатиях
  buttonHandler.showPresses();

if(!gameStarted){
//==================================================================================================================
//REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  --  REAL  
//==================================================================================================================
  //КОМБИНАЦИИ-------------------------------------------------------------------------------------------------

  //Включаем РАДИО
  if (buttonHandler.longPresses[2]==1 && buttonHandler.shortPresses[1]==1)  {
    client.publish("radio", "2");
  }

  //Понижаем громкость, если зажата 2 и нажата больше одного 1
  else if (buttonHandler.longPresses[2]==1 && buttonHandler.shortPresses[1]>1)  {
    snprintf (msg, 5, "%d", (buttonHandler.shortPresses[1]-1)*(-1));
    client.publish("volume_change", msg);
  }
  //Повышаем громкость, если зажата 2 и нажата больше одного 3
  else if (buttonHandler.longPresses[2]==1 && buttonHandler.shortPresses[3]>1)  {
    snprintf (msg, 5, "%d", buttonHandler.shortPresses[3]-1);
    client.publish("volume_change", msg);
  }

  //ПРОФИЛИ ЛЕНТЫ НА КУХНЕ
    else if (buttonHandler.longPresses[0]==1 && buttonHandler.shortPresses[3]==2) client.publish("devices/kitStrip1/status", "2", true);
    else if (buttonHandler.longPresses[0]==1 && buttonHandler.shortPresses[3]==3) client.publish("devices/kitStrip1/status", "3", true);
    else if (buttonHandler.longPresses[0]==1 && buttonHandler.shortPresses[3]==4) client.publish("devices/kitStrip1/status", "4", true);

  //ЭМУЛЯТОР КОРРИДОРНОГО ВЫКЛЮЧАТЕЛЯ
    //первая кнопка
    else if (buttonHandler.longPresses[4]==1 && buttonHandler.shortPresses[1]==1) client.publish("devices/hall/status/binar", BOARD_NAME);
    //вторая кнопка
    else if (buttonHandler.longPresses[4]==1 && buttonHandler.shortPresses[2]==1) client.publish("devices/hallStrip1/status/binar", BOARD_NAME);
    else if (buttonHandler.longPresses[4]==1 && buttonHandler.longPresses[2]==1) client.publish("devices/hallStrip1/status", "2", true);
    //третья кнопка
    else if (buttonHandler.longPresses[4]==1 && buttonHandler.shortPresses[3]==1) client.publish("devices/lobby/status/binar", BOARD_NAME);
  
  //Всеобщий пресет - 1
  else if (buttonHandler.shortPresses[0]==1 && buttonHandler.shortPresses[4]==1) client.publish("homeLightPresets", "1");
  //Всеобщий пресет - 2
  else if (buttonHandler.longPresses[0]==1 && buttonHandler.longPresses[4]==1) client.publish("homeLightPresets", "2");

/*****************************************************************************************************************
*****************************************************************************************************************/
  //КНОПКИ
  //ПЕРВАЯ кнопка-------------------------------------------------------------------------------------------------  
  else if (buttonHandler.shortPresses[0]==1) {
    if (offline){
      lightStat[0] = !lightStat[0];
      refreshIndicators();
      switchLights();
    }
    else if    (lock_wc) anim_rainbow(0);
    else  client.publish("devices/wc/status/binar", BOARD_NAME);
  }
  else if (buttonHandler.longPresses[0]==1) {
    if    (lock_wc) anim_rainbow(0);
    else if (guests) client.publish("devices/wc/status/binar", BOARD_NAME);
  }

  //ВТОРАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (buttonHandler.shortPresses[1]==1) {
    if (offline){
      lightStat[1] = !lightStat[1];
      refreshIndicators();
      switchLights();
    }
    else    client.publish("devices/kitchen/status/binar", BOARD_NAME);
    //delay(300);
    //client.publish("strip/kitchen/specificLedOn", "{'led':'181','red':'180','green':'0','blue':'0','white':'0'}");
  }
    
  //ТРЕТЬЯ кнопка-------------------------------------------------------------------------------------------------
  else if (buttonHandler.shortPresses[2]==1) client.publish("devices/kitBra/status/binar", BOARD_NAME);
  else if (buttonHandler.longPresses[2]==1) client.publish("devices/coohoo/status/binar", BOARD_NAME);

  //ЧЕТВЕРТАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (buttonHandler.shortPresses[3]==1) {
    client.publish("devices/kitStrip1/status/binar", BOARD_NAME);
    //delay(300);
    //client.publish("strip/kitchen/specificLedOn", "{'led':'181','red':'180','green':'0','blue':'0','white':'0'}");
  }
  else if (buttonHandler.longPresses[3]==1) client.publish("devices/kitStrip2/status/binar", BOARD_NAME);

  //ПЯТАЯ кнопка-------------------------------------------------------------------------------------------------
  else if (buttonHandler.shortPresses[4]==1) {
    if (offline){
          if(lightStat[7]){
            lightStat[7] = false;
            lightStat[6] = false;
          }
          else lightStat[6] = !lightStat[6];
          refreshIndicators();
          switchLights();
    }
    else if (lock_bath) anim_rainbow(1);
    else{
      if(lightStat[7]) client.publish("devices/bath2/status", "0", true);
      else             client.publish("devices/bath1/status/binar", BOARD_NAME);
    }
  }
  else if (buttonHandler.longPresses[4]==1) {
    if (offline){
      lightStat[7] = !lightStat[7];
      if(lightStat[7]) lightStat[6] = false;
      refreshIndicators();
      switchLights();
    }
    else if      (lock_bath) anim_rainbow(1);
    else if (guests)    client.publish("devices/bath1/status/binar", BOARD_NAME);
    else                client.publish("devices/bath2/status/binar", BOARD_NAME);   
  }
}
//==================================================================================================================
//                                                           GAME  
//==================================================================================================================
  else if(gameStarted){
    if      (buttonHandler.shortPresses[0]==1) client.publish("GAME/pressed", "1");
    else if (buttonHandler.shortPresses[1]==1) client.publish("GAME/pressed", "2");
    else if (buttonHandler.shortPresses[2]==1) client.publish("GAME/pressed", "3");
    else if (buttonHandler.shortPresses[3]==1) client.publish("GAME/pressed", "4");
    else if (buttonHandler.shortPresses[4]==1) client.publish("GAME/pressed", "5");
  }

}
