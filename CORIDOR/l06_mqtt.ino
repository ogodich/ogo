void mqtt_loop(){
  if (WiFi.waitForConnectResult() == WL_CONNECTED) {
    if (!client.connected()) {
      reconnect();
    }
    else client.loop();
  }
  else offline = true;
}

void mySubscribtion(){
      client.subscribe("light/kitchen/#");


      client.subscribe("test/animation");

      client.subscribe("homeLightPresets");

      client.subscribe("settings/button/wc/lock");
      client.subscribe("settings/button/bath/lock");
      client.subscribe("settings/mode/guests");
      client.subscribe("settings/switch/coridor/bright");
      
      client.subscribe("GAME/#");

      client.subscribe("devices/coohoo/status");
      client.subscribe("devices/kitStrip1/status");
      client.subscribe("devices/kitStrip2/status");
      client.subscribe("devices/kitBra/status");

      subscribeToDevicesStatus();
      client.subscribe("profiles/run/#");
      client.subscribe("profiles/change/log_enable");
}

void reconnect() {
  if (!offline || (offline && millis() - mqttLastReconnect > 5000)){
    Serial.print("WiFi.waitForConnectResult()=");
    Serial.println(WiFi.waitForConnectResult());
    Serial.print("Attempting MQTT connection...");
    
    // Если удалось подключиться
    if (client.connect(BOARD_NAME)) {
      Serial.println("connected");
      //Если пришли из офлайна, отправляем изменения
      if(offline) sendFromOffline();
      
      offline = false;
      // ... and resubscribe
      mySubscribtion();
    
    // Если не удалось подключиться
    } else {
      Serial.print("failed, rc=");
      Serial.println(client.state());
      offline = true;
      mqttLastReconnect = millis();

      refreshIndicators(); //только для устройств со светодиодами!!!
    }
       
  }
}

void sendFromOffline(){
  snprintf (msg, 75, "%d", lightStat[0]);
  //client.publish("devices/hall/status", msg, true);  
  snprintf (msg, 75, "%d", lightStat[1]);
  //client.publish("devices/lobby/status", msg, true);  
}

void callback(char* topic, byte* payload, unsigned int length) {  
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  String call = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    call+=(char)payload[i];
  }
  Serial.println();

  //----------------------  wc  ---------------------------------
  if (strcmp(topic,"devices/wc/status/binar")==0){
          lightStat[0] = !lightStat[0];
          snprintf (msg, 75, "%d", lightStat[0]);
          client.publish("devices/wc/status", msg, true);
  }
  else if (strcmp(topic,"devices/wc/status")==0){
          lightStat[0] = call.toInt();    
          refreshIndicators();
          switchLights();
  }
  //---------------------------  kitchen  -----------------------  
  else if (strcmp(topic,"devices/kitchen/status/binar")==0){
          lightStat[1] = !lightStat[1];
          snprintf (msg, 75, "%d", lightStat[1]);
          client.publish("devices/kitchen/status", msg, true);
  }
  else if (strcmp(topic,"devices/kitchen/status")==0){
          lightStat[1] = call.toInt();    
          refreshIndicators();
          switchLights();
  }
  //---------------------------  bath1  ------------------------- 
  else if (strcmp(topic,"devices/bath1/status/binar")==0){
          lightStat[6] = !lightStat[6];
          snprintf (msg, 75, "%d", lightStat[6]);
          client.publish("devices/bath1/status", msg, true);
  }
  else if (strcmp(topic,"devices/bath1/status")==0){
          lightStat[6] = call.toInt();
          if (call.toInt() == 1){
                lightStat[7] = false;
                client.publish("devices/bath2/status", "0", true);        
          }
          refreshIndicators();
          switchLights();    
  }

  //---------------------------  bath2  -------------------------
  else if (strcmp(topic,"devices/bath2/status/binar")==0){
          lightStat[7] = !lightStat[7];
          snprintf (msg, 75, "%d", lightStat[7]);
          client.publish("devices/bath2/status", msg, true);
  }
  else if (strcmp(topic,"devices/bath2/status")==0){
          lightStat[7] = call.toInt();
          if (call.toInt() == 1){
            lightStat[6] = false;
            client.publish("devices/bath1/status", "0", true);
          }
          refreshIndicators();
          switchLights();    
  }
  
  else if (strcmp(topic,"devices/kitBra/status")==0){
    if(call=="1" || call=="0") {
      lightStat[2] = call.toInt();
    }
    refreshIndicators();
  }//-----------------------------------------------
  
  else if (strcmp(topic,"devices/coohoo/status")==0){
    if(call=="1" || call=="0") {
      lightStat[3] = call.toInt();
    }
    refreshIndicators();
  }//--devices/coohoo/status

  else if (strcmp(topic,"devices/kitStrip1/status")==0){
    if(call.toInt()==0) lightStat[4] = false;
    if(call.toInt()>0)  lightStat[4] = true;
    refreshIndicators();
  }//--devices/kitStrip1/status

  else if (strcmp(topic,"devices/kitStrip2/status")==0){
    if(call.toInt()==0) lightStat[5] = false;
    if(call.toInt()>0)  lightStat[5] = true;
    refreshIndicators();
  }//-----------------------------------------------


  else if (strcmp(topic,"homeLightPresets")==0){
  
    //Спокойной ночи
    if(call=="2") {
      //Радио
      client.publish("radio", "0", true);
      
      //Большой свет на кухне
      client.publish("devices/kitchen/status", "0", true);
      //Вытяжка на кухне
      client.publish("devices/coohoo/status", "0", true);
      //Угловой свет на кухне
      client.publish("devices/kitBra/status", "0", true);
      //Светодиодная лента на кухне
      client.publish("devices/kitStrip1/status", "0", true);
      client.publish("devices/kitStrip2/status", "0", true);

      //Ванная
      client.publish("devices/bath1/status", "0", true);
      client.publish("devices/bath2/status", "0", true);
      //Туалет
      client.publish("devices/wc/status", "0", true);
      //Лента в прихожей
      client.publish("devices/hallStrip1/status", "0", true);
      //Гирлянды
      client.publish("remote_socket/1", "0", true);
      client.publish("remote_socket/2", "0", true);
      //Корридор, прихожая
      client.publish("devices/hall/status", "0", true);
      client.publish("devices/lobby/status", "0", true);
      //Спальня - картина
      client.publish("devices/bedroomPicture/status", "0", true);
    }    
  }//--homeLightPresets

  else if (strcmp(topic,"settings/button/wc/lock")==0)    lock("wc", call.toInt());
  else if (strcmp(topic,"settings/button/bath/lock")==0)  lock("bath", call.toInt());

  else if (strcmp(topic,"settings/mode/guests")==0){
    guests = call.toInt();
    refreshIndicators();
  }

  else if (strcmp(topic,"settings/switch/coridor/bright")==0){
    CurrenSwitchtBrightness = call.toInt();
    refreshIndicators();
  }

  else if (String(topic).indexOf("GAME/") != -1) {
    game(topic, call);
  }

  //----------ПРОФИЛИ
  else if (String(topic).indexOf("profiles/run/") != -1) {
      RunProfiles(topic, String(call));
  }
  else if (strcmp(topic,"profiles/change/log_enable")==0){
      logEnable = call.toInt();
  }//----------------
  

  
  //*********
}
