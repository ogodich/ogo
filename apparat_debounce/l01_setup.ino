  void setup(void){
  Serial.begin(115200);

  buttonHandler.buttonsNumber=3; //default is 10
  buttonHandler.buttonPins[0]=D5;
  buttonHandler.buttonPins[1]=D6;
  buttonHandler.buttonPins[2]=D7;
  for(int i=0; i<buttonHandler.buttonsNumber; i++) attachInterrupt(buttonHandler.buttonPins[i], interruptButtons, RISING);
  
  WiFi.begin(ssid, password);
  Serial.println("");

  wifiPrint();

  otaInit();

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  pinMode(A0, INPUT);
 
}
