//процедура обработки прерывайний
void interruptButtons() {
  //Отображение информации о нажатиях
  buttonHandler.showPresses();

  //Создаём тестовые задания на отправку сообщения в очередь
  if(buttonHandler.shortPresses[0] && buttonHandler.shortPresses[1]) async.addTask("test/shortBoth", "testVal2", false);
  else if(buttonHandler.longPresses[0] && buttonHandler.shortPresses[1] == 2) async.addTask("test/long0_short1=2", "testVal2", false);  
  else if(buttonHandler.longPresses[0] && buttonHandler.shortPresses[1] > 2){
	snprintf(msg, 5, "%d", buttonHandler.shortPresses[1]);
	async.addTask("test/long0_short1Much", msg, false);
	}

  else if(buttonHandler.longPresses[0]) {
    snprintf(msg, 5, "%d", analogRead(A0)); //Отправляем показания датчика света
    async.addTask("test/long0", msg, false);
  }
  else if(buttonHandler.longPresses[1]) async.addTask("test/long1", "testVal2", false);
  else if(buttonHandler.shortPresses[1]) async.addTask("test/short1", "testVal2", false);
  }

void wifiPrint(){
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}


void otaInit(){
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname(BOARD_NAME);

  // No authentication by default
   ArduinoOTA.setPassword((const char *)"strip");

  ArduinoOTA.onStart([]() {
    //Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    //Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();  
}

//*****************************************************************************************************
char *uint64_to_string(uint64_t input)
{
    static char result[21] = "";
    // Clear result from any leftover digits from previous function call.
    memset(&result[0], 0, sizeof(result));
    // temp is used as a temporary result storage to prevent sprintf bugs.
    char temp[21] = "";
    char c;
    uint8_t base = 10;

    while (input) 
    {
        int num = input % base;
        input /= base;
        c = '0' + num;

        sprintf(temp, "%c%s", c, result);
        strcpy(result, temp);
    } 
    return result;
}
